import { Link } from "react-router-dom"

export const InputNav = (props) =>{
    return(<>
    <li>
        <Link to={props.link}><i className="bi bi-circle"></i><span>{props.name}</span></Link>
    </li>
    </>)
}