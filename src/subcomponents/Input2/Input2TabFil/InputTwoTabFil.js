import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { DatePick } from '../../DatePick';
import { updateDataField } from '../../../redux/slice/InputTwo/FILInputTwo/FilSlice'; // Import your data slice
import { InputField } from '../../InputField';

export function InputTwoTabFil() {
  const { addendum_date, revised_interest_rate, disbursement_date, outstanding_amount, disbursed_amount, sanction_date, processing_fee_pct,
    interest_rate_pct, purpose_of_loan, loan_amount, type_of_assistance, existing_infrastructure
      } = useSelector((state) => state.filSlice);
  const dispatch = useDispatch();

  const handleExistingInfrastructureChange = (value) => {
    dispatch(updateDataField({ fieldName: 'existing_infrastructure', value }));
  };

  const handleTypeOfAssistanceChange = (value) => {
    dispatch(updateDataField({ fieldName: 'type_of_assistance', value }));
  };

  const handleLoanAmountChange = (value) => {
    dispatch(updateDataField({ fieldName: 'loan_amount', value }));
  };

  const handlePurposeOfLoanChange = (value) => {
    dispatch(updateDataField({ fieldName: 'purpose_of_loan', value }));
  };

  const handleInterestRateChange = (value) => {
    dispatch(updateDataField({ fieldName: 'interest_rate_pct', value }));
  };

  const handleProcessingFeeChange = (value) => {
    dispatch(updateDataField({ fieldName: 'processing_fee_pct', value }));
  };

  const handleSanctionDateChange = (value) => {
    dispatch(updateDataField({ fieldName: 'sanction_date', value }));
  };

  const handleDisbursedAmountChange = (value) => {
    dispatch(updateDataField({ fieldName: 'disbursed_amount', value }));
  };

  const handleOutstandingAmountChange = (value) => {
    dispatch(updateDataField({ fieldName: 'outstanding_amount', value }));
  };

  const handleDisbursementDateChange = (value) => {
    dispatch(updateDataField({ fieldName: 'disbursement_date', value }));
  };

  const handleRevisedInterestRateChange = (value) => {
    dispatch(updateDataField({ fieldName: 'revised_interest_rate', value }));
  };

  const handleAddendumDateChange = (value) => {
    dispatch(updateDataField({ fieldName: 'addendum_date', value }));
  };

  return (
    <div>
        <div class="row g-3">
      <InputField
        label="Existing Infrastructure"
        type="text"
        onChange={handleExistingInfrastructureChange}
        value={existing_infrastructure}
      />
      <InputField
        label="Type of Assistance"
        type="text"
        onChange={handleTypeOfAssistanceChange}
        value={type_of_assistance}
      />
      <InputField
        label="Loan Amount"
        type="number"
        endAr="(INR Lakh)"
        onChange={handleLoanAmountChange}
        value={loan_amount}
      />
      <InputField
        label="Purpose of Loan"
        type="text"
        onChange={handlePurposeOfLoanChange}
        value={purpose_of_loan}
      />
      <InputField
        label="Interest Rate"
        type="number"
        endAr="%"
        onChange={handleInterestRateChange}
        value={interest_rate_pct}
      />
      <InputField
        label="Processing Fee"
        type="number"
        endAr="%"
        onChange={handleProcessingFeeChange}
        value={processing_fee_pct}
      />
      <DatePick
        label="Sanction Date"
        className="custom-date-picker"
        onChange={handleSanctionDateChange}
        disbrushDate={sanction_date}
      />
      <InputField
        label="Disbursed amount"
        type="number"
        onChange={handleDisbursedAmountChange}
        value={disbursed_amount}
      />
      <InputField
        label="Outstanding amount"
        type="number"
        onChange={handleOutstandingAmountChange}
        value={outstanding_amount}
      />
      <DatePick
        label="Disbursement Date"
        className="custom-date-picker"
        onChange={handleDisbursementDateChange}
        disbrushDate={disbursement_date}
      />
      <InputField
        label="Revised Interest Rate"
        type="number"
        onChange={handleRevisedInterestRateChange}
        value={revised_interest_rate}
      />
      <DatePick
        label="Addendum Date"
        className="custom-date-picker"
        onChange={handleAddendumDateChange}
        disbrushDate={addendum_date}
      />
      </div>
    </div>
  );
}
