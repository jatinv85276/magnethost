
import React ,{ useState } from 'react';
import { useEffect } from 'react';
import { Offline, Online } from 'react-detect-offline';
import axios from 'axios';
import { Box,  FormControl, InputLabel,  MenuItem, Select } from '@mui/material';
import { getConfigWithToken } from '../../../../utils/Config/Config';

export const MSAMBBeneficiaryDetails = () =>{ 
    
    const labelStyle = {
        marginBottom: "20px",
        marginRight:"20px",
        width: "225px", 
        textAlign: "right", 
        fontWeight:"400",
        marginLeft:"-3rem",
    }

    const apiUrl = process.env.REACT_APP_API_URL;
    const [loading, setLoading] = useState(true);
    const [selectMSAMBFacility, setSelectMSAMBFacility] = useState("")
    const [selectWorkPackage, setSelectWorkPackage] = useState("")
    const [selectDivision, setSelectDivision] = useState("")
    const [selectDistrict, setSelectDistrict] = useState("")
    const [selectTaluka, setSelectTaluka] = useState("")
    const [selectCrop, setSelectCrop] = useState("")
    

    const [MSAMBFacility, setMSAMBFacility] = useState([])
    const [WorkPackage, setWorkPackage] = useState([])
    const [Division, setDivision] = useState([])
    const [District, setDistrict] = useState([])
    const [Taluka, setTaluka] = useState([])
    const [MagnetCrop, setMagentCrop] = useState([])


    const [errorSelectMSAMBFacility, setErrorSelectMSAMBFacility] = useState("")

    const handleMSAMBFacility = async (event) =>{
        setSelectMSAMBFacility(event.target.value)    
    }
    const handleWorkPackage = async (event) =>{
        setSelectWorkPackage(event.target.value)    
    }  
    const handleDivisionChange = async (event) =>{
        setSelectDivision(event.target.value)    
    }
    const handleDistrictChange = async (event) =>{
        setSelectDistrict(event.target.value)    
    }
    const handleTalukaChange = async (event) =>{
        setSelectTaluka(event.target.value)    
    }
    const handleMagnetCrop = async (event) =>{
        setSelectCrop(event.target.value)    
    }


    const checkFPOSelect = () =>{
        if(selectMSAMBFacility.length === 0){
            setErrorSelectMSAMBFacility("Please Select MSAMB Facility")
        }else{
            return true
        }
        return false
    }

    useEffect(()=>{
        const MSAMBFacilityData = async () => {
            try {
                const msamb = await axios.get(`${apiUrl}api/getMsambFacility?all=1`,getConfigWithToken());  
                const workpack = await axios.get(`${apiUrl}api/work-package-listing?all=1`,getConfigWithToken());  
                const division = await axios.get(`${apiUrl}api/getAllDivisions`,getConfigWithToken());  
                const district = await axios.get(`${apiUrl}api/get-all-district`,getConfigWithToken());  
                const taluka = await axios.get(`${apiUrl}api/get-all-taluka-with-village`,getConfigWithToken());  
            
                
                const msambData = msamb.data["data"].map((e)=>({"id":e["msamb_facility_id"],"msamb_facility_name":e["msamb_facility_name"]}));
                const workpackData = workpack.data["data"].map((e)=>({"id":e["work_package"],"work_package":e["work_package"]}));
                const divisionData = division.data["data"].map((e)=>({"id":e["divisions"],"divisions":e["divisions"]}));
                const districtData = district.data["data"].map((e)=>({"id":e["district"],"district":e["district"]}));
                const talukaData = taluka.data["data"].map((e)=>({"id":e["district"],"district":e["district"]}));
            

                setMSAMBFacility(msambData); // Pass msambData to setmsamb
                setWorkPackage(workpackData);
                setDivision(divisionData);
                setDistrict(districtData);
                setTaluka(talukaData);
                
                setLoading(false);
                } catch (error) {
                console.error("Error fetching data:", error);
                }
            };
          
          MSAMBFacilityData();

        setLoading(false);
    },[])
    return(<>   
    {/* <Online> */}
        
       <div id="exTab3" class="contain">
            <form class="row g-2"  >
                {/* Name of the MSAMB Facility */}
                <div>
                    <label for="MSAMBFacility" style={labelStyle}>Name of the MSAMB facility</label>
                        <select name="MSAMBFacility" style={{width:"50%" , height:"30px"}}
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={selectMSAMBFacility}
                            onChange={handleMSAMBFacility}                                               
                        >                     
                        {(MSAMBFacility).map((e)=>(       
                             <option value={e.id}>{e.msamb_facility_name}</option>
                        ))}
                        </select>
                </div>
                
                {/* Work Package */}
                <div>
                    <label for="WorkPackage" style={labelStyle}>Work Package</label>
                    <select name="WorkPackage" style={{width:"20%" , height:"30px"}}
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={selectWorkPackage}
                            onChange={handleWorkPackage}                          
                        >                      
                        {(WorkPackage).map((e)=>(       
                             <option value={e.id}>{e.work_package}</option>
                        ))}
                        </select>        
                </div>

                {/* Division */}
                <div>
                    <label for="Division" style={labelStyle}>Division</label>
                        <select name="Division" style={{width:"20%" , height:"30px"}}
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            value={selectDivision}
                            onChange={handleDivisionChange}                                         
                        >
                        {(Division).map((e)=>(
                             <option value={e.id}>{e.divisions}</option>
                        ))} 
                        </select>
                </div>

                {/* District */}
                <div>
                    <label for="District" style={labelStyle}>District</label>
                    <select name="District" style={{width:"10%" , height:"30px"}}
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        value={selectDistrict}
                        onChange={handleDistrictChange}                                         
                    >
                    {(District).map((e)=>(
                         <option value={e.id}>{e.district}</option>
                    ))} 
                    </select>
                </div>

                {/* Taluka */}
                <div>
                    <label for="Taluka" style={labelStyle}>Taluka</label>
                    <select name="Taluka" style={{width:"10%" , height:"30px"}}
                        labelId="demo-simple-select-label"
                        id="demo-simple-select"
                        // value={selectDivisionName}
                        // onChange={handleDivisionChange}                                         
                    >
                    {/* {(DivisionName).map((e)=>(
                         <option value={e.id}>{e.divisions}</option>
                    ))}  */}
                    </select>
                </div>

                {/* Magnet Crop */}
                <div>
                    <label for="MAGNET" style={labelStyle}>MAGNET Crops</label>
                    <select name="MAGNET" style={{width:"10%" , height:"30px"}}
                         labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            // value={selectDivisionName}
                            // onChange={handleDivisionChange}                                         
                        >
                        {/* {(DivisionName).map((e)=>(
                             <option value={e.id}>{e.divisions}</option>
                        ))}  */}
                    </select>
                </div>

                {/* Area */}
                <div>
                    <label for="Area" style={labelStyle}>Area</label>
                    <input type="text" name="Area" style={{width:"30%",height:"30px"}}/>
                </div>
            </form>
        </div>     
    {/* </Online>
    <Offline>
        You're Offline, Please Check your Connection
    </Offline> */}
    </>)
}

