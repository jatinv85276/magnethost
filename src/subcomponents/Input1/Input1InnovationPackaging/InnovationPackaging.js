import { DatePick } from "../../DatePick";
import { InputField } from "../../InputField";
import {
  resetValidation,
  setInputFieldData,
} from "../../../redux/slice/InputOne/InnovationPack/InnovationSlice";

import { useDispatch, useSelector } from "react-redux";

export const InnovationPackaging = () => {
  const dispatch = useDispatch();
  const innovation = useSelector((state) => state.innovation);

  const handleFieldProjectCost = (value) => {
    dispatch(setInputFieldData({ fieldName: "Total_Project_Cost", value }));
  };

  const handleFieldBeneContribution = (value) => {
    dispatch(
      setInputFieldData({ fieldName: "Beneficiary_Contribution", value })
    );
  };

  const handleFieldSanctionGrant = (value) => {
    dispatch(setInputFieldData({ fieldName: "Total_SanctionGrant", value }));
  };

  const handleFieldPayment = (value) => {
    dispatch(setInputFieldData({ fieldName: "Payment_Tranche", value }));
  };

  const handleFieldAmtDisburesement = (value) => {
    dispatch(setInputFieldData({ fieldName: "Amt_Disbursement", value }));
  };

  const handleFieldDisbursementDate = (value) => {
    dispatch(setInputFieldData({ fieldName: "Disbursement_Date", value }));
  };

  const handleFieldTotalExpediture = (value) => {
    dispatch(setInputFieldData({ fieldName: "Total_Expediture", value }));
  };

  const handleFieldFinancialProgress = (value) => {
    dispatch(setInputFieldData({ fieldName: "Financial_Progress", value }));
  };

  const handleFieldBeneficiaryExpenditure = (value) => {
    dispatch(
      setInputFieldData({ fieldName: "Beneficiary_Expenditure", value })
    );
  };

  const handleFieldTotalGrantExpenditure = (value) => {
    dispatch(setInputFieldData({ fieldName: "Total_GrantExpenditure", value }));
  };


  const handleResetFocus = () => {
    dispatch(resetValidation());
  };

  const handleInputChange = (fieldName, value) => {
    if (value < 0) {
      value = 0;
    }
    dispatch(setInputFieldData({ fieldName, value }));
  };

  return (
    <>
      <div className="word-with-line">
        <span>Sanctioned Break-up Cost</span>
        <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
      </div>
      <div class="row g-3">
        <InputField
          label="Total project Cost"
          type="number"
          endAr="(in lakhs)"
          value={innovation?.total_project_cost}
          readOnly={true}
          onChange={handleFieldProjectCost}
          onFocus={handleResetFocus}
          error={!!innovation?.validationErrors.Total_Project_Cost}
          helperText={innovation?.validationErrors.Total_Project_Cost}
        />
        <InputField
          label="Beneficiary Contribution"
          type="number"
          endAr="(in lakhs)"
          value={innovation?.beneficiary_contribution}
          readOnly={true}
          onChange={handleFieldBeneContribution}
          onFocus={handleResetFocus}
          error={!!innovation?.validationErrors.Beneficiary_Contribution}
          helperText={innovation?.validationErrors.Beneficiary_Contribution}
        />
        <InputField
          label="Total Sanction Grant from MAGNET"
          type="number"
          endAr="(in lakhs)"
          value={innovation?.total_saction_grant}
          readOnly={true}
          onChange={handleFieldSanctionGrant}
          onFocus={handleResetFocus}
          error={!!innovation?.validationErrors.Total_SanctionGrant}
          helperText={innovation?.validationErrors.Total_SanctionGrant}
        />

        <div className="word-with-line">
          <span>Project Wise Information</span>
          <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
        </div>
        <InputField
          label="Payment/Tranche"
          type="text"
          value={innovation?.payment_tranche}
          readOnly={true}
          onChange={handleFieldPayment}
          onFocus={handleResetFocus}
          error={!!innovation?.validationErrors.Payment_Tranche}
          helperText={innovation?.validationErrors.Payment_Tranche}
        />
        <InputField
          label="Amount Disbursement"
          type="number"
          endAr="(in lakhs)"
          value={innovation?.amount_disbursement}
          readOnly={true}
          onChange={handleFieldAmtDisburesement}
          onFocus={handleResetFocus}
          error={!!innovation?.validationErrors.Amt_Disbursement}
          helperText={innovation?.validationErrors.Amt_Disbursement}
        />
        <DatePick
          label="Disbursement Date"
          className="custom-date-picker"
          disbrushDate={innovation?.disbursement_date}
          readOnly={true}
          onChange={handleFieldDisbursementDate}
          onFocus={handleResetFocus}
          error={!!innovation?.validationErrors.Disbursement_Date}
          helperText={innovation?.validationErrors.Disbursement_Date}
        />
        <InputField
          label="Total expenditure till date "
          type="number"
          endAr="(in lakhs)"
          value={innovation?.total_exp_till_date}
          readOnly={true}
          onChange={handleFieldTotalExpediture}
          onFocus={handleResetFocus}
          error={!!innovation?.validationErrors.Total_Expediture}
          helperText={innovation?.validationErrors.Total_Expediture}
        />
        <InputField
          label="Financial Progress till date "
          type="number"
          endAr="%"
          value={innovation?.financial_progress_till_date_pct}
          readOnly={true}
          onChange={handleFieldFinancialProgress}
          onFocus={handleResetFocus}
          error={!!innovation?.validationErrors.Financial_Progress}
          helperText={innovation?.validationErrors.Financial_Progress}
        />
        <InputField
          label="Total Beneficiary expenditure till date "
          type="number"
          endAr="(in lakhs)"
          value={innovation?.total_beneficiary_exp_till_date}
          readOnly={true}
          onChange={handleFieldBeneficiaryExpenditure}
          onFocus={handleResetFocus}
          error={!!innovation?.validationErrors.Beneficiary_Expenditure}
          helperText={innovation?.validationErrors.Beneficiary_Expenditure}
        />
        <InputField
          label="Total Grant expenditure till date "
          type="number"
          endAr="(in lakhs)"
          value={innovation?.total_grant_exp_till_date}
          readOnly={true}
          onChange={handleFieldTotalGrantExpenditure}
          onFocus={handleResetFocus}
          error={!!innovation?.validationErrors.Total_GrantExpenditure}
          helperText={innovation?.validationErrors.Total_GrantExpenditure}
        />

        <div className="word-with-line">
          <span>Details of expenditure for Current Quarter</span>
          <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
        </div>
        <InputField
          label="Beneficiary expenditure incurred "
          type="number"
          endAr="(in lakhs)"
          value={innovation?.beneficiary_exp_incurred}
         onChange={(value)=>handleInputChange("beneficiary_exp_incurred",value)}
          onFocus={handleResetFocus}
          error={!!innovation?.validationErrors.beneficiary_exp_incurred}
          helperText={innovation?.validationErrors.beneficiary_exp_incurred}
        />
        <InputField
          label="Grant expenditure incurred "
          type="number"
          endAr="(in lakhs)"
          value={innovation?.grant_exp_incurred}
         onChange={(value)=>handleInputChange("grant_exp_incurred",value)}
          onFocus={handleResetFocus}
          error={!!innovation?.validationErrors.grant_exp_incurred}
          helperText={innovation?.validationErrors.grant_exp_incurred}
        />
        <InputField
          label="Expenditure incurred "
          type="number"
          endAr="(in lakhs)"
          value={innovation?.exp_incurred}
         onChange={(value)=>handleInputChange("exp_incurred",value)}
          onFocus={handleResetFocus}
          error={!!innovation?.validationErrors.exp_incurred}
          helperText={innovation?.validationErrors.exp_incurred}
        />
      </div>
    </>
  );
};
