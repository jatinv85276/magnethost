import {
  Box,
  FormControl,
  FormHelperText,
  InputLabel,
  MenuItem,
  Select,
} from "@mui/material";
import { DatePick } from "../../DatePick";
import { InputField } from "../../InputField";
import { useEffect, useState } from "react";
import {
  setInputFieldData,
  resetValidation,
  setTotalSC,
  setTotalST,
  setTotalPWD,
  setTotalBPL,
  setTotalOBC,
  setTotalParticipants,
} from "../../../redux/slice/InputOne/WorkShop/WorkShopSlice";
import axios from "axios";
import { getConfigWithToken } from "../../../utils/Config/Config";
import { useDispatch, useSelector } from "react-redux";

export const Workshop = (props) => {
  const apiUrl = process.env.REACT_APP_API_URL;
  const dispatch = useDispatch();
  const workShop = useSelector((state) => state.workShop);
  const [taluka, setTaluka] = useState([]);

  useEffect(() => {
    const workData = async () => {
      try {
        const taluka = await axios.get(
          `${apiUrl}api/get-all-taluka-with-village?all=1`,
          getConfigWithToken()
        );
        setTaluka(taluka.data["data"]);
      } catch (error) {}
    };

    workData();
  }, []);

  // Total of SC
  const totalSC =
    parseInt(workShop.sc_male === "" ? 0 : workShop.sc_male) +
    parseInt(workShop.sc_female === "" ? 0 : workShop.sc_female);
  dispatch(setTotalSC(totalSC));
  // Total of ST
  const totalST =
    parseInt(workShop.st_male === "" ? 0 : workShop.st_male) +
    parseInt(workShop.st_female === "" ? 0 : workShop.st_female);
  dispatch(setTotalST(totalST));
  // Total of PWD
  const totalPWD =
    parseInt(workShop.pwd_male === "" ? 0 : workShop.pwd_male) +
    parseInt(workShop.pwd_female === "" ? 0 : workShop.pwd_female);
  dispatch(setTotalPWD(totalPWD));
  // Total of BPL
  const totalBPL =
    parseInt(workShop.bpl_male === "" ? 0 : workShop.bpl_male) +
    parseInt(workShop.bpl_female === "" ? 0 : workShop.bpl_female);
  dispatch(setTotalBPL(totalBPL));
  // Total of OBC/Minority/Open
  const totalOBCMinorityOpen =
    parseInt(
      workShop.obc_minority_open_male === ""
        ? 0
        : workShop.obc_minority_open_male
    ) +
    parseInt(
      workShop.obc_minority_open_female === ""
        ? 0
        : workShop.obc_minority_open_female
    );
  dispatch(setTotalOBC(totalOBCMinorityOpen));

  // Total of OBC/Minority/Open
  const totalParticipants =
    parseInt(
      workShop.participants_male === "" ? 0 : workShop.participants_male
    ) +
    parseInt(
      workShop.participants_female === "" ? 0 : workShop.participants_female
    );
  dispatch(setTotalParticipants(totalParticipants));

  // Dispatch functions for SC fields
  const handleFieldChangeSCMale = (value) => {
    dispatch(setInputFieldData({ fieldName: "sc_male", value }));
  };
  const handleFieldChangeSCFemale = (value) => {
    dispatch(setInputFieldData({ fieldName: "sc_female", value }));
  };

  // Dispatch functions for ST fields
  const handleFieldChangeSTMale = (value) => {
    dispatch(setInputFieldData({ fieldName: "st_male", value }));
  };
  const handleFieldChangeSTFemale = (value) => {
    dispatch(setInputFieldData({ fieldName: "st_female", value }));
  };

  // Dispatch functions for PWD fields
  const handleFieldChangePWDMale = (value) => {
    dispatch(setInputFieldData({ fieldName: "pwd_male", value }));
  };
  const handleFieldChangePWDFemale = (value) => {
    dispatch(setInputFieldData({ fieldName: "pwd_female", value }));
  };

  // Dispatch functions for BPL fields
  const handleFieldChangeBPLMale = (value) => {
    dispatch(setInputFieldData({ fieldName: "bpl_male", value }));
  };
  const handleFieldChangeBPLFemale = (value) => {
    dispatch(setInputFieldData({ fieldName: "bpl_female", value }));
  };

  // Dispatch functions for OBC/Minority/Open fields
  const handleFieldChangeOBCMale = (value) => {
    dispatch(setInputFieldData({ fieldName: "obc_minority_open_male", value }));
  };
  const handleFieldChangeOBCFemale = (value) => {
    dispatch(
      setInputFieldData({ fieldName: "obc_minority_open_female", value })
    );
  };

  // Dispatch functions for Participants fields
  const handleFieldChangeParticipantMale = (value) => {
    dispatch(setInputFieldData({ fieldName: "participants_male", value }));
  };
  const handleFieldChangeParticipantFemale = (value) => {
    dispatch(setInputFieldData({ fieldName: "participants_female", value }));
  };

  const handleFromDateChange = (value) => {
    dispatch(setInputFieldData({ fieldName: "from_date", value }));
  };

  const handleToDateChange = (value) => {
    dispatch(setInputFieldData({ fieldName: "to_date", value }));
  };

  const handleFieldRemarks = (value) => {
    dispatch(
      setInputFieldData({
        fieldName: "action_points_recommendation_made",
        value,
      })
    );
  };

  const handleFieldSubject = (value) => {
    dispatch(setInputFieldData({ fieldName: "subject", value }));
  };
  const handleFieldLocationAddress = (value) => {
    dispatch(setInputFieldData({ fieldName: "location_address", value }));
  };

  const handleOptionstaluka = (event) => {
    dispatch(
      setInputFieldData({
        fieldName: "location_taluka",
        value: event.target.value,
      })
    );
  };

  const handleResetFocus = () => {
    dispatch(resetValidation());
  };

  return (
    <>
      <DatePick
        label="From Date"
        className="custom-date-picker"
        disbrushDate={workShop.from_date}
        onChange={handleFromDateChange}
        errorText={!!workShop.validationErrors.from_date}
      />
      <DatePick
        label="To Date"
        className="custom-date-picker"
        disbrushDate={workShop.to_date}
        onChange={handleToDateChange}
        errorText={!!workShop.validationErrors.to_date}
      />
      <div className="col-md-4" style={{ paddingRight: 0 }}>
        <Box sx={{ minWidth: "100%" }}>
          <FormControl
            fullWidth
            error={
              workShop.validationErrors.location_taluka !== "" ? true : false
            }
          >
            <InputLabel htmlFor="demo-simple-select-label">
              Location (Taluka)
            </InputLabel>
            <Select
              labelId="demo-simple-select-label"
              id="demo-simple-select"
              value={workShop.location_taluka}
              label="Location (Taluka)"
              onChange={handleOptionstaluka}
            >
              {taluka.map((e, key) => (
                <MenuItem value={e.id} key={key}>
                  {e.taluka}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
          <FormHelperText style={{ color: "#d32f2f" }}>
            {workShop.validationErrors.location_taluka}
          </FormHelperText>
        </Box>
      </div>
      <InputField
        label="Location(Address)"
        type="text"
        value={workShop.location_address}
        onChange={handleFieldLocationAddress}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.location_address}
        helperText={workShop.validationErrors.location_address}
      />
      <InputField
        label="Subject"
        type="text"
        value={workShop.subject}
        onChange={handleFieldSubject}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.subject}
        helperText={workShop.validationErrors.remarks}
      />

      <div className="word-with-line"></div>

      <div className="col-md-3 input1Heading">
        <span>SC</span>
      </div>
      <InputField
        col="3"
        label="Male"
        type="number"
        value={workShop.sc_male}
        onChange={handleFieldChangeSCMale}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.sc_male}
        helperText={workShop.validationErrors.sc_male}
      />
      <InputField
        col="3"
        label="Female"
        type="number"
        value={workShop.sc_female}
        onChange={handleFieldChangeSCFemale}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.sc_female}
        helperText={workShop.validationErrors.sc_female}
      />
      <InputField
        col="3"
        label="Total SC"
        type="number"
        value={workShop.sc_total}
        readOnly={true}
      />

      <div className="col-md-3 input1Heading">
        <span>ST</span>
      </div>
      <InputField
        col="3"
        label="Male"
        type="number"
        value={workShop.st_male}
        onChange={handleFieldChangeSTMale}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.st_male}
        helperText={workShop.validationErrors.st_male}
      />
      <InputField
        col="3"
        label="Female"
        type="number"
        value={workShop.st_female}
        onChange={handleFieldChangeSTFemale}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.st_female}
        helperText={workShop.validationErrors.st_female}
      />
      <InputField
        col="3"
        label="Total ST"
        type="number"
        value={workShop.st_total}
        readOnly={true}
      />

      <div className="col-md-3 input1Heading">
        <span>PWD</span>
      </div>
      <InputField
        col="3"
        label="Male"
        type="number"
        value={workShop.pwd_male}
        onChange={handleFieldChangePWDMale}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.pwd_male}
        helperText={workShop.validationErrors.pwd_male}
      />
      <InputField
        col="3"
        label="Female"
        type="number"
        value={workShop.pwd_female}
        onChange={handleFieldChangePWDFemale}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.pwd_female}
        helperText={workShop.validationErrors.pwd_female}
      />
      <InputField
        col="3"
        label="Total PWD"
        type="number"
        value={workShop.pwd_total}
        readOnly={true}
      />

      <div className="col-md-3 input1Heading">
        <span>BPL</span>
      </div>
      <InputField
        col="3"
        label="Male"
        type="number"
        value={workShop.bpl_male}
        onChange={handleFieldChangeBPLMale}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.bpl_male}
        helperText={workShop.validationErrors.bpl_male}
      />
      <InputField
        col="3"
        label="Female"
        type="number"
        value={workShop.bpl_female}
        onChange={handleFieldChangeBPLFemale}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.bpl_female}
        helperText={workShop.validationErrors.bpl_female}
      />
      <InputField
        col="3"
        label="Total BPL"
        type="number"
        value={workShop.bpl_total}
        readOnly={true}
      />

      <div className="col-md-3 input1Heading">
        <span>General (Open, OBC and others)</span>
      </div>
      <InputField
        col="3"
        label="Male"
        type="number"
        value={workShop.obc_minority_open_male}
        onChange={handleFieldChangeOBCMale}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.obc_minority_open_male}
        helperText={workShop.validationErrors.obc_minority_open_male}
      />
      <InputField
        col="3"
        label="Female"
        type="number"
        value={workShop.obc_minority_open_female}
        onChange={handleFieldChangeOBCFemale}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.obc_minority_open_female}
        helperText={workShop.validationErrors.obc_minority_open_female}
      />
      <InputField
        col="3"
        label="Total OBC/ Minority/ Open"
        type="number"
        value={workShop.obc_minority_open_total}
        readOnly={true}
      />

      <div className="col-md-3 input1Heading">
        <span>Participants</span>
      </div>
      <InputField
        col="3"
        label="Male"
        type="number"
        value={workShop.participants_male}
        onChange={handleFieldChangeParticipantMale}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.participants_male}
        helperText={workShop.validationErrors.participants_male}
      />
      <InputField
        col="3"
        label="Female"
        type="number"
        value={workShop.participants_female}
        onChange={handleFieldChangeParticipantFemale}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.participants_male}
        helperText={workShop.validationErrors.participants_male}
      />
      <InputField
        col="3"
        label="Total Participants"
        type="number"
        value={workShop.participants_total}
        readOnly={true}
      />

      <InputField
        col="6"
        label="Action points/ Recommendation made"
        type="text"
        value={workShop.action_points_recommendation_made}
        onChange={handleFieldRemarks}
        onFocus={handleResetFocus}
        error={!!workShop.validationErrors.action_points_recommendation_made}
        helperText={workShop.validationErrors.action_points_recommendation_made}
      />
    </>
  );
};
