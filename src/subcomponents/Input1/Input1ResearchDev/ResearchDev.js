import { DatePick } from "../../DatePick";
import { InputField } from "../../InputField";
import {
  resetValidation,
  setInputFieldData,
} from "../../../redux/slice/InputOne/ResearchDev/researchDevSlice";

import { useDispatch, useSelector } from "react-redux";

export const ResearchDev = (props) => {
  const dispatch = useDispatch();
  const researchDev = useSelector((state) => state.researchDev);

  const handleFieldProjectCost = (value) => {
    dispatch(setInputFieldData({ fieldName: "Total_Project_Cost", value }));
  };

  const handleFieldBeneContribution = (value) => {
    dispatch(
      setInputFieldData({ fieldName: "Beneficiary_Contribution", value })
    );
  };

  const handleFieldSanctionGrant = (value) => {
    dispatch(setInputFieldData({ fieldName: "Total_SanctionGrant", value }));
  };

  const handleFieldPayment = (value) => {
    dispatch(setInputFieldData({ fieldName: "Payment_Tranche", value }));
  };

  const handleFieldAmtDisburesement = (value) => {
    dispatch(setInputFieldData({ fieldName: "Amt_Disbursement", value }));
  };

  const handleFieldDisbursementDate = (value) => {
    dispatch(setInputFieldData({ fieldName: "Disbursement_Date", value }));
  };

  const handleFieldTotalExpediture = (value) => {
    dispatch(setInputFieldData({ fieldName: "Total_Expediture", value }));
  };

  const handleFieldFinancialProgress = (value) => {
    dispatch(setInputFieldData({ fieldName: "Financial_Progress", value }));
  };

  const handleFieldBeneficiaryExpenditure = (value) => {
    dispatch(
      setInputFieldData({ fieldName: "Beneficiary_Expenditure", value })
    );
  };

  const handleFieldTotalGrantExpenditure = (value) => {
    dispatch(setInputFieldData({ fieldName: "Total_GrantExpenditure", value }));
  };

 

  const handleResetFocus = () => {
    dispatch(resetValidation());
  };

  const handleInputChange = (fieldName, value) => {
    if (value < 0) {
      value = 0;
    }
    dispatch(setInputFieldData({ fieldName, value }));
  };

  return (
    <>
      <div className="word-with-line">
        <span>Sanctioned Break-up Cost</span>
        <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
      </div>
      <div class="row g-3">
        <InputField
          label="Total project Cost"
          type="number"
          endAr="(in lakhs)"
          value={researchDev?.total_project_cost}
          readOnly={true}
          onChange={handleFieldProjectCost}
          onFocus={handleResetFocus}
          error={!!researchDev?.validationErrors.Total_Project_Cost}
          helperText={researchDev?.validationErrors.Total_Project_Cost}
        />
        <InputField
          label="Beneficiary Contribution"
          type="number"
          endAr="(in lakhs)"
          value={researchDev?.beneficiary_contribution}
          readOnly={true}
          onChange={handleFieldBeneContribution}
          onFocus={handleResetFocus}
          error={!!researchDev?.validationErrors.Beneficiary_Contribution}
          helperText={researchDev?.validationErrors.Beneficiary_Contribution}
        />
        <InputField
          label="Total Sanction Grant from MAGNET"
          type="number"
          endAr="(in lakhs)"
          value={researchDev?.total_saction_grant}
          readOnly={true}
          onChange={handleFieldSanctionGrant}
          onFocus={handleResetFocus}
          error={!!researchDev?.validationErrors.Total_SanctionGrant}
          helperText={researchDev?.validationErrors.Total_SanctionGrant}
        />

        <div className="word-with-line">
          <span>Project Wise Information</span>
          <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
        </div>
        <InputField
          label="Payment/Tranche"
          type="text"
          value={researchDev?.payment_tranche}
          readOnly={true}
          onChange={handleFieldPayment}
          onFocus={handleResetFocus}
          error={!!researchDev?.validationErrors.Payment_Tranche}
          helperText={researchDev?.validationErrors.Payment_Tranche}
        />
        <InputField
          label="Amount Disbursement"
          type="number"
          endAr="(in lakhs)"
          value={researchDev?.amount_disbursement}
          readOnly={true}
          onChange={handleFieldAmtDisburesement}
          onFocus={handleResetFocus}
          error={!!researchDev?.validationErrors.Amt_Disbursement}
          helperText={researchDev?.validationErrors.Amt_Disbursement}
        />
        <DatePick
          label="Disbursement Date"
          className="custom-date-picker"
          disbrushDate={researchDev?.disbursement_date}
          readOnly={true}
          onChange={handleFieldDisbursementDate}
          onFocus={handleResetFocus}
          error={!!researchDev?.validationErrors.Disbursement_Date}
          helperText={researchDev?.validationErrors.Disbursement_Date}
        />
        <InputField
          label="Total expenditure till date "
          type="number"
          endAr="(in lakhs)"
          value={researchDev?.total_exp_till_date}
          readOnly={true}
          onChange={handleFieldTotalExpediture}
          onFocus={handleResetFocus}
          error={!!researchDev?.validationErrors.Total_Expediture}
          helperText={researchDev?.validationErrors.Total_Expediture}
        />
        <InputField
          label="Financial Progress till date "
          type="number"
          endAr="%"
          value={researchDev?.financial_progress_till_date_pct}
          readOnly={true}
          onChange={handleFieldFinancialProgress}
          onFocus={handleResetFocus}
          error={!!researchDev?.validationErrors.Financial_Progress}
          helperText={researchDev?.validationErrors.Financial_Progress}
        />
        <InputField
          label="Total Beneficiary expenditure till date "
          type="number"
          endAr="(in lakhs)"
          value={researchDev?.total_beneficiary_exp_till_date}
          readOnly={true}
          onChange={handleFieldBeneficiaryExpenditure}
          onFocus={handleResetFocus}
          error={!!researchDev?.validationErrors.Beneficiary_Expenditure}
          helperText={researchDev?.validationErrors.Beneficiary_Expenditure}
        />
        <InputField
          label="Total Grant expenditure till date "
          type="number"
          endAr="(in lakhs)"
          value={researchDev?.total_grant_exp_till_date}
          readOnly={true}
          onChange={handleFieldTotalGrantExpenditure}
          onFocus={handleResetFocus}
          error={!!researchDev?.validationErrors.Total_GrantExpenditure}
          helperText={researchDev?.validationErrors.Total_GrantExpenditure}
        />

        <div className="word-with-line">
          <span>Details of expenditure for Current Quarter</span>
          <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
        </div>
        <InputField
          label="Beneficiary expenditure incurred "
          type="number"
          endAr="(in lakhs)"
          value={researchDev?.beneficiary_exp_incurred}
          onChange={(value) =>
            handleInputChange("beneficiary_exp_incurred", value)
          }
          onFocus={handleResetFocus}
          error={!!researchDev?.validationErrors.beneficiary_exp_incurred}
          helperText={researchDev?.validationErrors.beneficiary_exp_incurred}
        />
        <InputField
          label="Grant expenditure incurred "
          type="number"
          endAr="(in lakhs)"
          value={researchDev?.grant_exp_incurred}
          onChange={(value) => handleInputChange("grant_exp_incurred", value)}
          onFocus={handleResetFocus}
          error={!!researchDev?.validationErrors.grant_exp_incurred}
          helperText={researchDev?.validationErrors.grant_exp_incurred}
        />
        <InputField
          label="Expenditure incurred "
          type="number"
          endAr="(in lakhs)"
          value={researchDev?.exp_incurred}
          onChange={(value) => handleInputChange("exp_incurred", value)}
          onFocus={handleResetFocus}
          error={!!researchDev?.validationErrors.exp_incurred}
          helperText={researchDev?.validationErrors.exp_incurred}
        />
      </div>
    </>
  );
};
