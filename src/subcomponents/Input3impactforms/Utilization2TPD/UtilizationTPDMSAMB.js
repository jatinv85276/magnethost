import { Box, FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { DatePickInput3 } from "../../DatePickInput3";
import { InputField } from "../../InputField";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import axios from "axios";
import { getConfigWithToken } from "../../../utils/Config/Config";
import { MultiSelect } from "../../MultiSelect";
import { setInputFieldData } from "../../../redux/slice/InputThreeImpactForms/UtilizationThreeTPD/UtilizationThreeTPDSlice";

export const UtilizationTPDMSAMB = (props) => {
  const apiUrl = process.env.REACT_APP_API_URL;
  const dispatch = useDispatch();
  const utilizationTPDThree = useSelector((state) => state.utilizationTPDThree);
  const {
    validationErrors,
    is_facility_in_opration,
    use_by_msamb_type,
    total_quantity_handled_tpd,
    total_capacity_utilised_tpd,
    capacity_utilised_job_work,
    capacity_utilised_captive_used,
    total_amount,
    service_rental_charges,
    processing_charges,
    number_of_farmers,
    members_of_fpo_small,
    members_of_fpo_medium,
    members_of_fpo_others,
    non_members_small,
    non_members_medium,
    non_members_others,
  } = useSelector((state) => state.utilizationTPDThree);

  const FacilityOpr = [
    {
      id: 1,
      Status_mode: "Yes",
    },
    {
      id: 2,
      Status_mode: "No",
    },
  ];
  const UsedBy = [
    {
      id: 1,
      Status_mode: "MSAMB",
    },
    {
      id: 2,
      Status_mode: "FPO",
    },
    {
      id: 3,
      Status_mode: "VCO",
    },
    {
      id: 4,
      Status_mode: "Others",
    },
  ];

  const updateTotalFarmersSupplying = (
    members_of_fpo_small,
    members_of_fpo_medium,
    members_of_fpo_others,
    non_members_small,
    non_members_medium,
    non_members_others
  ) => {
    function parseFloatOrZero(value) {
      const parsedValue = parseFloat(value);
      return isNaN(parsedValue) ? 0 : parsedValue;
    }
    const newValue =
      parseFloatOrZero(members_of_fpo_small) +
      parseFloatOrZero(members_of_fpo_medium) +
      parseFloatOrZero(members_of_fpo_others) +
      parseFloatOrZero(non_members_small) +
      parseFloatOrZero(non_members_medium) +
      parseFloatOrZero(non_members_others);
    const roundedValue = newValue.toFixed(2);
    dispatch(
      setInputFieldData({
        fieldName: "number_of_farmers",
        value: roundedValue,
      })
    );
  };

  const handleMultiMagnetCropChange = (data) => {
    dispatch(setInputFieldData({ fieldName: "existing_crops", value: data }));
  };

  const handleOptionFacilityOpr = (event) => {
    dispatch(
      setInputFieldData({
        fieldName: "is_facility_in_opration",
        value: event.target.value,
      })
    );
  };
  const handleOptionUsedBy = (event) => {
    dispatch(
      setInputFieldData({
        fieldName: "use_by_msamb_type",
        value: event.target.value,
      })
    );
  };

  const handleTotalQtyHandTPD = (value) => {
    const newValue = parseFloat(value);
    if (!isNaN(newValue) && newValue >= 0) {
      dispatch(
        setInputFieldData({
          fieldName: "total_quantity_handled_tpd",
          value,
        })
      );
    }
  };

  const handleTotalCapacityUtilized = (value) => {
    const newValue = parseFloat(value);
    if (!isNaN(newValue) && newValue >= 0) {
      dispatch(
        setInputFieldData({
          fieldName: "total_capacity_utilised_tpd",
          value,
        })
      );
    }
  };

  const handleCapacityUtilisedJobWork = (value) => {
    const newValue = parseFloat(value);
    if (!isNaN(newValue) && newValue >= 0) {
      dispatch(
        setInputFieldData({
          fieldName: "capacity_utilised_job_work",
          value,
        })
      );
    }
  };

  const handleCapacityUtilisedCapativeUsed = (value) => {
    const newValue = parseFloat(value);
    if (!isNaN(newValue) && newValue >= 0) {
      dispatch(
        setInputFieldData({
          fieldName: "capacity_utilised_captive_used",
          value,
        })
      );
    }
  };

  const handleTotalAmount = (value) => {
    const newValue = parseFloat(value);
    if (!isNaN(newValue) && newValue >= 0) {
      dispatch(
        setInputFieldData({
          fieldName: "total_amount",
          value,
        })
      );
    }
  };

  const handleserviceRentalCharges = (value) => {
    const newValue = parseFloat(value);
    if (!isNaN(newValue) && newValue >= 0) {
      dispatch(
        setInputFieldData({
          fieldName: "service_rental_charges",
          value,
        })
      );
    }
  };

  const handleProcessingCharges = (value) => {
    const newValue = parseFloat(value);
    if (!isNaN(newValue) && newValue >= 0) {
      dispatch(
        setInputFieldData({
          fieldName: "processing_charges",
          value,
        })
      );
    }
  };

  const handleMemFPOSmall = (value) => {
    const newValue = parseFloat(value);
    if (!isNaN(newValue) && newValue >= 0) {
      dispatch(
        setInputFieldData({
          fieldName: "members_of_fpo_small",
          value,
        })
      );
      updateTotalFarmersSupplying(
        newValue,
        members_of_fpo_medium,
        members_of_fpo_others,
        non_members_small,
        non_members_medium,
        non_members_others
      );
    }
  };

  const handleMemFPOMedium = (value) => {
    const newValue = parseFloat(value);
    if (!isNaN(newValue) && newValue >= 0) {
      dispatch(
        setInputFieldData({
          fieldName: "members_of_fpo_medium",
          value,
        })
      );
      updateTotalFarmersSupplying(
        members_of_fpo_small,
        newValue,
        members_of_fpo_others,
        non_members_small,
        non_members_medium,
        non_members_others
      );
    }
  };

  const handleMemFPOOthers = (value) => {
    const newValue = parseFloat(value);
    if (!isNaN(newValue) && newValue >= 0) {
      dispatch(
        setInputFieldData({
          fieldName: "members_of_fpo_others",
          value,
        })
      );
      updateTotalFarmersSupplying(
        members_of_fpo_small,
        members_of_fpo_medium,
        newValue,
        non_members_small,
        non_members_medium,
        non_members_others
      );
    }
  };

  const handleNonMemFPOSmall = (value) => {
    const newValue = parseFloat(value);
    if (!isNaN(newValue) && newValue >= 0) {
      dispatch(
        setInputFieldData({
          fieldName: "non_members_small",
          value,
        })
      );
      updateTotalFarmersSupplying(
        members_of_fpo_small,
        members_of_fpo_medium,
        members_of_fpo_others,
        newValue,
        non_members_medium,
        non_members_others
      );
    }
  };

  const handleNonMemFPOMedium = (value) => {
    const newValue = parseFloat(value);
    if (!isNaN(newValue) && newValue >= 0) {
      dispatch(
        setInputFieldData({
          fieldName: "non_members_medium",
          value,
        })
      );
      updateTotalFarmersSupplying(
        members_of_fpo_small,
        members_of_fpo_medium,
        members_of_fpo_others,
        non_members_small,
        newValue,
        non_members_others
      );
    }
  };

  const handleNonMemFPOOthers = (value) => {
    const newValue = parseFloat(value);
    if (!isNaN(newValue) && newValue >= 0) {
      dispatch(
        setInputFieldData({
          fieldName: "non_members_others",
          value,
        })
      );
      updateTotalFarmersSupplying(
        members_of_fpo_small,
        members_of_fpo_medium,
        members_of_fpo_others,
        non_members_small,
        non_members_medium,
        newValue
      );
    }
  };
  return (
    <div>
      <div className="row g-3">
        <div className="col-md-4" style={{ paddingRight: 0 }}>
          <MultiSelect
            label="Crops"
            data={props.existingmagentCrop}
            errorText={
              validationErrors.existing_crops.length !== 0 ? true : false
            }
            value={utilizationTPDThree.existing_crops}
            onChange={handleMultiMagnetCropChange}
          />
        </div>

        <div className="col-md-4">
          <Box sx={{ minWidth: "100%" }}>
            <FormControl
              fullWidth
              //   error={errorWorkpackage !== "" ? true : false}
            >
              <InputLabel htmlFor="demo-simple-select-label">
                Is this facility in operation
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                errorText={
                  validationErrors.is_facility_in_opration.length !== 0
                    ? true
                    : false
                }
                value={utilizationTPDThree.is_facility_in_opration}
                label="Is this facility in operation"
                onChange={handleOptionFacilityOpr}
              >
                {FacilityOpr.map((e) => (
                  <MenuItem value={e.Status_mode}>{e.Status_mode}</MenuItem>
                ))}
              </Select>
              {/* <FormHelperText>{errorWorkpackage}</FormHelperText> */}
            </FormControl>
          </Box>
        </div>

        <div className="col-md-4">
          <Box sx={{ minWidth: "100%" }}>
            <FormControl
              fullWidth
              //   error={errorWorkpackage !== "" ? true : false}
            >
              <InputLabel htmlFor="demo-simple-select-label">
                Used by (MSAMB / FPO / VCO/ Others)
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                errorText={
                  validationErrors.use_by_msamb_type.length !== 0 ? true : false
                }
                value={utilizationTPDThree.use_by_msamb_type}
                label="Used by (MSAMB / FPO / VCO/ Others)"
                onChange={handleOptionUsedBy}
              >
               
                {UsedBy.map((e) => (
                  <MenuItem value={e.Status_mode}>{e.Status_mode}</MenuItem>
                ))}
              </Select>
              {/* <FormHelperText>{errorWorkpackage}</FormHelperText> */}
            </FormControl>
          </Box>
        </div>

        <InputField
          label="Total Qty Handled - TPD (MT)"
          type="number"
          onChange={handleTotalQtyHandTPD}
          value={
            total_quantity_handled_tpd
              ? total_quantity_handled_tpd.toString()
              : ""
          }
          error={!!validationErrors.total_quantity_handled_tpd}
          helperText={validationErrors.total_quantity_handled_tpd}
        />

        {/* Prefill fields */}
        {/* <InputField
          label="Total Capacity  - TPD"
          type="number"
          //   onChange={handleTUCPackHouse}
          //   value={tuc_pack_house ? tuc_pack_house.toString() : ""}
          //   error={!!validationErrors.tuc_pack_house}
          //   helperText={validationErrors.tuc_pack_house}
        /> */}

        <InputField
          label="Total Capacity Utilised - TPD (In MT) "
          type="number"
          onChange={handleTotalCapacityUtilized}
          value={
            total_capacity_utilised_tpd
              ? total_capacity_utilised_tpd.toString()
              : ""
          }
          error={!!validationErrors.total_capacity_utilised_tpd}
          helperText={validationErrors.total_capacity_utilised_tpd}
        />

        <InputField
          label="Capacity utilised for Job work (In MT)"
          type="number"
          onChange={handleCapacityUtilisedJobWork}
          value={
            capacity_utilised_job_work
              ? capacity_utilised_job_work.toString()
              : ""
          }
          error={!!validationErrors.capacity_utilised_job_work}
          helperText={validationErrors.capacity_utilised_job_work}
        />
        <InputField
          label="Capacity utilised for Captive used (In MT)"
          type="number"
          onChange={handleCapacityUtilisedCapativeUsed}
          value={
            capacity_utilised_captive_used
              ? capacity_utilised_captive_used.toString()
              : ""
          }
          error={!!validationErrors.capacity_utilised_captive_used}
          helperText={validationErrors.capacity_utilised_captive_used}
        />
        <InputField
          label="Total Amount(In Rs.)"
          type="number"
          onChange={handleTotalAmount}
          value={total_amount ? total_amount.toString() : ""}
          error={!!validationErrors.total_amount}
          helperText={validationErrors.total_amount}
        />
        <InputField
          label="Service/Rental Charges (In Rs.)"
          type="number"
          onChange={handleserviceRentalCharges}
          value={
            service_rental_charges ? service_rental_charges.toString() : ""
          }
          error={!!validationErrors.service_rental_charges}
          helperText={validationErrors.service_rental_charges}
        />
        <InputField
          label="Processing Charges (In Rs.)"
          type="number"
          onChange={handleProcessingCharges}
          value={processing_charges ? processing_charges.toString() : ""}
          error={!!validationErrors.processing_charges}
          helperText={validationErrors.processing_charges}
        />

        {/* calculated field ///Nos of Farmers supplying/// */}
        <InputField
          readOnly
          label="Nos of Farmers supplying"
          type="number"
          value={number_of_farmers ? number_of_farmers.toString() : ""}
        />

        <div class="row g-3">
          <div className="col-md-3 input1Heading">
            <span>Members of FPO</span>
          </div>
          <InputField
            col="3"
            label="Small"
            type="number"
            onChange={handleMemFPOSmall}
            value={members_of_fpo_small ? members_of_fpo_small.toString() : ""}
            error={!!validationErrors.members_of_fpo_small}
            helperText={validationErrors.members_of_fpo_small}
          />

          <InputField
            col="3"
            label="Medium"
            type="number"
            onChange={handleMemFPOMedium}
            value={
              members_of_fpo_medium ? members_of_fpo_medium.toString() : ""
            }
            error={!!validationErrors.members_of_fpo_medium}
            helperText={validationErrors.members_of_fpo_medium}
          />
          <InputField
            col="3"
            label="Others"
            type="number"
            onChange={handleMemFPOOthers}
            value={
              members_of_fpo_others ? members_of_fpo_others.toString() : ""
            }
            error={!!validationErrors.members_of_fpo_others}
            helperText={validationErrors.members_of_fpo_others}
          />

          <div className="col-md-3 input1Heading">
            <span>Non Members/ Traders or other Corporates</span>
          </div>
          <InputField
            col="3"
            label="Small"
            type="number"
            onChange={handleNonMemFPOSmall}
            value={non_members_small ? non_members_small.toString() : ""}
            error={!!validationErrors.non_members_small}
            helperText={validationErrors.non_members_small}
          />
          <InputField
            col="3"
            label="Medium"
            type="number"
            onChange={handleNonMemFPOMedium}
            value={non_members_medium ? non_members_medium.toString() : ""}
            error={!!validationErrors.non_members_medium}
            helperText={validationErrors.non_members_medium}
          />
          <InputField
            col="3"
            label="Others"
            type="number"
            onChange={handleNonMemFPOOthers}
            value={non_members_others ? non_members_others.toString() : ""}
            error={!!validationErrors.non_members_others}
            helperText={validationErrors.non_members_others}
          />
        </div>
      </div>
    </div>
  );
};
