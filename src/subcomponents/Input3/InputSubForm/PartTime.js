import { InputField } from "../../InputField";
import React from "react";
import "../../../assets/css/inputForm.css";

import { useSelector, useDispatch } from "react-redux";
// import { setInputFieldData } from '../../redux/slice/technicalCivilSlice';
import { usePermify } from "@permify/react-role";

export function PartTime(props) {
  const { user } = usePermify();

  return (
    <>
      <div className="componentSub">
        <div class="row g-3">
          <div className="col-md-3 input1Heading">
            <span>SC</span>
          </div>
          <InputField col="3" label="Male" type="number" />
          <InputField col="3" label="Female" type="number" />
          <InputField col="3" label="Total SC" type="number" />
          <div className="col-md-3 input1Heading">
            <span>ST</span>
          </div>
          <InputField col="3" label="Male" type="number" />
          <InputField col="3" label="Female" type="number" />
          <InputField col="3" label="Total ST" type="number" />
          <div className="col-md-3 input1Heading">
            <span>PWD</span>
          </div>
          <InputField col="3" label="Male" type="number" />
          <InputField col="3" label="Female" type="number" />
          <InputField col="3" label="Total PWD" type="number" />
          <div className="col-md-3 input1Heading">
            <span>BPL</span>
          </div>
          <InputField col="3" label="Male" type="number" />
          <InputField col="3" label="Female" type="number" />
          <InputField col="3" label="Total BPL" type="number" />
          <div className="col-md-3 input1Heading">
            <span>General (Open, OBC and others)</span>
          </div>
          <InputField col="3" label="Male" type="number" />
          <InputField col="3" label="Female" type="number" />
          <InputField col="3" label="Total OBC/ Minority/ Open" type="number" />

          <div className="col-md-3 input1Heading">
            <span>Nos.of full time Employment</span>
          </div>
          <InputField col="3" label="Male" type="number" />
          <InputField col="3" label="Female" type="number" />
          <InputField
            col="3"
            label="Total part time Employment"
            type="number"
          />

          <div className="word-with-line">
            <span>Total Employment Generated</span>
            <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
          </div>
          <InputField col="3" label="Male" type="number" />
          <InputField col="3" label="Female" type="number" />
          <InputField
            col="3"
            label="Total Full time & Part Time"
            type="number"
          />
          <div
            class="col-md-6"
            style={{ position: "relative", right: "15px", bottom: "10px" }}
          >
            <label for="Text" class="col-lg col-form-label">
              Upload Supporting document (e.g QPR, Photos etc){" "}
            </label>
            <div class="col-sm-10">
              <input class="form-control" type="file" id="formFile" />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
export default PartTime;
