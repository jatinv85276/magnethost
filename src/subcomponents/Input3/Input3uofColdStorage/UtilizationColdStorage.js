import { Box, FormControl, InputLabel, MenuItem, Select } from "@mui/material";
import { DatePickInput3 } from "../../DatePickInput3";
import { InputField } from "../../InputField";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import axios from "axios";
import {
  resetValidation,
  setInputFieldData,
} from "../../../redux/slice/InputThree/Input3PhysicalFinance/PhysicalFinanceSlice";
import { getConfigWithToken } from "../../../utils/Config/Config";
import { MultiSelect } from "../../MultiSelect";

export const UtilizationColdStorage = (props) => {
  const apiUrl = process.env.REACT_APP_API_URL;
  const dispatch = useDispatch();

  return (
    <div>
      <div className="row g-3">
        {/* <div className="col-md-4" style={{ paddingRight: 0 }}>
          <MultiSelect
            label="Target Crops"
            data={props.magentCrop}
            errorText={
              validationErrors.magnet_crop_id.length !== 0 ? true : false
            }
            // value={beneficiaryLossMaster.magnet_crop_id}
            value={beneficiaryLossMaster.magnet_crop_id}
            onChange={handleMultiMagnetCropChange}
          />
        </div> */}
        <div className="col-md-4">
          <Box sx={{ minWidth: "100%" }}>
            <FormControl
              fullWidth
              //   error={errorWorkpackage !== "" ? true : false}
            >
              <InputLabel htmlFor="demo-simple-select-label">Crop</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                // value={selectWorkPackage}
                label="Crop"
                // onChange={handleApplicantChangeWorkPackage}
              >
                {/* {workPackage.map((e) => (
                  <MenuItem value={e.id} key={e.id}>
                    {e["work_package"]}
                  </MenuItem>
                ))} */}
              </Select>
              {/* <FormHelperText>{errorWorkpackage}</FormHelperText> */}
            </FormControl>
          </Box>
        </div>
        <div className="col-md-4">
          <Box sx={{ minWidth: "100%" }}>
            <FormControl
              fullWidth
              //   error={errorWorkpackage !== "" ? true : false}
            >
              <InputLabel htmlFor="demo-simple-select-label">
                Is this facility in operation
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                // value={selectWorkPackage}
                label="Is this facility in operation"
                // onChange={handleApplicantChangeWorkPackage}
              >
                {/* {workPackage.map((e) => (
                  <MenuItem value={e.id} key={e.id}>
                    {e["work_package"]}
                  </MenuItem>
                ))} */}
              </Select>
              {/* <FormHelperText>{errorWorkpackage}</FormHelperText> */}
            </FormControl>
          </Box>
        </div>
        <div className="col-md-4">
          <Box sx={{ minWidth: "100%" }}>
            <FormControl
              fullWidth
              //   error={errorWorkpackage !== "" ? true : false}
            >
              <InputLabel htmlFor="demo-simple-select-label">
                Used by (MSAMB / FPO / VCO/ Others)
              </InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                // value={selectWorkPackage}
                label="Used by (MSAMB / FPO / VCO/ Others)"
                // onChange={handleApplicantChangeWorkPackage}
              >
                {/* multiple */}
                {/* {workPackage.map((e) => (
                  <MenuItem value={e.id} key={e.id}>
                    {e["work_package"]}
                  </MenuItem>
                ))} */}
              </Select>
              {/* <FormHelperText>{errorWorkpackage}</FormHelperText> */}
            </FormControl>
          </Box>
        </div>
        <InputField
          label="Total Qty Handled - coldStorage (MT)"
          type="number"
          //   onChange={handleTUCPackHouse}
          //   value={tuc_pack_house ? tuc_pack_house.toString() : ""}
          //   error={!!validationErrors.tuc_pack_house}
          //   helperText={validationErrors.tuc_pack_house}
        />
        <InputField
          label="Total Capacity  - coldStorage"
          type="number"
          //   onChange={handleTUCPackHouse}
          //   value={tuc_pack_house ? tuc_pack_house.toString() : ""}
          //   error={!!validationErrors.tuc_pack_house}
          //   helperText={validationErrors.tuc_pack_house}
        />
        <InputField
          label="Total Capacity Utilised - coldStorage (In MT) "
          type="number"
          //   onChange={handleTUCPackHouse}
          //   value={tuc_pack_house ? tuc_pack_house.toString() : ""}
          //   error={!!validationErrors.tuc_pack_house}
          //   helperText={validationErrors.tuc_pack_house}
        />
        <InputField
          label="Capacity utilised for Job work (In MT)"
          type="number"
          //   onChange={handleTUCPackHouse}
          //   value={tuc_pack_house ? tuc_pack_house.toString() : ""}
          //   error={!!validationErrors.tuc_pack_house}
          //   helperText={validationErrors.tuc_pack_house}
        />
        <InputField
          label="Capacity utilised for Captive used (In MT)"
          type="number"
          //   onChange={handleTUCPackHouse}
          //   value={tuc_pack_house ? tuc_pack_house.toString() : ""}
          //   error={!!validationErrors.tuc_pack_house}
          //   helperText={validationErrors.tuc_pack_house}
        />
        <InputField
          label="Total Amount(In Rs.)"
          type="number"
          //   onChange={handleTUCPackHouse}
          //   value={tuc_pack_house ? tuc_pack_house.toString() : ""}
          //   error={!!validationErrors.tuc_pack_house}
          //   helperText={validationErrors.tuc_pack_house}
        />
        <InputField
          label="Service/Rental Charges (In Rs.)"
          type="number"
          //   onChange={handleTUCPackHouse}
          //   value={tuc_pack_house ? tuc_pack_house.toString() : ""}
          //   error={!!validationErrors.tuc_pack_house}
          //   helperText={validationErrors.tuc_pack_house}
        />
        <InputField
          label="Processing Charges (In Rs.)"
          type="number"
          //   onChange={handleTUCPackHouse}
          //   value={tuc_pack_house ? tuc_pack_house.toString() : ""}
          //   error={!!validationErrors.tuc_pack_house}
          //   helperText={validationErrors.tuc_pack_house}
        />

        {/* calculated field ///Nos of Farmers supplying/// */}
        <InputField
          label="Nos of Farmers supplying"
          type="number"
          //   onChange={handleTUCPackHouse}
          //   value={tuc_pack_house ? tuc_pack_house.toString() : ""}
          //   error={!!validationErrors.tuc_pack_house}
          //   helperText={validationErrors.tuc_pack_house}
        />
        <div className="col-md-3 input1Heading">
          <span>Members of FPO</span>
        </div>
        <InputField
          col="3"
          label="Small"
          type="number"
        //   value={capacityBuilding.obc_minority_open_male}
          // onChange={handleFieldChangeOBCMale}
        //   onChange={(value) =>
        //     handleInputChange("obc_minority_open_male", value)
        //   }
        //   onFocus={handleResetFocus}
        //   error={!!capacityBuilding.validationErrors.obc_minority_open_male}
        //   helperText={capacityBuilding.validationErrors.obc_minority_open_male}
        />
        <InputField
          col="3"
          label="Medium"
          type="number"
        //   value={capacityBuilding.obc_minority_open_male}
          // onChange={handleFieldChangeOBCMale}
        //   onChange={(value) =>
        //     handleInputChange("obc_minority_open_male", value)
        //   }
        //   onFocus={handleResetFocus}
        //   error={!!capacityBuilding.validationErrors.obc_minority_open_male}
        //   helperText={capacityBuilding.validationErrors.obc_minority_open_male}
        />
        <InputField
          col="3"
          label="Others"
          type="number"
        //   value={capacityBuilding.obc_minority_open_male}
          // onChange={handleFieldChangeOBCMale}
        //   onChange={(value) =>
        //     handleInputChange("obc_minority_open_male", value)
        //   }
        //   onFocus={handleResetFocus}
        //   error={!!capacityBuilding.validationErrors.obc_minority_open_male}
        //   helperText={capacityBuilding.validationErrors.obc_minority_open_male}
        />

        <div className="col-md-3 input1Heading">
          <span>Non Members/ Traders or other Corporates</span>
        </div>
        <InputField
          col="3"
          label="Small"
          type="number"
        //   value={capacityBuilding.obc_minority_open_male}
          // onChange={handleFieldChangeOBCMale}
        //   onChange={(value) =>
        //     handleInputChange("obc_minority_open_male", value)
        //   }
        //   onFocus={handleResetFocus}
        //   error={!!capacityBuilding.validationErrors.obc_minority_open_male}
        //   helperText={capacityBuilding.validationErrors.obc_minority_open_male}
        />
        <InputField
          col="3"
          label="Medium"
          type="number"
        //   value={capacityBuilding.obc_minority_open_male}
          // onChange={handleFieldChangeOBCMale}
        //   onChange={(value) =>
        //     handleInputChange("obc_minority_open_male", value)
        //   }
        //   onFocus={handleResetFocus}
        //   error={!!capacityBuilding.validationErrors.obc_minority_open_male}
        //   helperText={capacityBuilding.validationErrors.obc_minority_open_male}
        />
        <InputField
          col="3"
          label="Others"
          type="number"
        //   value={capacityBuilding.obc_minority_open_male}
          // onChange={handleFieldChangeOBCMale}
        //   onChange={(value) =>
        //     handleInputChange("obc_minority_open_male", value)
        //   }
        //   onFocus={handleResetFocus}
        //   error={!!capacityBuilding.validationErrors.obc_minority_open_male}
        //   helperText={capacityBuilding.validationErrors.obc_minority_open_male}
        />
      </div>
    </div>
  );
};
