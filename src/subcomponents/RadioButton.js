import Radio from '@mui/material/Radio';
import FormControlLabel from '@mui/material/FormControlLabel';
export const RadioButton = (props) =>{
    return(<>
        <div className={`col-md-2`}>
                <FormControlLabel value={props.value} control={<Radio />} label={props.label} />
        </div>
    </>)
}