import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';

export function SelectField(props) {

  return (
    <Box sx={{ minWidth: "100%" }}>
      <FormControl fullWidth>
        <InputLabel htmlFor="demo-simple-select-label">{props.label}</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={props.value}
          label="Age"
          onChange={props.onChange}
        >
          {(props.dropValue).map((e)=>(
            <MenuItem value={e.id} key={e.id}>{e["applicant_name"]}</MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
    // <Box sx={{ minWidth: "100%" }}>
    //   <FormControl fullWidth>
    //     <InputLabel htmlFor="quarter-select-label">Quarter</InputLabel>
    //     <Select
    //       labelId="quarter-select-label"
    //       id="quarter-select"
    //       value={age}
    //       onChange={handleChange}
    //     >
    //       {props.selectData.map((quarter, index) => (
    //         <MenuItem key={index} value={quarter}>
    //           {quarter}
    //         </MenuItem>
    //       ))}
    //     </Select>
    //   </FormControl>
    // </Box>
  );
}