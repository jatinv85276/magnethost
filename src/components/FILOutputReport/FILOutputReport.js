import React, { useEffect, useMemo, useState } from "react";
import { useTable } from "react-table";
import "../../assets/css/OutputTable.css";
import {
  Box,
  CircularProgress,
  FormControl,
  InputLabel,
  Button,
  MenuItem,
  Select,
} from "@mui/material";
import axios from "axios";
import Swal from "sweetalert2";
import { getConfigWithToken } from "../../utils/Config/Config";
import AxiosRetry from "axios-retry";
import * as XLSX from "xlsx";

export const FilOutputReport = ({
  setDivision,
  financialYear,
  currentFinancialYear,
  currentQuarter,
}) => {
  const apiUrl = process.env.REACT_APP_API_URL;
  const [loading, setLoading] = useState(true);
  const [divisions, setDivisions] = useState([]);
  const [selectDivision, setSelectDivision] = useState([]);
  const [selectFinnacialYear, setSelectFinancialYear] = useState("2023-2024");
  const [selectSubFilter, setSelectSubFilter] = useState("Q3");
  const [tableData, setTableData] = useState([]);

  // Create an Axios instance with retry settings
  const axiosInstance = axios.create({
    baseURL: apiUrl,
    timeout: 15000, // Set a timeout for requests
  });

  // Apply retry settings to the instance
  AxiosRetry(axiosInstance, {
    retries: 3, // Number of retry attempts
    retryDelay: AxiosRetry.exponentialDelay, // Exponential backoff
  });

  // Function for Handle Division selection
  const handleSelectDivision = async (e) => {
    setSelectDivision(e.target.value);
    try {
      const divisionData = await axios.get(`${apiUrl}api/getFillInTwo?division_id=${e.target.value}`, getConfigWithToken());
      const data = divisionData.data.data.data.map((e) => {
        return {
          ...e,
        };
      });
       setTableData(data); 
    } catch (error) {
      console.error('Error fetching division data:', error);
    }
  };
  
  useEffect(() => {
    async function getData() {
      try {
        const getAllUser = await axios.get(`${apiUrl}api/getFillInTwo`, getConfigWithToken());
        const getdb = getAllUser.data.data.data.map((e) => {
          return {
            ...e,
          };
        });
        setTableData(getdb); 
        setDivisions(setDivision);
        setLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
        if (error.response && error.response.status === 429) {
          Swal.fire("An refresh the Page", "", "error");
        } else {
          throw new Error("Data Not Found");
        }
      }
    }
  
    setTimeout(() => {
      getData();
    }, 2000);
  }, [setDivision, financialYear, currentFinancialYear, currentQuarter]);
  

  // creating Header and rows for table
  const columns = useMemo(
    () => [
      {
        Header: "Name of the Borrower",
        accessor: "applicant_name",
      },

      {
        Header: "Division",
        accessor: "divisions",
      },
      {
        Header: "District",
        accessor: "district",
      },
      {
        Header: "Taluka",
        accessor: "taluka",
      },
      {
        Header: "Entity Type",
        accessor: "entity_type",
      },
      {
        Header: "Bank",
        accessor: "bank_name",
      },
      {
        Header: "Existing Magnet Crops",
        accessor: "existing_crop_id",
        Cell: ({ value }) => {
          const crops = JSON.parse(value);
          return crops.map(crop => crop.name).join(", ");
        },
      },
      {
        Header: "Magnet Crops",
        accessor: "magnet_crop_id",
        Cell: ({ value }) => {
          const crops = JSON.parse(value);
          return crops.map(crop => crop.name).join(", ");
        },
      },
      {
        Header: "Exisiting Infrastructure",
        accessor: "existing_infrastructure",
      },
      {
        Header: "Type of Assistence",
        accessor: "type_of_assistance",
      },
      {
        Header: "Purpose of Loan",
        accessor: "purpose_of_loan",
      },
      {
        Header: "Loan Amount (INR Lakh)",
        accessor: "loan_amount",
      },
      {
        Header: "Interest Rate %",
        accessor: "interest_rate_pct",
      },
      {
        Header: "Processing Fee %",
        accessor: "processing_fee_pct",
      },
      {
        Header: "Sanction Date",
        accessor: "sanction_date",
      },
      {
        Header: "Disbursed Amount",
        accessor: "disbursed_amount",
      },
      {
        Header: "Outstanding Amount",
        accessor: "outstanding_amount",
      },
      {
        Header: "Disbursement Date",
        accessor: "disbursement_date",
      },
      {
        Header: "Revised Interest Rate",
        accessor: "revised_interest_rate",
      },
      {
        Header: "Addendum Date",
        accessor: "addendum_date",
      },
    ],
    []
  );

  // Calculate the total width for equally sized columns
  const totalWidthForEqualColumns = 800; 

  // Calculate the width for each equally sized column
  const equalColumnWidth = totalWidthForEqualColumns / (columns.length - 1);

  // Set the width for each column (excluding "Components")
  columns.forEach((column, index) => {
    if (index !== 0) {
      column.width = equalColumnWidth;
    }
  });

 

  
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable({
      columns,
      data: tableData, 
    });

  const cellStyle = {
    padding: "8px",
    border: "1px solid #ccc",
    textAlign: "center", // Center text horizontally
    verticalAlign: "middle", // Center text vertically
  };


  // Function for Data export to csv
const handleExport = () => {
  const selectedDivision = divisions.find(division => division.id === selectDivision);
  const divisionName = selectedDivision ? selectedDivision.division_name : 'All';

  let formattedData;

  if (tableData.length === 0) {
    // Create a single row with only headers if there's no data
    formattedData = [{
      "Financial Year": selectFinnacialYear,
      "Period": selectSubFilter,
      ...(divisionName !== 'All' && { "Division": divisionName })
    }];

    columns.forEach(column => {
      formattedData[0][column.Header] = ""; // Empty value for the headers
    });
  } else {
    // Map table data and conditionally add the "Division" column
    formattedData = tableData.map(row => {
      let formattedRow = {};

      if (divisionName !== 'All') {
        formattedRow["Division"] = divisionName;
      }

      columns.forEach(column => {
        let cellValue = row[column.accessor];

        // Handle JSON fields for magnet_crop_id and existing_crop_id
        if (column.accessor === "magnet_crop_id" || column.accessor === "existing_crop_id") {
          try {
            const crops = JSON.parse(cellValue);
            cellValue = crops.map(crop => crop.name).join(", ");
          } catch (error) {
            console.error(`Error parsing JSON for column ${column.accessor}:`, error);
          }
        }

        formattedRow[column.Header] = cellValue;
      });

      return formattedRow;
    });
  }

  // Create worksheet and workbook
  const worksheet = XLSX.utils.json_to_sheet(formattedData);
  const workbook = XLSX.utils.book_new();
  XLSX.utils.book_append_sheet(workbook, worksheet, "Table Data");

  const fileName = `${divisionName}_FilReport.xlsx`;
  XLSX.writeFile(workbook, fileName);
};


  if (loading) {
    return (
      <>
        <Box sx={{ display: "flex" }}>
          <CircularProgress />
        </Box>
      </>
    );
  }
  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
          marginBottom: "10px",
        }}
      >
        <div className="col-md-5"></div>
        <div className="col-md-7">
          <div className="row">
          <div className="col-md-3"></div>
          <div className="col-md-3"></div>
            <div className="col-md-3">
              <Box>
                <FormControl fullWidth>
                  <InputLabel htmlFor="demo-simple-select-label">
                    Division
                  </InputLabel>
                  <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={selectDivision}
                    label="Division"
                    onChange={handleSelectDivision}
                  >
                    {divisions.map((e) => (
                      <MenuItem value={e.id} key={e.id}>
                        {e.division_name}
                      </MenuItem>
                    ))}
                  </Select>
                </FormControl>
              </Box>
            </div>
            <div className="col-md-3">
              <Box sx={{ width: "95%" }}>
                <Button
                  onClick={handleExport}
                  style={{
                    color: "#4e2683",
                    fontWeight: "bold",
                    fontSize: "13.4px",
                    height: "53px",
                    width: "155px",
                    border: "0.9px solid #4e2683",
                    marginLeft: "-9px",
                  }}
                >
                  Export
                </Button>
              </Box>
            </div>
          </div>
        </div>
      </div>
      <div style={{ width: "100%", overflowX: "auto" }}>
        <table {...getTableProps()} className="table">
          <thead>
            {headerGroups.map((headerGroup) => (
              <tr {...headerGroup.getHeaderGroupProps()}>
                {headerGroup.headers.map((column) => (
                  <th
                    {...column.getHeaderProps()}
                    style={{
                      borderBottom: "2px solid #ccc",
                      background: "#f2f2f2", // Add grey background color
                      fontWeight: "bold", // Optionally make text bold
                      border: "2px solid #ccc",
                      ...cellStyle, // Add border style here
                    }}
                  >
                    {column.render("Header")}
                  </th>
                ))}
              </tr>
            ))}
          </thead>
          <tbody {...getTableBodyProps()}>
            {rows.map((row, index) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell, cellIndex) => {
                    return (
                      <td
                        {...cell.getCellProps()}
                        style={{
                          padding: "8px",
                          border: "1px solid #ccc",
                          ...cellStyle,
                        }}
                      >
                        {cellIndex === 0 && index === 0 ? (
                          <span>{cell.value}</span> // Display the applicant name only once
                        ) : (
                          cell.render("Cell")
                        )}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </>
  );
};
