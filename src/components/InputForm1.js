/* eslint-disable default-case */
import '../assets/css/inputForm.css'
import { DatePick } from "../subcomponents/DatePick"
import { InputField } from "../subcomponents/InputField"
import pdf from '../assets/PDF_file_icon.svg.png'
import imgIcon from '../assets/image_icon-icons.com_50366.png'
import React, { useState } from 'react';
import { setValidationErrorsWorkshop, resetDataWorkShop, updateWorkShopForm } from '../redux/slice/InputOne/WorkShop/WorkShopSlice'
import { setValidationErrorsPostHarvest, resetDataPostHarvest, updatePostHarvestForm } from '../redux/slice/InputOne/PostHarvestTrainingSlice/PostHarvestSlice'
import { setValidationErrorsValueChain, resetDataValueChain, updateValueChainForm } from '../redux/slice/InputOne/ValueChain/ValueChainSlice'
import { setValidationErrorsHighDensity, resetDataHighDensity, updateHighDensityForm } from '../redux/slice/InputOne/HighDensity/HighDensitySlice'
import { setValidationErrorsCapacityBuilding, resetDataCapacityBuilding, updateCapacityBuildingForm } from '../redux/slice/InputOne/CapacityBuilding/CapacityBuildingSlice'
import { setValidationErrorsSMELinkage, resetDataSMELinkage, updateSMELinkageForm } from '../redux/slice/InputOne/SMELinkage/smeLinkageSlice'
import { setValidationErrorsMarketAssist, resetDataMarketAssist, updateMarketAssistForm } from '../redux/slice/InputOne/MarketAssistence/MarketAssistenceSlice'
import { setValidationErrorsOtherCerti, resetDataOtherCerti, updateOtherCertiForm} from '../redux/slice/InputOne/OtherCertification/OtherCertificationSlice'
import { updateApiDataToNursery, resetDataNursery, setValidationErrorsNursery } from '../redux/slice/InputOne/NurseryDevelopment/NurseryDevSlice';
import { updateApiDataToTissue, resetDataTissue, setValidationErrorsTissue } from '../redux/slice/InputOne/TissueCulture/TissueCultureSlice';
import { updateApiDataToSupportTech, resetDataSupportTech, setValidationErrorsSupportTech } from '../redux/slice/InputOne/SupportTechnology/SupportTechSlice';
import { updateApiDataToResearch, resetDataResearch, setValidationErrorsResearch } from '../redux/slice/InputOne/ResearchDev/researchDevSlice';
import { updateProductionPlantForm, resetDataProductionPlant, setValidationErrorsProductionPlant } from '../redux/slice/InputOne/ProductionPlant/ProductionPlantSlice';
import { updateApiDataToInnovation, resetDataInnovation, setValidationErrorsInnovation } from '../redux/slice/InputOne/InnovationPack/InnovationSlice';
import { updateApiDataTobiowaste, resetDatabiowaste, setValidationErrorsbiowaste } from '../redux/slice/InputOne/BioWaste/BioWasteSlice';
import { updateApiDataToIntro, resetDataIntro, setValidationErrorsIntro } from '../redux/slice/InputOne/IntoToVariety/IntroVarietySlice';
import { updateApiDataToStudy, resetDataStudy, setValidationErrorsStudy } from '../redux/slice/InputOne/StudyTourExposure/StudyTourExposureSlice';
import { updateApiDataToFarmer, resetDataFarmer, setValidationErrorsFarmer } from '../redux/slice/InputOne/StudyTourFarmer/StudyTourFarmerSlice';
import { updateApiDataTradeFair, resetDataTradeFair, setValidationErrorsTradeFair } from '../redux/slice/InputOne/TradeFairExhibition/TradeFairSlice';
import { updateApiDataMarketProm, resetDataMarketProm, setValidationErrorsMarketProm } from '../redux/slice/InputOne/MarketPromotion/MarketPromotionSlice';
import { updateTotTrainingForm, resetDataTotTraining, setValidationErrorsTotTraining } from '../redux/slice/InputOne/TotTraining/TotTrainingSlice';
import { TabContext, TabPanel } from '@mui/lab';
import { Box, CircularProgress, FormControl, FormHelperText, InputLabel, MenuItem, Select, Tab, Tabs } from '@mui/material';
import { useEffect } from 'react';
import { Offline, Online } from 'react-detect-offline';
import axios from 'axios';
import { getConfigWithToken } from '../utils/Config/Config';
import { useDispatch, useSelector } from 'react-redux';
import Swal from 'sweetalert2';
import { Workshop } from '../subcomponents/Input1/Input1Workshop/Workshop';
import { setInputFieldData } from '../redux/slice/InputTwo/MasterSubSlice';
import { PHMtraining } from "../subcomponents/Input1/Input1PostHarvest/PHMTraining";
import { GapTraining } from "../subcomponents/Input1/Input1CapacityBuilding/TrainingOnGap";
import { MarketAssistence } from '../subcomponents/Input1/Input1MarketAssistence/MarketAssistence'
import { WomenLedtraining } from "../subcomponents/Input1/Input1ValueChain/TrainingOfWomenLed";
import { TotTraining } from '../subcomponents/Input1/Input1Tot/TotTraining';
import { NurseryMasterForm } from '../subcomponents/Input1/Input1NurseryMaster/NurseryMasterForm'
import { MultiSelect } from '../subcomponents/MultiSelect';
import { HighDensity } from '../subcomponents/Input1/Input1HDP/HighDensity';
import { SMELinkage } from '../subcomponents/Input1/Input1SMELinkage/SMELinkage';
import { OtherCertification } from '../subcomponents/Input1/Input1OtherCertificateCost/OtherCertificateCost';
import { ProductionPlant } from '../subcomponents/Input1/Input1ProductionPlant/ProductionPlant';
import { TissueCulture } from '../subcomponents/Input1/Input1TissueCulture/TissueCulture'
import { ResearchDev } from '../subcomponents/Input1/Input1ResearchDev/ResearchDev'
import { InnovationPackaging } from '../subcomponents/Input1/Input1InnovationPackaging/InnovationPackaging'
import { IntroVariety } from '../subcomponents/Input1/Intput1IntroVariety/IntroVariety'
import { GAPPH1, PHM, ValueChain, MarketDevAss, OtherCert, ProdPlant, StudyExpVisit, StudyTourFarm, TradeFairExhibition, MarketDevProm } from '../utils/Input1Components';
import { SupportTechnology } from '../subcomponents/Input1/Input1Support/SupportTechnology'
import { BioWaste } from '../subcomponents/Input1/Input1BioWaste/BioWaste'
import { StudyTourMagnet } from '../subcomponents/Input1/Input1StudyTourMagnet/StudyTourMagnet'
import { StudyTourFarmer } from '../subcomponents/Input1/Input1StudyTourFarmer/StudyTourFarmer'
import { TradeFareExhibition } from '../subcomponents/Input1/Input1TradeFare/TradeFairExhibition'
import { MarketPromotion } from '../subcomponents/Input1/Input1MarketDevProm/MarketPromotion'
  

export const InputForm1 = () =>{

    const apiUrl = process.env.REACT_APP_API_URL;
    const fileUrl = process.env.REACT_APP_INPUT1_API_FILE_URL;

    const dispatch = useDispatch();
    const [loading, setLoading] = useState(true);
    const [value, setValue] = useState('1');
    const [quarters, setQuarter] = useState([])
    const [selectQuarter, setSelectedQuarters] = useState("")
    const [selectedGAP, setSelectedGAP] = useState("");
    const [component, setComponents] = useState([])
    const [selectNurseryComponent, setSelectNurseryComponent] = useState("")
    const [selectTissueComponent, setSelectTissueComponent] = useState("")
    const [selectSupportComponent, setSelectSupportComponent] = useState("")
    const [selectPMU, setSelectPMUId] = useState([])
    const [beneStudyTourFarm, setBeneStudyTourFarm]= useState([])
    const [selectDivision, setSelectDivision] = useState("")
    const [selectKPMGDivision, setSelectKPMGDivision] = useState("")
    const [selectedValueChain, setSelectedValueChain] = useState("");
    const [selectedMarketLevel, setSelectedMarketLevel] = useState("");
    const [selectedOtherCertification, setSelectedOtherCertification] = useState("");
    const [selectedProductionPlant, setSelectedProductionPlant] = useState("");
    const [selectedStudyTourExpVisit, setSelectedStudyTourExpVisit] = useState("");
    const [selectedStudyTourFarmer, setSelectedStudyFarmer] = useState("")
    const [selectedTradeFareExhitbiton, setSelectedTradeFareExhitbiton] = useState("") 
    const [selectedMarketDevProm, setSelectedMarketDevProm] = useState("")
    const [magentCrop, setMagentCrop] = useState([])

    const [workShopFlagEdit, setWorkShopFlagEdit] = useState(false)
    const [workShopIdEdit, setWorkShopIdEdit] = useState("")
    const [postHarvestFlagEdit, setPostHarvestFlagEdit] = useState(false)
    const [postHarvestIdEdit, setPostHarvestIdEdit] = useState("")
    const [capacityBuildingFlagEdit, setCapacityBuildingFlagEdit] = useState(false)
    const [capacityBuildingIdEdit, setcapacityBuildingIdEdit] = useState("")
    const [valueChainFlagEdit, setValueChainFlagEdit] = useState(false)
    const [valueChainIdEdit, setValueChainIdEdit] = useState("")
    const [nurseryFlagEdit, setNurseryFlagEdit] = useState(false)
    const [nurseryIdEdit, setNurseryIdEdit] = useState("")
    const [hdpDensityFlagEdit, setHdpDensityFlagEdit] = useState(false)
    const [hdpDensityIdEdit, setHdpDensityIdEdit] = useState("")
    const [smeLinkageFlagEdit, setSMELinkageFlagEdit] = useState(false)
    const [smeLinkageIdEdit, setSMELinkageIdEdit] = useState("")
    const [marketLevelFlagEdit, setMarketLevelFlagEdit] = useState(false)
    const [marketLevelIdEdit, setMarketLevelIdEdit] = useState("")
    const [otherCertFlagEdit, setOtherCertFlagEdit] = useState(false)
    const [otherCertIdEdit, setOtherCertIdEdit] = useState("")
    const [prodPlantFlagEdit, setProdPlantFlagEdit] = useState(false)
    const [prodPlantIdEdit, setProdPlantIdEdit] = useState("")
    const [researchDevFlagEdit, setResearchDevFlagEdit] = useState(false)
    const [researchDevIdEdit, setResearchDevIdEdit] = useState("")
    const [studyTourFlagEdit, setStudyTourFlagEdit] = useState(false)
    const [studyTourIdEdit, setStudyTourIdEdit] = useState("")
    const [totTrainingFlagEdit, setTotTrainingFlagEdit] = useState(false)
    const [totTrainingIdEdit, setTotTrainingIdEdit] = useState("")
    const [tot_Magnet_crop_id, setTot_Magnet_Crop_Id] = useState([])
    const [nameOfThePerson, setNameOfThePerson] = useState("")

    const [beniFiApplicant, setBeniApplicant] = useState([])
    const [isMatchingGrant, setIsMatchingGrant] = useState("")
    const [capacityActionPoint, setCapacityActionPoint] = useState("")
    const [taluka, setTaluka] = useState([]);
    const [selectbeniFiApplicant, setSelectBeniApplicant] = useState("")
    const [hdpbeneficiary, setHdpbeneficiary] = useState([])
    const [errorSelectBeniApplicant, setErrorSelectBeniApplicant] = useState("")

    // Files State
    const [editInput1Files, setEditInput1Files] = useState([])
    const [selectCapacityFile, setSelectCapacityFile] = useState([])
    const [selectOtherCertificationFile, setSelectOtherCertificationFile] = useState([])
    const [errorCapacityFile, setErrorCapacityFile] = useState("")

    // setting beneficiary
    const [nurseryTissueBeneficiary, setNurseryTissueBeneficiary] = useState([])
    const [PMU_Pune, setPMU_PUNE] = useState([]);
    const [studyFarmApplicant, setStudyFarmApplicant] = useState([])
    const [division, setDivision] = useState([]);
    const [beniById, setBeniById] = useState({})

    const dd = useSelector(setInputFieldData);

    const checkFPOSelect = () =>{
        if(selectbeniFiApplicant.length === 0){
            setErrorSelectBeniApplicant("Please Select Applicant")
        }else{
            return true
        }
        return false
    }

    const resetAllState = () => {
        setSelectNurseryComponent("")
        setSelectTissueComponent("")
        setSelectBeniApplicant("")
        setErrorSelectBeniApplicant("")
        setSelectedGAP("")
        setSelectedOtherCertification("")
        setSelectedProductionPlant("")
        setBeniById({})
        setNurseryTissueBeneficiary([])
        setBeneStudyTourFarm([])
        setHdpbeneficiary([])
        setEditInput1Files([])
        setStudyFarmApplicant([])
        setNurseryFlagEdit(false)
        setResearchDevFlagEdit(false)
        setStudyTourFlagEdit(false)
        dispatch(resetDataResearch())
        dispatch(resetDataSMELinkage())
        dispatch(resetDataWorkShop())
        dispatch(resetDataProductionPlant())
        dispatch(resetDatabiowaste())
        dispatch(resetDataMarketProm())
        dispatch(resetDataTradeFair())
        dispatch(resetDataPostHarvest())
    }

    const handleFieldNameOfPerson = (value) => {
        setNameOfThePerson(value)
    }
    const handleTotMagnetCrop = async (data) => {
        setTot_Magnet_Crop_Id(data)
        try{
            const getDatatot = await axios.get(`${apiUrl}api/tot-inputs-edit?form_id=23&name_of_the_person=${nameOfThePerson}&quarter=${selectQuarter}&magnet_crop_id=${data.join(",")}`,getConfigWithToken());
            if(getDatatot.data["status"] === true){
                getDatatot.data["flag"]?setTotTrainingFlagEdit(true):setTotTrainingFlagEdit(false)
                const data = getDatatot.data["data"][0]
                setEditInput1Files(data["remarks"])
                setTotTrainingIdEdit(data["id"])
                data["nomination_by_division"] = data["nomination_by_division"].split(',')
                dispatch(updateTotTrainingForm(data))
            }else{
                setEditInput1Files([])
                setTotTrainingFlagEdit(false)
                dispatch(resetDataTotTraining())
            }
            
        }catch(err){
            
        }
    }

    const handleChange = async (event, newValue) => {
        resetAllState()
        try{
            setValue(newValue);
            if(newValue === '2'){
                const component = await axios.get(`${apiUrl}api/get-tech-component-by-formid?form_id=4`,getConfigWithToken());
                const componentData = component.data["data"].map((e) =>({"id":e["id"],"technical_component":e["technical_component"]}));
                setComponents(componentData)
                setSelectBeniApplicant("")
            }
            if(newValue === '3'){
                const component = await axios.get(`${apiUrl}api/get-tech-component-by-formid?form_id=5`,getConfigWithToken());
                const componentData = component.data["data"].map((e) =>({"id":e["id"],"technical_component":e["technical_component"]}));
                setComponents(componentData)
            }
            if(newValue === '19'){
                const component = await axios.get(`${apiUrl}api/get-tech-component-by-formid?form_id=26`,getConfigWithToken());
                const componentData = component.data["data"].map((e) =>({"id":e["id"],"technical_component":e["technical_component"]}));
                setComponents(componentData)
            }
            
        }catch(err){

        }
    };

    const handleMultiFilesGAPChange = (event) => {
        setSelectCapacityFile(event.target.files)
    }

    const handleMultiFilesOtherCertifChange = (event) => {
        setSelectOtherCertificationFile(event.target.files)
    }

    const handleFieldChangeActionPoints = (value) => {
        setCapacityActionPoint(value)
    }

    const handleTechComponentChangeChange = async (event) =>{
        try{
            if(checkFPOSelect()){
                if(value === '2'){
                    setSelectNurseryComponent(event.target.value)
                    const getComponentData = await axios.get(`${apiUrl}api/getAllDetailsNurseryDev?beneficiary_id=${selectbeniFiApplicant}&component_id=${event.target.value}&quarter=${selectQuarter}&form_id=4`,getConfigWithToken())
                    if(getComponentData.data["status"]===true){
                        const data = getComponentData.data["data"][0]
            
                        setIsMatchingGrant(data.is_matching_grant)
                        setEditInput1Files(getComponentData.data["remarks"])
                        data["payment_tranche"] = data["tranche"]
                        getComponentData.data["flag"]?setNurseryFlagEdit(true):setNurseryFlagEdit(false)
                        setNurseryIdEdit(data["id"])
                        dispatch(updateApiDataToNursery(data))
                    }else{
                        setNurseryFlagEdit(false)
                        setEditInput1Files([])
                        dispatch(resetDataNursery())
                        const getDataFromBeneficiary = await axios.get(`${apiUrl}api/get_primarydata_by_beneficiary_componentid?beneficiary_id=${selectbeniFiApplicant}&quarter=${selectQuarter}&component_id=${event.target.value}&form_id=4`,getConfigWithToken())
                        const data = getDataFromBeneficiary.data["data"]
                        setIsMatchingGrant(data.is_matching_grant)
            
                        data["payment_tranche"] = data["tranch"]["payment_tranche"]
                        data["tranche_id"] = data["tranch"]["tranche_id"]
            
                        dispatch(updateApiDataToNursery(data))
                    }
                }
                if(value === '3'){
                    setSelectTissueComponent(event.target.value)
                    const getComponentData = await axios.get(`${apiUrl}api/getAllDetailsTissueCulture?beneficiary_id=${selectbeniFiApplicant}&quarter=${selectQuarter}&component_id=${event.target.value}&form_id=5`,getConfigWithToken())
                    if(getComponentData.data["status"]===true){
                        const data = getComponentData.data["data"][0]
            
                        setIsMatchingGrant(data.is_matching_grant) 
                        setEditInput1Files(getComponentData.data["remarks"])
                        data["payment_tranche"] = data["tranche"]
                        getComponentData.data["flag"]?setNurseryFlagEdit(true):setNurseryFlagEdit(false)
                        setNurseryIdEdit(data["id"])
                        dispatch(updateApiDataToTissue(data))
                    }else{
                        setNurseryFlagEdit(false)
                        setEditInput1Files([])
                        dispatch(resetDataTissue())
                        const getDataFromBeneficiary = await axios.get(`${apiUrl}api/get_primarydata_by_beneficiary_componentid?beneficiary_id=${selectbeniFiApplicant}&quarter=${selectQuarter}&component_id=${event.target.value}&form_id=5`,getConfigWithToken())
                        const data = getDataFromBeneficiary.data["data"]
                        setIsMatchingGrant(data.is_matching_grant)
                        dispatch(updateApiDataToTissue(data))
                    }
                }
                if(value === '19'){
                    setSelectSupportComponent(event.target.value)
                    const getComponentData = await axios.get(`${apiUrl}api/support-new-technology-edit?beneficiary_id=${selectbeniFiApplicant}&quarter=${selectQuarter}&new_technology_id=${event.target.value}&form_id=26`,getConfigWithToken())
                    if(getComponentData.data["status"]===true){
                        const data = getComponentData.data["data"][0]
            
                        setIsMatchingGrant(data.is_matching_grant) 
                        setEditInput1Files(data["remarks"])
                        data["payment_tranche"] = data["tranche"]
                        getComponentData.data["flag"]?setNurseryFlagEdit(true):setNurseryFlagEdit(false)
                        setNurseryIdEdit(data["id"])
                        // data["total_technology_cost"] = data["total_project_cost"]
                        dispatch(updateApiDataToSupportTech(data))
                    }else{
                        setNurseryFlagEdit(false)
                        setEditInput1Files([])
                        dispatch(resetDataSupportTech())
                        const getDataFromBeneficiary = await axios.get(`${apiUrl}api/get_primarydata_by_beneficiary_componentid?beneficiary_id=${selectbeniFiApplicant}&quarter=${selectQuarter}&new_technology_id=${event.target.value}&form_id=26`,getConfigWithToken())
                        const data = getDataFromBeneficiary.data["data"]
                        data["payment_tranche"] = data["tranche"]["tranche"]
                        data["tranche_id"] = data["tranche"]["tranche_id"]
                        setIsMatchingGrant(data.is_matching_grant)
                        dispatch(updateApiDataToSupportTech(data))
                    }
                }
            }
        }catch(err){

        }
    }

    const handleDeleteFile = async (id) =>{
        const deleteFile = await axios.get(`${apiUrl}api/remark-delete?id=${id}`,getConfigWithToken())
        if(deleteFile.data["status"]){
            const newRemarks = editInput1Files.filter((remark) => remark.id !== id);

            // Update the state with the new remarks data
            setEditInput1Files({
              ...editInput1Files,
              remarks: newRemarks,
            });
          Swal.fire(
            `File deleted Successfully`,
            '',
            'success'
        )
        }
    }

    const handleOptionsGAP = async (event) => {
        if(checkFPOSelect()){
            setSelectedGAP(event.target.value);
            const getCapacityBuildingEditData = await axios.get(`${apiUrl}api/getAllDetailsCapacityBuildingGap?quarter=${selectQuarter}&beneficiary_id=${selectbeniFiApplicant}&training_type_id=${event.target.value}&form_id=3`,getConfigWithToken())

            if(getCapacityBuildingEditData.data["status"] === true){
                getCapacityBuildingEditData.data["flag"]?setCapacityBuildingFlagEdit(true):setCapacityBuildingFlagEdit(false)
                const data = getCapacityBuildingEditData.data["data"][0]
                setEditInput1Files(getCapacityBuildingEditData.data["remarks"])
                setcapacityBuildingIdEdit(getCapacityBuildingEditData.data["data"][0]["id"])
                data["magnet_crop_id"] = JSON.parse(data["magnet_crop_id"]).map((e)=>(e.id))
                dispatch(updateCapacityBuildingForm(data))
            }else{
                setCapacityBuildingFlagEdit(false)
                dispatch(resetDataCapacityBuilding())
                const getDataFromBeneficiary = await axios.get(`${apiUrl}api/get_primarydata_by_beneficiary_componentid?quarter=${selectQuarter}&beneficiary_id=${selectbeniFiApplicant}&component_id=${event.target.value}&form_id=3`,getConfigWithToken())
                const data = getDataFromBeneficiary.data["data"]
                if(data!==null){
                    data["magnet_crop_id"] = JSON.parse(data["magnet_crop_id"]).map((e)=>(e.id))
                    data["location_taluka"] = data["location"]["location_taluka_id"]
                    dispatch(updateCapacityBuildingForm(data))
                }
            }
        }
    };
    
    const handleOptionsValueChain = async (event) => {
        setSelectedValueChain(event.target.value);
        // if(checkFPOSelect()){
            
        //  
        //     const getValueChainEditData = await axios.get(`${apiUrl}api/getAllDetailsF23?quarter=${selectQuarter}&beneficiary_id=${selectbeniFiApplicant}&training_type_id=${event.target.value}`,getConfigWithToken())
        //  
        //     if(getValueChainEditData.data["status"] === true){
        //         getValueChainEditData.data["flag"]?setValueChainFlagEdit(true):setValueChainFlagEdit(false)
        //         const data = getValueChainEditData.data["data"][0]
        //         setValueChainIdEdit(getValueChainEditData.data["data"][0]["id"])
        //      
        //         data["magnet_crop_id"] = JSON.parse(data["magnet_crop_id"]).map((e)=>(e.id))
        //         dispatch(updateValueChainForm(data))
        //     }else{
        //         setValueChainFlagEdit(false)
        //         dispatch(resetDataValueChain())
        //     }
        // }
    };
    
    const handleOptionsMarketLevel = async (event) =>{
        if(checkFPOSelect()){
            setSelectedMarketLevel(event.target.value)
            const getMarketAssistEditData = await axios.get(`${apiUrl}api/market-dev-ass-edit?quarter=${selectQuarter}&beneficiary_id=${selectbeniFiApplicant}&training_type_id=${event.target.value}&form_id=17`,getConfigWithToken())

            if(getMarketAssistEditData.data["status"] === true){
                getMarketAssistEditData.data["flag"]?setMarketLevelFlagEdit(true):setMarketLevelFlagEdit(false)
                const data = getMarketAssistEditData.data["data"][0]

                setMarketLevelIdEdit(getMarketAssistEditData.data["data"][0]["id"])
                dispatch(updateMarketAssistForm(data))
            }else{
                setMarketLevelFlagEdit(false)
                dispatch(resetDataMarketAssist())
                const getDataFromBeneficiary = await axios.get(`${apiUrl}api/get_primarydata_by_beneficiary_componentid?quarter=${selectQuarter}&beneficiary_id=${selectbeniFiApplicant}&component_id=${event.target.value}&form_id=17`,getConfigWithToken())
                const data = getDataFromBeneficiary.data["data"]
                if(data!==null){
                    data["name_of_exhibition"] = data["name_of_certification_exhibition"]
                    dispatch(updateMarketAssistForm(data))
                }
            }
        }
    }

    const handleOptionsOtherCertCost = async (event) => {
        if(checkFPOSelect()){
            setSelectedOtherCertification(event.target.value)
            const getOtherCertEditData = await axios.get(`${apiUrl}api/other-cirtification-edit?quarter=${selectQuarter}&beneficiary_id=${selectbeniFiApplicant}&training_type_id=${event.target.value}&form_id=15`,getConfigWithToken())

            if(getOtherCertEditData.data["status"] === true){
                getOtherCertEditData.data["flag"]?setOtherCertFlagEdit(true):setOtherCertFlagEdit(false)
                const data = getOtherCertEditData.data["data"][0]
                setEditInput1Files(data["remarks"])
                setOtherCertIdEdit(getOtherCertEditData.data["data"][0]["id"])
                dispatch(updateOtherCertiForm(data))
            }else{
                setOtherCertFlagEdit(false)
                dispatch(resetDataOtherCerti())
                const getDataFromBeneficiary = await axios.get(`${apiUrl}api/get_primarydata_by_beneficiary_componentid?quarter=${selectQuarter}&beneficiary_id=${selectbeniFiApplicant}&component_id=${event.target.value}&form_id=15`,getConfigWithToken())
                const data = getDataFromBeneficiary.data["data"]
                if(data!==null){
                    data["name_of_certification"] = data["name_of_certification_exhibition"]
                    dispatch(updateOtherCertiForm(data))
                }
            }
        }
    }

    const handleOptionsProductionPlant = async (event) => {
        if(checkFPOSelect()){
            setSelectedProductionPlant(event.target.value)
            const getProdPlantEditData = await axios.get(`${apiUrl}api/production-plant-cirtification-edit?quarter=${selectQuarter}&beneficiary_id=${selectbeniFiApplicant}&training_type_id=${event.target.value}&form_id=16`,getConfigWithToken())

            if(getProdPlantEditData.data["status"] === true){
                getProdPlantEditData.data["flag"]?setProdPlantFlagEdit(true):setProdPlantFlagEdit(false)
                const data = getProdPlantEditData.data["data"][0]
                setEditInput1Files(data["remarks"])
                setProdPlantIdEdit(getProdPlantEditData.data["data"][0]["id"])
                dispatch(updateProductionPlantForm(data))
            }else{
                setProdPlantFlagEdit(false)
                dispatch(resetDataProductionPlant())
                const getDataFromBeneficiary = await axios.get(`${apiUrl}api/get_primarydata_by_beneficiary_componentid?quarter=${selectQuarter}&beneficiary_id=${selectbeniFiApplicant}&component_id=${event.target.value}&form_id=16`,getConfigWithToken())
                const data = getDataFromBeneficiary.data["data"]
                if(data!==null){
                    data["name_of_certification"] = data["name_of_certification_exhibition"]
                    dispatch(updateProductionPlantForm(data))
                }
            }
        }
    }

    const handleHighDensityFPO = async (event) => {
        const applicantId = event.target.value
        setSelectBeniApplicant(applicantId)
        const getHighDensityEdit = await axios.get(`${apiUrl}api/getAllDetailsDemonstrationsOnHdp?quarter=${selectQuarter}&beneficiary_id=${applicantId}`, getConfigWithToken())
        // const getHighDensityEdit = await axios.get(`${apiUrl}api/get_primarydata_by_benificiary?beneficiary_id=${applicantId}&form_id=6`, getConfigWithToken())
        if(getHighDensityEdit.data["status"] === true){
            getHighDensityEdit.data["flag"]?setHdpDensityFlagEdit(true):setHdpDensityFlagEdit(false)
            const data = getHighDensityEdit.data["data"][0]
            setHdpDensityIdEdit(data["id"])
            data["magnet_crop_id"]?(data["magnet_crop_id"]= JSON.parse(data["magnet_crop_id"]).map((e)=>(e.id))):(data["magnet_crop_id"]=[])

            dispatch(updateHighDensityForm(data))
            setHdpbeneficiary(data)
        }else{
            const getHighDensityEdit = await axios.get(`${apiUrl}api/get_primarydata_by_benificiary?beneficiary_id=${applicantId}&quarter=${selectQuarter}&form_id=6`, getConfigWithToken())
            const data = getHighDensityEdit.data["data"]
            data["magnet_crop_id"]?(data["magnet_crop_id"]= JSON.parse(data["magnet_crop_id"]).map((e)=>(e.id))):(data["magnet_crop_id"]=[])

            setHdpbeneficiary(data)
            setHdpDensityFlagEdit(false)
            dispatch(resetDataHighDensity())
        }
    }

    const handleOptionsStudyTourExpVisit = async (event) => {
            setSelectedStudyTourExpVisit(event.target.value);
    };

    const handleOptionTradeFare = (event) => {
        setSelectedTradeFareExhitbiton(event.target.value)
    }

    const handleOptionMarketPromotion = (event) => {
        setSelectedMarketDevProm(event.target.value)
    }

    const handleDivisionIdChange = (event) => {
        setSelectDivision(event.target.value)
    }

    const handleDivisionKMPGChange = async (event) => {
        setSelectKPMGDivision(event.target.value)
        try{
            const beneficiaryByFormDivision = await axios.get(`${apiUrl}api/get-beneficiary-by-divisionid?division_id=${event.target.value}`, getConfigWithToken())
            const studyFarm = beneficiaryByFormDivision.data.data[0].map((e)=>({"id":e["id"],"crop":e["beneficiary_name"]}))
            setStudyFarmApplicant(studyFarm)
        }catch(err){

        }
    }

    const handlePmuIdChange = async (value) => {
        setSelectPMUId(value)
        const getStudyTourEditData = await axios.get(`${apiUrl}api/study-tour-for-magnet-edit?quarter=${selectQuarter}&division_id=${selectDivision}&pmu_id[]=${value.join(',')}&training_type_id=${selectedStudyTourExpVisit}&form_id=25`,getConfigWithToken())
        if(getStudyTourEditData.data["status"] === true){
            getStudyTourEditData.data["flag"]?setStudyTourFlagEdit(true):setStudyTourFlagEdit(false)
            const data = getStudyTourEditData.data["data"][0]
            setEditInput1Files(data["remarks"])
            setStudyTourIdEdit(getStudyTourEditData.data["data"][0]["id"])
            dispatch(updateApiDataToStudy(data))
        }else{
            setStudyTourFlagEdit(false)
            dispatch(resetDataStudy())
        }
    }

    const handleBenefiStudyTourFarm = async (beneficiary_id) => {
        setBeneStudyTourFarm(beneficiary_id)
        if(value === '0'){

            const getStudyTourEditData = await axios.get(`${apiUrl}api/study-tour-for-farmers-edit?quarter=${selectQuarter}&division_id=${selectDivision}&beneficiary_id[]=${beneficiary_id.join(',')}&training_type_id=${selectedStudyTourFarmer}&form_id=24`,getConfigWithToken())
            if(getStudyTourEditData.data["status"] === true){
                getStudyTourEditData.data["flag"]?setStudyTourFlagEdit(true):setStudyTourFlagEdit(false)
                const data = getStudyTourEditData.data["data"][0]
                data["magnet_crop_id"]?(data["magnet_crop_id"]= JSON.parse(data["magnet_crop_id"]).map((e)=>(e.id))):(data["magnet_crop_id"]=[])
                setEditInput1Files(data["remarks"])
                setStudyTourIdEdit(data["id"])
                dispatch(updateApiDataToFarmer(data))
            }else{
                setStudyTourFlagEdit(false)
                dispatch(resetDataFarmer())
            }
        }
        if(value === '21'){

            const getStudyTourEditData = await axios.get(`${apiUrl}api/trade-fair-exhibition-input-one-edit?quarter=${selectQuarter}&division_id=${selectKPMGDivision}&beneficiary_id=${beneficiary_id.join(',')}&training_type_id=${selectedTradeFareExhitbiton}&form_id=33`,getConfigWithToken())
            if(getStudyTourEditData.data["status"] === true){
                getStudyTourEditData.data["flag"]?setStudyTourFlagEdit(true):setStudyTourFlagEdit(false)
                const data = getStudyTourEditData.data["data"][0]
                setEditInput1Files(data["remarks"])
                setStudyTourIdEdit(data["id"])
                dispatch(updateApiDataTradeFair(data))
            }else{
                setEditInput1Files([])
                setStudyTourFlagEdit(false)
                dispatch(resetDataTradeFair())
            }
        }
        if(value === '22'){
            const getStudyTourEditData = await axios.get(`${apiUrl}api/market-dev-promotion-input-one-edit?quarter=${selectQuarter}&beneficiary_id=${beneficiary_id.join(',')}&training_type_id=${selectedMarketDevProm}&form_id=32`,getConfigWithToken())
            if(getStudyTourEditData.data["status"] === true){
                getStudyTourEditData.data["flag"]?setStudyTourFlagEdit(true):setStudyTourFlagEdit(false)
                const data = getStudyTourEditData.data["data"][0]
                setEditInput1Files(data["remarks"])
                setStudyTourIdEdit(data["id"])
                dispatch(updateApiDataMarketProm(data))
            }else{
                setEditInput1Files([])
                setStudyTourFlagEdit(false)
                dispatch(resetDataMarketProm())
            }
        }
        if(value === '9'){
            const getStudyTourEditData = await axios.get(`${apiUrl}api/f23-inputs-edit?quarter=${selectQuarter}&beneficiary_id=${beneficiary_id.join(',')}&training_type_id=${selectedValueChain}&form_id=1`,getConfigWithToken())
            if(getStudyTourEditData.data["status"] === true){
                getStudyTourEditData.data["flag"]?setValueChainFlagEdit(true):setValueChainFlagEdit(false)
                const data = getStudyTourEditData.data["data"][0]
                setEditInput1Files(data["remarks"])
                setValueChainIdEdit(data["id"])
                dispatch(updateValueChainForm(data))
            }else{
                setEditInput1Files([])
                setValueChainFlagEdit(false)
                dispatch(resetDataValueChain())
            }
        }
        if(value === '6'){
            const getStudyTourEditData = await axios.get(`${apiUrl}api/postHar-Cap-edit?quarter=${selectQuarter}&beneficiary_id=${beneficiary_id.join(',')}&form_id=2`,getConfigWithToken())
            if(getStudyTourEditData.data["status"] === true){
                getStudyTourEditData.data["flag"]?setPostHarvestFlagEdit(true):setPostHarvestFlagEdit(false)
                const data = getStudyTourEditData.data["data"][0]
                data["magnet_crop_id"] = data["magnet_crop_id"].split(',')
                setEditInput1Files(data["remarks"])
                setPostHarvestIdEdit(data["id"])
                dispatch(updatePostHarvestForm(data))
            }else{
                setEditInput1Files([])
                setPostHarvestFlagEdit(false)
                dispatch(resetDataPostHarvest())
            }
        }
    }

    const handleOptionsStudyFarmer = async (event) => {
        setSelectedStudyFarmer(event.target.value)
    }

// Change the API of Applicant with the Form Id and Beneficiary Id
    const handleApplicantDataByFormId = async (applicantId, form_id) => {
        try{
            const beneficiary_by_id = await axios.get(`${apiUrl}api/get_primarydata_by_benificiary?beneficiary_id=${applicantId}&quarter=${selectQuarter}&form_id=${form_id}`,getConfigWithToken())
            const data = await beneficiary_by_id.data["data"]
            data["magnet_crop_id"]?(data["magnet_crop_id"]= JSON.parse(data["magnet_crop_id"]).map((e)=>(e.id))):(data["magnet_crop_id"]=[])
            setNurseryTissueBeneficiary(data)
            return await data
        }catch(err){

        }
    }

    const handleApplicantDataWithoutComponent = async (applicantId, form_id) => {
        try{
            const beneficiary_by_id = await axios.get(`${apiUrl}api/get_primary_data_for_non_cat_compo_forms?beneficiary_id=${applicantId}&quarter=${selectQuarter}&form_id=${form_id}`,getConfigWithToken())
            const data = await beneficiary_by_id.data["data"]
            data["magnet_crop_id"]?(data["magnet_crop_id"]= JSON.parse(data["magnet_crop_id"]).map((e)=>(e.id))):(data["magnet_crop_id"]=[])
            setNurseryTissueBeneficiary(data)
            return await data
        }catch(err){

        }
    }

    // const handleNurseryTissueFPO = async (event) =>{
    //     setSelectBeniApplicant(event.target.value)

    //     if(value === '15'){
    //         // const getNurseryTissueData = await axios.get(`${apiUrl}api/get_primarydata_by_benificiary?beneficiary_id=${event.target.value}&form_id=4`, getConfigWithToken())
    //         handleApplicantDataByFormId(event.target.value, '19')
    //         const downData = await axios.get(`${apiUrl}api/research-dev-edit?beneficiary_id=${event.target.value}&form_id=19&quarter=${selectQuarter}`,getConfigWithToken())
    //         if(downData.data["status"] === true){
    //             downData.data["flag"]?setResearchDevFlagEdit(true):setResearchDevFlagEdit(false)
    //             setResearchDevIdEdit(downData.data.data[0]['id'])
    //             setEditInput1Files(downData.data.data[0]["remarks"])
    //             dispatch(updateApiDataToResearch(downData.data.data[0]))
    //         }else{
    //             dispatch(resetDataResearch())
    //             setEditInput1Files([])
    //             setResearchDevFlagEdit(false)
    //         }
    //     }
    // }

    const handleApplicantChangeESIFPO = async (event) =>{
        setErrorSelectBeniApplicant("")
        const applicantId = event.target.value
        setSelectBeniApplicant(applicantId)
        setErrorSelectBeniApplicant("")
        setSelectedValueChain("")
        setSelectedGAP("")
        setSelectedOtherCertification("")
        setSelectedMarketLevel("")
        setSelectedProductionPlant("")
        setSelectSupportComponent("")
        try{
            // const beneficiary_by_id = await axios.get(`${apiUrl}api/get-benificiary-details-by-id?id=${applicantId}`,getConfigWithToken())
            // const beneficiaryData = beneficiary_by_id.data["data"][0]
            
            // setBeniById(beneficiaryData)

            switch(value){
                case '1':
                    handleApplicantDataByFormId(applicantId, 3)
                break;
                case '2':
                    setSelectNurseryComponent("")
                    handleApplicantDataByFormId(applicantId, 4)
                break;
                case '3':
                    setSelectTissueComponent("")
                    handleApplicantDataByFormId(applicantId, 5)
                break;
                case '5':
                    handleApplicantDataByFormId(applicantId, 6)
                break;
                case '6':
                    handleApplicantDataByFormId(applicantId, 2)
                break;
                case '7':
                    handleApplicantDataByFormId(applicantId, 7)
                    const smeLinkageEdit = await axios.get(`${apiUrl}api/getAllDetailsSme?quarter=${selectQuarter}&beneficiary_id=${applicantId}`, getConfigWithToken())
                    if(smeLinkageEdit.data["status"] === true){
                        smeLinkageEdit.data["flag"]?setSMELinkageFlagEdit(true):setSMELinkageFlagEdit(false)
                        const data = smeLinkageEdit.data["data"][0]
                        setSMELinkageIdEdit(data["id"])
                        dispatch(updateSMELinkageForm(data))
                    }else{
                        setSMELinkageFlagEdit(false)
                        dispatch(resetDataSMELinkage())
                    }
                break;
                case '8':
                    handleApplicantDataByFormId(applicantId, 17)
                break;
                case '11':
                    const getWorkShopEditData = await axios.get(`${apiUrl}api/gworkshop-inputs-edit?quarter=${selectQuarter}&beneficiary_id=${applicantId}&form_id=22`,getConfigWithToken())
                    if(getWorkShopEditData.data["status"] === true){
                        getWorkShopEditData.data["flag"]?setWorkShopFlagEdit(true):setWorkShopFlagEdit(false)
                        const data = getWorkShopEditData.data["data"][0]
            
                        setWorkShopIdEdit(getWorkShopEditData.data["data"][0]["id"])
                        setEditInput1Files(data["remarks"])
                        dispatch(updateWorkShopForm(data))
                    }else{
                        setWorkShopFlagEdit(false)
                        dispatch(resetDataWorkShop())
                    }
                break;
                case '12':
                    handleApplicantDataByFormId(applicantId, 23)
                break;
                case '13':
                    handleApplicantDataByFormId(applicantId, 15)
                break;
                case '15':
                    const data = await handleApplicantDataWithoutComponent(applicantId, 19)
                    const downData = await axios.get(`${apiUrl}api/research-dev-edit?beneficiary_id=${applicantId}&form_id=19&quarter=${selectQuarter}`,getConfigWithToken())
                    if(downData.data["status"] === true){
                        const data = downData.data.data[0]
                        downData.data["flag"]?setResearchDevFlagEdit(true):setResearchDevFlagEdit(false)
                        setResearchDevIdEdit(data['id'])
                        setEditInput1Files(data["remarks"])
                        data["payment_tranche"] = data["tranche"]
                        dispatch(updateApiDataToResearch(data))
                    }else{
                        data["payment_tranche"] = data["tranche"]["tranche"]
                        data["tranche_id"] = data["tranche"]["tranche_id"]
                        dispatch(updateApiDataToResearch(data))
                        setEditInput1Files([])
                        setResearchDevFlagEdit(false)
                    }
                break;
                case '16':
                    const dataInova = await handleApplicantDataWithoutComponent(applicantId, 20)
                    const downDataInnovation = await axios.get(`${apiUrl}api/innovation-in-packaging-edit?beneficiary_id=${applicantId}&form_id=20&quarter=${selectQuarter}`,getConfigWithToken())
                    if(downDataInnovation.data["status"] === true){
                        const data = downDataInnovation.data.data[0]
                        downDataInnovation.data["flag"]?setResearchDevFlagEdit(true):setResearchDevFlagEdit(false)
                        setResearchDevIdEdit(data['id'])
                        setEditInput1Files(data["remarks"])
                        data["payment_tranche"] = data["tranche"]
                        dispatch(updateApiDataToInnovation(data))
                    }else{
                        dataInova["payment_tranche"] = dataInova["tranche"]["tranche"]
                        dataInova["tranche_id"] = dataInova["tranche"]["tranche_id"]
            
                        dispatch(updateApiDataToInnovation(dataInova))
                        setEditInput1Files([])
                        setResearchDevFlagEdit(false)
                    }
                break;
                case '17':
                    const dataIntro = await handleApplicantDataWithoutComponent(applicantId, 21)
                    const downDataIntro = await axios.get(`${apiUrl}api/intro-to-new-variety-edit?beneficiary_id=${applicantId}&form_id=21&quarter=${selectQuarter}`,getConfigWithToken())
                    if(downDataIntro.data["status"] === true){
                        const data = downDataIntro.data.data[0]
                        downDataIntro.data["flag"]?setResearchDevFlagEdit(true):setResearchDevFlagEdit(false)
                        setResearchDevIdEdit(data['id'])
                        setEditInput1Files(data["remarks"])
                        data["payment_tranche"] = data["tranche"]
                        dispatch(updateApiDataToIntro(data))
                    }else{
            
                        dataIntro["payment_tranche"] = dataIntro["tranche"]["tranche"]
                        dataIntro["tranche_id"] = dataIntro["tranche"]["tranche_id"]
                        dispatch(updateApiDataToIntro(dataIntro))
            
                        setEditInput1Files([])
                        setResearchDevFlagEdit(false)
                    }
                break;
                case '18':
                    handleApplicantDataByFormId(applicantId, 16)
                break;
                case '19':
                    handleApplicantDataByFormId(applicantId, 26)
                break;
                case '20':
                    const dataBio = await handleApplicantDataWithoutComponent(applicantId, 18)
                    const downDataBio = await axios.get(`${apiUrl}api/bio-west-wealth-edit?beneficiary_id=${applicantId}&form_id=18&quarter=${selectQuarter}`,getConfigWithToken())
                    if(downDataBio.data["status"] === true){
                        const data = downDataBio.data.data[0]
                        downDataBio.data["flag"]?setResearchDevFlagEdit(true):setResearchDevFlagEdit(false)
                        setResearchDevIdEdit(data['id'])
                        setEditInput1Files(data["remarks"])
                        data["payment_tranche"] = data["tranche"]
                        dispatch(updateApiDataTobiowaste(data))
                    }else{
                        dataBio["payment_tranche"] = dataBio["tranche"]["tranche"]
                        dataBio["tranche_id"] = dataBio["tranche"]["tranche_id"]
                        
                        dispatch(updateApiDataTobiowaste(dataBio))
                        setEditInput1Files([])
                        setResearchDevFlagEdit(false)
                    }
                break;

            }

            // if(value === '11'){
                // const getWorkShopEditData = await axios.get(`${apiUrl}api/getAllDetailsGworkshop?quarter=${selectQuarter}&applicant_id=${applicantId}&division_id=${beneficiaryData.divisions.division_id}&district_id=${beneficiaryData.district.district_id}&taluka_id=${beneficiaryData.taluka.taluka_id}`,getConfigWithToken())
                // if(getWorkShopEditData.data["status"] === true){
                //     getWorkShopEditData.data["flag"]?setWorkShopFlagEdit(true):setWorkShopFlagEdit(false)
                //     const data = getWorkShopEditData.data["data"][0]
                //  
                //     setWorkShopIdEdit(getWorkShopEditData.data["data"][0]["id"])
                //     data["magnet_crop_id"] = JSON.parse(data["magnet_crop_id"]).map((e)=>(e.id))

                //     dispatch(updateWorkShopForm(data))
                // }else{
                //     setWorkShopFlagEdit(false)
                //     dispatch(resetDataWorkShop())
                // }
            // }

        }catch(error){
            Swal.fire(
                'An error occurred while Selecting Benificary Applicants',
                'Please try again later',
                'error'
            );
        }
    }
    
    const handleQuarterChange = async (event) =>{
        setSelectedQuarters(event.target.value)
    }

    async function handleWorkShopSubmit (e){
        e.preventDefault()
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["workShop"];
            const errorMessages = {
                location_taluka: 'Taluka is Required',
                location_address: 'Taluka Address is Required',
                from_date:'From Date is Required',
                to_date:'To Date is Required',
                subject:'Subject is Required',
                sc_male: 'SC Male is required',
                sc_female: 'SC Female is required',
                st_male: 'ST Male is required',
                st_female: 'ST Female is required',
                pwd_male: 'PWD Male is required',
                pwd_female: 'PWD Female is required',
                bpl_male: 'BPL Male is required',
                bpl_female: 'BPL Female is required',
                obc_minority_open_male: 'OBC Minority Open Male is required',
                obc_minority_open_female: 'OBC Minority Open Female is required',
                participants_male: 'Participants Male is required',
                participants_female: 'Participants Female is required',
            };
        
            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsWorkshop(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };
        if(checkFPOSelect() && validateForm()){
            try{
                const workData = dd["payload"]["workShop"]

                const formData = new FormData();
                 for (const key in workData) {
                    formData.append(key, workData[key]!==null?workData[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('beneficiary_id', selectbeniFiApplicant)
                  formData.append('form_id', '22');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }

                try{
                    if(!workShopFlagEdit){
                        const submitWorkShopData = await axios.post(`${apiUrl}api/gworkshop-inputs-create`, formData, getConfigWithToken())
                        if(submitWorkShopData.data["status"] === true){
                            Swal.fire(
                                `${submitWorkShopData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataWorkShop())
                        }else{
                            Swal.fire(
                                `${submitWorkShopData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append('id', workShopIdEdit);
                        const submitWorkShopData = await axios.post(`${apiUrl}api/gworkshop-inputs-update`, formData, getConfigWithToken())
                        if(submitWorkShopData.data["status"] === true){
                            Swal.fire(
                                `${submitWorkShopData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataWorkShop())
                        }else{
                            Swal.fire(
                                `${submitWorkShopData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting WorkShop form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting WorkShop form',
                    'Please try again later',
                    'error'
                );
            }
        } 
    }

    const handlePostHarvest = async (e) =>{
        e.preventDefault()
        const formData = dd["payload"]["postHarvest"];
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["postHarvest"];
            const errorMessages = {
                from_date: 'From Date is a Required Field',
                to_date: 'To Date is a Required Field',
                location_taluka: 'Taluka is required',
                conducted_by_tot: 'Conducted is required',
                magnet_crop_id: 'Magnet Crop ID is required',
                participants_male: 'Trainee Male is required',
                participants_female: 'Trainee Female is required',
            };

            if(formData["conducted_by_tot"] === 'Yes'){
                errorMessages["no_of_tots"]='No of TOT is required'
            }
        
            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsPostHarvest(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        if(validateForm()){
            try{
                const postHarvest = dd["payload"]["postHarvest"]

                const formData = new FormData();
                 for (const key in postHarvest) {
                    if(key === 'magnet_crop_id'){
                        formData.append('magnet_crop_id', postHarvest["magnet_crop_id"].join(','))
                    }else{
                        formData.append(key, postHarvest[key]!==null?postHarvest[key]:"");
                    }
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('division_id', selectKPMGDivision)
                  formData.append('beneficiary_id', beneStudyTourFarm)
                  formData.append('no_of_tots', postHarvest["no_of_tots"]?postHarvest["no_of_tots"]:"0")
                  formData.append('form_id', '2');
                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }

                try{
                    if(!postHarvestFlagEdit){
                        const submitPostHarvestData = await axios.post(`${apiUrl}api/postHar-Cap-create`, formData, getConfigWithToken())
                        if(submitPostHarvestData.data["status"] === true){
                            Swal.fire(
                                `${submitPostHarvestData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataPostHarvest())
                        }else{
                            Swal.fire(
                                `${submitPostHarvestData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append('id', postHarvestIdEdit)
                        const submitPostHarvestData = await axios.post(`${apiUrl}api/postHar-Cap-update`, formData, getConfigWithToken())
            
                        if(submitPostHarvestData.data["status"] === true){
                            Swal.fire(
                                `${submitPostHarvestData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataPostHarvest())
                            setEditInput1Files([])
                        }else{
                            Swal.fire(
                                `${submitPostHarvestData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting WorkShop form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting WorkShop form',
                    'Please try again later',
                    'error'
                );
            }
        } 

    }

    const handleValueChainSubmit = async (e) =>{
        e.preventDefault()
        const formData = dd["payload"]["valueChain"];
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["valueChain"];
            const errorMessages = {
                from_date: 'From Date is a Required Field',
                to_date: 'To Date is a Required Field',
                location_taluka: 'Taluka is required',
                location_address: 'Address is required',
                resource_person: 'Resource Person is required',
                action_points_recommendation_made: 'Action points is required',
                sc_male: 'SC Male is required',
                sc_female: 'SC Female is required',
                st_male: 'ST Male is required',
                st_female: 'ST Female is required',
                pwd_male: 'PWD Male is required',
                pwd_female: 'PWD Female is required',
                bpl_male: 'BPL Male is required',
                bpl_female: 'BPL Female is required',
                obc_minority_open_male: 'OBC Minority Open Male is required',
                obc_minority_open_female: 'OBC Minority Open Female is required',
                participants_male: 'Participant Male is required',
                participants_female: 'Participant Female is required',
            };
        
            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsValueChain(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        if(validateForm()){
            try{
                const valueChain = dd["payload"]["valueChain"]

                const formData = new FormData();
                 for (const key in valueChain) {
                    formData.append(key, valueChain[key]!==null?valueChain[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('training_type_id', selectedValueChain)
                  formData.append('division_id', selectKPMGDivision)
                  formData.append('beneficiary_id', beneStudyTourFarm)
                  formData.append('form_id', '1');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }

                try{
                    if(!valueChainFlagEdit){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitWorkShopData = await axios.post(`${apiUrl}api/f23-inputs-create`, formData, getConfigWithToken())
                        if(submitWorkShopData.data["status"] === true){
                            Swal.fire(
                                `${submitWorkShopData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataValueChain())
                        }else{
                            Swal.fire(
                                `${submitWorkShopData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", valueChainIdEdit);
                        const submitWorkShopData = await axios.post(`${apiUrl}api/f23-inputs-update`, formData, getConfigWithToken())
                        if(submitWorkShopData.data["status"] === true){
                            Swal.fire(
                                `${submitWorkShopData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataValueChain())
                        }else{
                            Swal.fire(
                                `${submitWorkShopData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting WorkShop form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting WorkShop form',
                    'Please try again later',
                    'error'
                );
            }
        } 
    }

    const handleHighDensitySubmit = async (e) => {
        e.preventDefault()
        const formData = dd["payload"]["highDensity"];
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["highDensity"];
            const errorMessages = {
                sc_male: 'SC Male is required',
                sc_female: 'SC Female is required',
                st_male: 'ST Male is required',
                st_female: 'ST Female is required',
                pwd_male: 'PWD Male is required',
                pwd_female: 'PWD Female is required',
                bpl_male: 'BPL Male is required',
                bpl_female: 'BPL Female is required',
                obc_minority_open_male: 'OBC Minority Open Male is required',
                obc_minority_open_female: 'OBC Minority Open Female is required',
                beneficiaries_male: 'Beneficiaries Male is required',
                beneficiaries_female: 'Beneficiaries Female is required',
            };
        
            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsHighDensity(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        if(checkFPOSelect() && validateForm()){
            try{
                const highDensity = dd["payload"]["highDensity"]
                
                const jsonMagnetCrop = hdpbeneficiary["magnet_crop_id"].map(id => {
                    const matchingObject = magentCrop.find(item => item.id === Number(id));
                    if (matchingObject) {
                      return {
                        id: matchingObject.id,
                        name: matchingObject.crop,
                      };
                    }
                    return null; // Handle cases where there's no matching ID
                  }).filter(item => item !== null); // Remove any null entries
                
                const resultMagnetCrop = { data: jsonMagnetCrop };
    
                const highDensityData = {...highDensity,
                    "quarter": selectQuarter,
                    "beneficiary_id": selectbeniFiApplicant,
                    "district_id": hdpbeneficiary.district.district_id,
                    "division_id": hdpbeneficiary.divisions.division_id,
                    "taluka_id": hdpbeneficiary.taluka.taluka_id,
                    "location_taluka": hdpbeneficiary.location.location_taluka_id,
                    "beneficiary_type_id": hdpbeneficiary.beneficiary_type.beneficiary_type_id,
                    "magnet_crop_id": JSON.stringify(resultMagnetCrop["data"]),
                    "total_saction_grant": hdpbeneficiary.total_saction_grant,
                    "total_plantation_cost": hdpbeneficiary.total_plantation_cost,
                    "area": hdpbeneficiary.area,
                    "payment_tranche": hdpbeneficiary.tranche ? hdpbeneficiary.tranche.payment_tranche: hdpbeneficiary.payment_tranche,
                    "no_of_farmers_demonstrated": hdpbeneficiary.no_of_farmers,
                    "form_id": "6",
                    "plantation_cost": hdpbeneficiary.total_plantation_cost,
                    "grant_disbursement_date": hdpbeneficiary.grant_disbursement_date,
                    "grant_disbursement_amt": hdpbeneficiary.grant_disbursement_amount,
                }
    

                try{
                    if(!hdpDensityFlagEdit){
                        const submitHighDensityData = await axios.post(`${apiUrl}api/demonHDP-inputs-create`, highDensityData, getConfigWithToken())
                        if(submitHighDensityData.data["status"] === true){
                            Swal.fire(
                                `${submitHighDensityData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataHighDensity())
                        }else{
                            Swal.fire(
                                `${submitHighDensityData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        highDensityData.id=hdpDensityIdEdit
            
                        const submitHighDensityData = await axios.post(`${apiUrl}api/demonHDP-inputs-update`, highDensityData, getConfigWithToken())
            
                        if(submitHighDensityData.data["status"] === true){
                            Swal.fire(
                                `${submitHighDensityData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataHighDensity())
                        }else{
                            Swal.fire(
                                `${submitHighDensityData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting WorkShop form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting WorkShop form',
                    'Please try again later',
                    'error'
                );
            }
        }

    }
    
    const handleCapacityBuildingSubmit = async (e) => {
        e.preventDefault()
        const formData = dd["payload"]["capacityBuilding"];
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["capacityBuilding"];
            const errorMessages = {
                magnet_crop_id: 'Magnet Crop is required',
                // date: 'Date is required',
                // action_points: 'Action Point is required',
                location_taluka: 'Loaction Taluka is Required',
                sc_male: 'SC Male is required',
                sc_female: 'SC Female is required',
                st_male: 'ST Male is required',
                st_female: 'ST Female is required',
                pwd_male: 'PWD Male is required',
                pwd_female: 'PWD Female is required',
                bpl_male: 'BPL Male is required',
                bpl_female: 'BPL Female is required',
                obc_minority_open_male: 'OBC Minority Open Male is required',
                obc_minority_open_female: 'OBC Minority Open Female is required',
                beneficiaries_male: 'Beneficiaries Male is required',
                beneficiaries_female: 'Beneficiaries Female is required',
            };

            if(selectedGAP === '5'){
                errorMessages["resource_person"]= 'Resource Person is Required'
                errorMessages["topic_covered"]= 'Topic Covered is Required'
                errorMessages["date"]= 'Date is required'
            }
            
            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }


            dispatch(setValidationErrorsCapacityBuilding(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        // validateForm()

        if(checkFPOSelect() && validateForm()){
            try{
                const capacityBuilding = dd["payload"]["capacityBuilding"]

                const jsonMagnetCrop = capacityBuilding["magnet_crop_id"].map(id => {
                    const matchingObject = magentCrop.find(item => item.id === id);
                    if (matchingObject) {
                      return {
                        id: matchingObject.id,
                        name: matchingObject.crop,
                      };
                    }
                    return null; // Handle cases where there's no matching ID
                  }).filter(item => item !== null); // Remove any null entries
                const resultMagnetCrop = { data: jsonMagnetCrop };
    
                const formData = new FormData();
                for (const key in capacityBuilding) {
                    formData.append(key, capacityBuilding[key]!==null?capacityBuilding[key]:"");
                  }
                  formData.append('quarter', selectQuarter);
                  formData.append('beneficiary_id', selectbeniFiApplicant);
                  formData.append('district_id', nurseryTissueBeneficiary.district.district_id);
                  formData.append('division_id', nurseryTissueBeneficiary.divisions.division_id);
                  formData.append('taluka_id', nurseryTissueBeneficiary.taluka.taluka_id);
                  formData.append('beneficiary_type_id', nurseryTissueBeneficiary.beneficiary_type.beneficiary_type_id);
                  formData.append('training_type_id', selectedGAP);
                  formData.append('form_id', '3');
                  formData.append('action_points', capacityActionPoint);
                  formData.append('magnet_crop_id', JSON.stringify(resultMagnetCrop["data"]));

                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }

                
                try{
                    if(!capacityBuildingFlagEdit){
                    // if(true){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitHighDensityData = await axios.post(`${apiUrl}api/capBuild-Gap-create`, formData, getConfigWithToken())
                        if(submitHighDensityData.data["status"] === true){
                            Swal.fire(
                                `${submitHighDensityData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataCapacityBuilding())
                        }else{
                            Swal.fire(
                                `${submitHighDensityData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", capacityBuildingIdEdit);
                        const submitHighDensityData = await axios.post(`${apiUrl}api/capBuild-Gap-update`, formData, getConfigWithToken())
            
                        if(submitHighDensityData.data["status"] === true){
                            Swal.fire(
                                `${submitHighDensityData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataCapacityBuilding())
                        }else{
                            Swal.fire(
                                `${submitHighDensityData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting WorkShop form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting WorkShop form',
                    'Please try again later',
                    'error'
                );
            }
        }

    }

    const handleSMELinkageSubmit = async (e) =>{
        e.preventDefault()
        const formData = dd["payload"]["smeLinkage"];
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["smeLinkage"];
            const errorMessages = {
                date: 'Date is required',
                location_taluka: 'Location Taluka is Required',
                location_address: 'Location Address is Required',
                resource_person: 'Resource Person is Required',
                topic_covered: 'Topic Covered is Required',
                sc_male: 'SC Male is required',
                sc_female: 'SC Female is required',
                st_male: 'ST Male is required',
                st_female: 'ST Female is required',
                pwd_male: 'PWD Male is required',
                pwd_female: 'PWD Female is required',
                bpl_male: 'BPL Male is required',
                bpl_female: 'BPL Female is required',
                obc_minority_open_male: 'OBC Minority Open Male is required',
                obc_minority_open_female: 'OBC Minority Open Female is required',
                beneficiaries_male: 'Beneficiaries Male is required',
                beneficiaries_female: 'Beneficiaries Female is required',
            };
        
            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsSMELinkage(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        
        if(checkFPOSelect() && validateForm()){
            try{
                const smeLinkage = dd["payload"]["smeLinkage"]

                const formData = {...smeLinkage,
                    "quarter": selectQuarter,
                    "beneficiary_id": selectbeniFiApplicant,
                    'district_id': nurseryTissueBeneficiary.district.district_id,
                    'division_id': nurseryTissueBeneficiary.divisions.division_id,
                    'taluka_id': nurseryTissueBeneficiary.taluka.taluka_id,
                    'beneficiary_type_id': nurseryTissueBeneficiary.beneficiary_type.beneficiary_type_id,
                    "form_id":"7"
                }

                

                try{
                    if(!smeLinkageFlagEdit){
                    // if(true){
                        const submitSMELinkageData = await axios.post(`${apiUrl}api/smelinkage-inputs-create`, formData, getConfigWithToken())
                        if(submitSMELinkageData.data["status"] === true){
                            Swal.fire(
                                `${submitSMELinkageData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataSMELinkage())
                        }else{
                            Swal.fire(
                                `${submitSMELinkageData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData["id"] = smeLinkageIdEdit
                        const submitSMELinkageData = await axios.post(`${apiUrl}api/smelinkage-inputs-update`, formData, getConfigWithToken())
            
                        if(submitSMELinkageData.data["status"] === true){
                            Swal.fire(
                                `${submitSMELinkageData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataSMELinkage())
                        }else{
                            Swal.fire(
                                `${submitSMELinkageData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting WorkShop form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting WorkShop form',
                    'Please try again later',
                    'error'
                );
            }
        }
    }

    const handleMarketAssistSubmit = async (e) => {
        e.preventDefault()
        const formData = dd["payload"]["marketAssistence"];
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["marketAssistence"];
            const errorMessages = {
                disbursement_amt: "Disbursement Amount is Required",
                disbursement_date: "Disbursement Date is Required",
            };
        
            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsMarketAssist(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };


        if(checkFPOSelect() && validateForm()){
            try{
                const marketAssistence = dd["payload"]["marketAssistence"]

                const formData = {...marketAssistence,
                    "quarter": selectQuarter,
                    "beneficiary_id": selectbeniFiApplicant,
                    'district_id': nurseryTissueBeneficiary.district.district_id,
                    'division_id': nurseryTissueBeneficiary.divisions.division_id,
                    'taluka_id': nurseryTissueBeneficiary.taluka.taluka_id,
                    'beneficiary_type_id': nurseryTissueBeneficiary.beneficiary_type.beneficiary_type_id,
                    "form_id":"17",
                    "training_type_id": selectedMarketLevel
                }

                

                try{
                    if(!marketLevelFlagEdit){
                    // if(true){
                        const submitMarketAssistData = await axios.post(`${apiUrl}api/market-dev-ass-create`, formData, getConfigWithToken())
                        if(submitMarketAssistData.data["status"] === true){
                            Swal.fire(
                                `${submitMarketAssistData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataMarketAssist())
                        }else{
                            Swal.fire(
                                `${submitMarketAssistData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData["id"] = marketLevelIdEdit
                        const submitMarketAssistData = await axios.post(`${apiUrl}api/market-dev-ass-update`, formData, getConfigWithToken())
            
                        if(submitMarketAssistData.data["status"] === true){
                            Swal.fire(
                                `${submitMarketAssistData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataMarketAssist())
                        }else{
                            Swal.fire(
                                `${submitMarketAssistData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting Market Assistence form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting Market Assistence form',
                    'Please try again later',
                    'error'
                );
            }
        }
    }

    const handleOtherCertificationSubmit = async (e) => {
        e.preventDefault()
        const formData = dd["payload"]["otherCertification"];
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["otherCertification"];
            const errorMessages = {
                disbursement_amt: "Disbursement Amount is Required",
                disbursement_date: "Disbursement Date is Required",
            };
        
            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsOtherCerti(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        // validateForm()

        if(checkFPOSelect() && validateForm()){
            try{
                const capacityBuilding = dd["payload"]["otherCertification"]

                const formData = new FormData();
                for (const key in capacityBuilding) {
                    formData.append(key, capacityBuilding[key]!==null?capacityBuilding[key]:"");
                  }
                  formData.append('quarter', selectQuarter);
                  formData.append('beneficiary_id', selectbeniFiApplicant);
                  formData.append('district_id', nurseryTissueBeneficiary.district.district_id);
                  formData.append('division_id', nurseryTissueBeneficiary.divisions.division_id);
                  formData.append('taluka_id', nurseryTissueBeneficiary.taluka.taluka_id);
                  formData.append('beneficiary_type_id', nurseryTissueBeneficiary.beneficiary_type.beneficiary_type_id);
                  formData.append('training_type_id', selectedGAP);
                  formData.append('form_id', '15');


                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectOtherCertificationFile.length !== 0){
                    for(let i=0; i<selectOtherCertificationFile.length;i++){
                        size += selectOtherCertificationFile[i].size
                        formData.append("remarks[]", selectOtherCertificationFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }

                

                try{
                    if(!otherCertFlagEdit){
                    // if(true){
                        // if(selectOtherCertificationFile.length === 0){
                        //     setErrorCapacityFile("Please Select File")
                        //     return
                        // }
                        const submitOtherCertiData = await axios.post(`${apiUrl}api/other-cirtification-create`, formData, getConfigWithToken())
                        if(submitOtherCertiData.data["status"] === true){
                            Swal.fire(
                                `${submitOtherCertiData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataOtherCerti())
                        }else{
                            Swal.fire(
                                `${submitOtherCertiData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", otherCertIdEdit);
                        const submitOtherCertiData = await axios.post(`${apiUrl}api/other-cirtification-update`, formData, getConfigWithToken())
            
                        if(submitOtherCertiData.data["status"] === true){
                            Swal.fire(
                                `${submitOtherCertiData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataOtherCerti())
                        }else{
                            Swal.fire(
                                `${submitOtherCertiData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting WorkShop form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting WorkShop form',
                    'Please try again later',
                    'error'
                );
            }
        }
    }

    const handleNurserySubmit = async (e) => {
        e.preventDefault()
        
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["nurseryDev"];
            const errorMessages = {
                promoters_exp_incurred: 'Promoter Expenditure Incurred is required',
                term_loan_exp_incurred:'Term Loan Expenditure Incurred is required',
            };

            if(isMatchingGrant === true){
                errorMessages["matching_grant_exp_incurred"]= 'Matching Grant Expenditure Incurred is required'
            }
            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsNursery(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        if(checkFPOSelect() && validateForm()){
            try{
                const NurseryDev = dd["payload"]["nurseryDev"]
                
                const jsonMagnetCrop = nurseryTissueBeneficiary["magnet_crop_id"].map(id => {
                    const matchingObject = magentCrop.find(item => item.id === id);
                    if (matchingObject) {
                      return {
                        id: matchingObject.id,
                        name: matchingObject.crop,
                      };
                    }
                    return null; // Handle cases where there's no matching ID
                  }).filter(item => item !== null); // Remove any null entries
                
                const resultMagnetCrop = { data: jsonMagnetCrop };

                const formData = new FormData();
                 for (const key in NurseryDev) {
                    formData.append(key, NurseryDev[key]!==null?NurseryDev[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('beneficiary_id', selectbeniFiApplicant);
                  formData.append('district_id', nurseryTissueBeneficiary.district.district_id);
                  formData.append('division_id', nurseryTissueBeneficiary.divisions.division_id);
                  formData.append('taluka_id', nurseryTissueBeneficiary.taluka.taluka_id);
                  formData.append('beneficiary_type_id', nurseryTissueBeneficiary.beneficiary_type.beneficiary_type_id);
                  formData.append('component_id', selectNurseryComponent);
                  formData.append('magnet_crop_id', JSON.stringify(resultMagnetCrop["data"]));
                  formData.append('capacity_total_plants', nurseryTissueBeneficiary.capacity_total_plants?nurseryTissueBeneficiary.capacity_total_plants:"");
                  formData.append('area', nurseryTissueBeneficiary.area ? nurseryTissueBeneficiary.area: '');
                  formData.append('location_taluka', nurseryTissueBeneficiary.location["location_taluka_id"] ? nurseryTissueBeneficiary.location["location_taluka_id"]:'');
                  formData.append('form_id', '4');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }


                try{
                    if(!nurseryFlagEdit){
                    // if(true){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitNurseryDevData = await axios.post(`${apiUrl}api/nursery-development-create`, formData, getConfigWithToken())
                        if(submitNurseryDevData.data["status"] === true){
                            Swal.fire(
                                `${submitNurseryDevData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataNursery())
                        }else{
                            Swal.fire(
                                `${submitNurseryDevData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", nurseryIdEdit);
                        const submitNurseryData = await axios.post(`${apiUrl}api/nursery-development-update`, formData, getConfigWithToken())
            
                        if(submitNurseryData.data["status"] === true){
                            Swal.fire(
                                `${submitNurseryData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataNursery())
                        }else{
                            Swal.fire(
                                `${submitNurseryData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting Nursery Devlopment form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting Nursery Devlopment form',
                    'Please try again later',
                    'error'
                );
            }
        }

    }

    const handleTissueSubmit = async (e) =>{
        e.preventDefault()

        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["tissueCulture"];
            const errorMessages = {
                promoters_exp_incurred: 'Promoter Expenditure Incurred is required',
                term_loan_exp_incurred:'Term Loan Expenditure Incurred is required',
            };

            if(isMatchingGrant === true){
                errorMessages["matching_grant_exp_incurred"]= 'Matching Grant Expenditure Incurred is required'
            }
            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsTissue(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        if(checkFPOSelect() && validateForm()){
            try{
                const TissueCulture = dd["payload"]["tissueCulture"]
                
                const jsonMagnetCrop = nurseryTissueBeneficiary["magnet_crop_id"].map(id => {
                    const matchingObject = magentCrop.find(item => item.id === id);
                    if (matchingObject) {
                      return {
                        id: matchingObject.id,
                        name: matchingObject.crop,
                      };
                    }
                    return null; // Handle cases where there's no matching ID
                  }).filter(item => item !== null); // Remove any null entries
                
                const resultMagnetCrop = { data: jsonMagnetCrop };

                const formData = new FormData();
                 for (const key in TissueCulture) {
                    formData.append(key, TissueCulture[key]!==null?TissueCulture[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('beneficiary_id', selectbeniFiApplicant);
                  formData.append('district_id', nurseryTissueBeneficiary.district.district_id);
                  formData.append('division_id', nurseryTissueBeneficiary.divisions.division_id);
                  formData.append('taluka_id', nurseryTissueBeneficiary.taluka.taluka_id);
                  formData.append('beneficiary_type_id', nurseryTissueBeneficiary.beneficiary_type.beneficiary_type_id);
                  formData.append('component_id', selectTissueComponent);
                  formData.append('magnet_crop_id', JSON.stringify(resultMagnetCrop["data"]));
                  formData.append('capacity_total_plants', nurseryTissueBeneficiary.capacity_total_plants?nurseryTissueBeneficiary.capacity_total_plants:"");
                  formData.append('area', nurseryTissueBeneficiary.area ? nurseryTissueBeneficiary.area: '');
                  formData.append('location_taluka', nurseryTissueBeneficiary.location["location_taluka_id"] ? nurseryTissueBeneficiary.location["location_taluka_id"]:'');
                  formData.append('form_id', '5');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }


                try{
                    if(!nurseryFlagEdit){
                    // if(true){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitNurseryDevData = await axios.post(`${apiUrl}api/tissue-culture-create`, formData, getConfigWithToken())
                        if(submitNurseryDevData.data["status"] === true){
                            Swal.fire(
                                `${submitNurseryDevData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataTissue())
                        }else{
                            Swal.fire(
                                `${submitNurseryDevData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", nurseryIdEdit);
                        const submitNurseryData = await axios.post(`${apiUrl}api/tissue-culture-update`, formData, getConfigWithToken())
            
                        if(submitNurseryData.data["status"] === true){
                            Swal.fire(
                                `${submitNurseryData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataTissue())
                        }else{
                            Swal.fire(
                                `${submitNurseryData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting Tissue Devlopment form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting Tissue Devlopment form',
                    'Please try again later',
                    'error'
                );
            }
        }

    }

    const handleResearchSubmit = async (e) => {
        e.preventDefault()

        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["researchDev"];
            const errorMessages = {
                beneficiary_exp_incurred: 'Promoter Expenditure Incurred is required',
                grant_exp_incurred:'Term Loan Expenditure Incurred is required',
                exp_incurred: 'Expenditure Incurred is Required'
            };

            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsResearch(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        if(checkFPOSelect() && validateForm()){
            try{
                const TissueCulture = dd["payload"]["researchDev"]
                
                const jsonMagnetCrop = nurseryTissueBeneficiary["magnet_crop_id"].map(id => {
                    const matchingObject = magentCrop.find(item => item.id === id);
                    if (matchingObject) {
                      return {
                        id: matchingObject.id,
                        name: matchingObject.crop,
                      };
                    }
                    return null; // Handle cases where there's no matching ID
                  }).filter(item => item !== null); // Remove any null entries
                
                const resultMagnetCrop = { data: jsonMagnetCrop };

                const formData = new FormData();
                 for (const key in TissueCulture) {
                    formData.append(key, TissueCulture[key]!==null?TissueCulture[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('beneficiary_id', selectbeniFiApplicant);
                  formData.append('district_id', nurseryTissueBeneficiary.district.district_id);
                  formData.append('division_id', nurseryTissueBeneficiary.divisions.division_id);
                  formData.append('taluka_id', nurseryTissueBeneficiary.taluka.taluka_id);
                  formData.append('beneficiary_type_id', nurseryTissueBeneficiary.beneficiary_type.beneficiary_type_id);
                  formData.append('magnet_crop_id', JSON.stringify(resultMagnetCrop["data"]));
                  formData.append('sub_project_title', nurseryTissueBeneficiary.sub_project_title);
                  formData.append('signing_date', nurseryTissueBeneficiary.signing_date);
                  formData.append('form_id', '19');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }


                try{
                    if(!researchDevFlagEdit){
                    // if(true){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitNurseryDevData = await axios.post(`${apiUrl}api/research-dev-create`, formData, getConfigWithToken())
                        if(submitNurseryDevData.data["status"] === true){
                            Swal.fire(
                                `${submitNurseryDevData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataResearch())
                        }else{
                            Swal.fire(
                                `${submitNurseryDevData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", researchDevIdEdit);
                        const submitNurseryData = await axios.post(`${apiUrl}api/research-dev-update`, formData, getConfigWithToken())
            
                        if(submitNurseryData.data["status"] === true){
                            Swal.fire(
                                `${submitNurseryData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataResearch())
                        }else{
                            Swal.fire(
                                `${submitNurseryData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting Research Devlopment form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting Research Devlopment form',
                    'Please try again later',
                    'error'
                );
            }
        }
    }

    const handleBioWasteSubmit = async (e) => {
        e.preventDefault()
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["bioWaste"]
           
            const errorMessages = {
                beneficiary_exp_incurred: 'Promoter Expenditure Incurred is required',
                grant_exp_incurred:'Term Loan Expenditure Incurred is required',
                exp_incurred: 'Expenditure Incurred is Required'
            };
          

            for (const field in errorMessages) {
                const value = formData[field];
               
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
           
            dispatch(setValidationErrorsbiowaste(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        if(checkFPOSelect() && validateForm()){
            try{
                const BioWaste = dd["payload"]["bioWaste"]
                
                const jsonMagnetCrop = nurseryTissueBeneficiary["magnet_crop_id"].map(id => {
                    const matchingObject = magentCrop.find(item => item.id === id);
                    if (matchingObject) {
                      return {
                        id: matchingObject.id,
                        name: matchingObject.crop,
                      };
                    }
                    return null; // Handle cases where there's no matching ID
                  }).filter(item => item !== null); // Remove any null entries
                
                const resultMagnetCrop = { data: jsonMagnetCrop };

                const formData = new FormData();
                 for (const key in BioWaste) {
                    formData.append(key, BioWaste[key]!==null?BioWaste[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('beneficiary_id', selectbeniFiApplicant);
                  formData.append('district_id', nurseryTissueBeneficiary.district.district_id);
                  formData.append('division_id', nurseryTissueBeneficiary.divisions.division_id);
                  formData.append('taluka_id', nurseryTissueBeneficiary.taluka.taluka_id);
                  formData.append('beneficiary_type_id', nurseryTissueBeneficiary.beneficiary_type.beneficiary_type_id);
                  formData.append('magnet_crop_id', JSON.stringify(resultMagnetCrop["data"]));
                  formData.append('sub_project_title', nurseryTissueBeneficiary.sub_project_title);
                  formData.append('signing_date', nurseryTissueBeneficiary.signing_date);
                  formData.append('form_id', '18');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }


                try{
                    if(!researchDevFlagEdit){
                    // if(true){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitNurseryDevData = await axios.post(`${apiUrl}api/bio-west-wealth-create`, formData, getConfigWithToken())
                        if(submitNurseryDevData.data["status"] === true){
                            Swal.fire(
                                `${submitNurseryDevData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDatabiowaste())
                        }else{
                            Swal.fire(
                                `${submitNurseryDevData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", researchDevIdEdit);
                        const submitNurseryData = await axios.post(`${apiUrl}api/bio-west-wealth-update`, formData, getConfigWithToken())
            
                        if(submitNurseryData.data["status"] === true){
                            Swal.fire(
                                `${submitNurseryData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDatabiowaste())
                        }else{
                            Swal.fire(
                                `${submitNurseryData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting Bio Waste form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting Bio Waste form',
                    'Please try again later',
                    'error'
                );
            }
        }
    }

    const handleInnovationSubmit = async (e) => {
        e.preventDefault()

        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["innovation"];
            const errorMessages = {
                beneficiary_exp_incurred: 'Promoter Expenditure Incurred is required',
                grant_exp_incurred:'Term Loan Expenditure Incurred is required',
                exp_incurred: 'Expenditure Incurred is Required'
            };

            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsInnovation(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        if(checkFPOSelect() && validateForm()){
            try{
                const Innovation = dd["payload"]["innovation"]
                
                const jsonMagnetCrop = nurseryTissueBeneficiary["magnet_crop_id"].map(id => {
                    const matchingObject = magentCrop.find(item => item.id === id);
                    if (matchingObject) {
                      return {
                        id: matchingObject.id,
                        name: matchingObject.crop,
                      };
                    }
                    return null; // Handle cases where there's no matching ID
                  }).filter(item => item !== null); // Remove any null entries
                
                const resultMagnetCrop = { data: jsonMagnetCrop };

                const formData = new FormData();
                 for (const key in Innovation) {
                    formData.append(key, Innovation[key]!==null?Innovation[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('beneficiary_id', selectbeniFiApplicant);
                  formData.append('district_id', nurseryTissueBeneficiary.district.district_id);
                  formData.append('division_id', nurseryTissueBeneficiary.divisions.division_id);
                  formData.append('taluka_id', nurseryTissueBeneficiary.taluka.taluka_id);
                  formData.append('beneficiary_type_id', nurseryTissueBeneficiary.beneficiary_type.beneficiary_type_id);
                  formData.append('magnet_crop_id', JSON.stringify(resultMagnetCrop["data"]));
                  formData.append('sub_project_title', nurseryTissueBeneficiary.sub_project_title);
                  formData.append('signing_date', nurseryTissueBeneficiary.signing_date);
                  formData.append('form_id', '20');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }


                try{
                    if(!researchDevFlagEdit){
                    // if(true){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitNurseryDevData = await axios.post(`${apiUrl}api/innovation-in-packaging-create`, formData, getConfigWithToken())
                        if(submitNurseryDevData.data["status"] === true){
                            Swal.fire(
                                `${submitNurseryDevData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataInnovation())
                        }else{
                            Swal.fire(
                                `${submitNurseryDevData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", researchDevIdEdit);
                        const submitNurseryData = await axios.post(`${apiUrl}api/innovation-in-packaging-update`, formData, getConfigWithToken())
            
                        if(submitNurseryData.data["status"] === true){
                            Swal.fire(
                                `${submitNurseryData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataInnovation())
                        }else{
                            Swal.fire(
                                `${submitNurseryData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting Innovation Devlopment form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting Innovation Devlopment form',
                    'Please try again later',
                    'error'
                );
            }
        }
    }

    const handleIntroVarietySubmit = async (e) => {
        
        e.preventDefault()
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["introVariety"];
            const errorMessages = {
                beneficiary_exp_incurred: 'Promoter Expenditure Incurred is required',
                grant_exp_incurred:'Term Loan Expenditure Incurred is required',
                exp_incurred: 'Expenditure Incurred is Required'
            };

            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsIntro(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        if(checkFPOSelect() && validateForm()){
            try{
                const Innovation = dd["payload"]["introVariety"]
                
                const jsonMagnetCrop = nurseryTissueBeneficiary["magnet_crop_id"].map(id => {
                    const matchingObject = magentCrop.find(item => item.id === id);
                    if (matchingObject) {
                      return {
                        id: matchingObject.id,
                        name: matchingObject.crop,
                      };
                    }
                    return null; // Handle cases where there's no matching ID
                  }).filter(item => item !== null); // Remove any null entries
                
                const resultMagnetCrop = { data: jsonMagnetCrop };

                const formData = new FormData();
                 for (const key in Innovation) {
                    formData.append(key, Innovation[key]!==null?Innovation[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('beneficiary_id', selectbeniFiApplicant);
                  formData.append('district_id', nurseryTissueBeneficiary.district.district_id);
                  formData.append('division_id', nurseryTissueBeneficiary.divisions.division_id);
                  formData.append('taluka_id', nurseryTissueBeneficiary.taluka.taluka_id);
                  formData.append('beneficiary_type_id', nurseryTissueBeneficiary.beneficiary_type.beneficiary_type_id);
                  formData.append('magnet_crop_id', JSON.stringify(resultMagnetCrop["data"]));
                  formData.append('sub_project_title', nurseryTissueBeneficiary.sub_project_title);
                  formData.append('signing_date', nurseryTissueBeneficiary.signing_date);
                  formData.append('variety_name', nurseryTissueBeneficiary.variety_name);
                  formData.append('form_id', '21');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }


                try{
                    if(!researchDevFlagEdit){
                    // if(true){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitNurseryDevData = await axios.post(`${apiUrl}api/intro-to-new-variety-create`, formData, getConfigWithToken())
                        if(submitNurseryDevData.data["status"] === true){
                            Swal.fire(
                                `${submitNurseryDevData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataIntro())
                            resetAllState()
                        }else{
                            Swal.fire(
                                `${submitNurseryDevData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", researchDevIdEdit);
                        const submitNurseryData = await axios.post(`${apiUrl}api/intro-to-new-variety-update`, formData, getConfigWithToken())
            
                        if(submitNurseryData.data["status"] === true){
                            Swal.fire(
                                `${submitNurseryData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataIntro())
                            resetAllState()
                        }else{
                            Swal.fire(
                                `${submitNurseryData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting Intro Variety Devlopment form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting Intro Variety Devlopment form',
                    'Please try again later',
                    'error'
                );
            }
        }
    }

    const handleProductionSubmit = async (e) => {
        e.preventDefault()
        const formData = dd["payload"]["productionPlant"];
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["productionPlant"];
            const errorMessages = {
                disbursement_amt: "Disbursement Amount is Required",
                disbursement_date: "Disbursement Date is Required",
            };
        
            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsProductionPlant(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };


        if(checkFPOSelect() && validateForm()){
            try{
                const productionPlant = dd["payload"]["productionPlant"]
                const formData = new FormData();
                 for (const key in productionPlant) {
                    formData.append(key, productionPlant[key]!==null?productionPlant[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('beneficiary_id', selectbeniFiApplicant);
                  formData.append('district_id', nurseryTissueBeneficiary.district.district_id);
                  formData.append('division_id', nurseryTissueBeneficiary.divisions.division_id);
                  formData.append('taluka_id', nurseryTissueBeneficiary.taluka.taluka_id);
                  formData.append('beneficiary_type_id', nurseryTissueBeneficiary.beneficiary_type.beneficiary_type_id);
                  formData.append('training_type_id', selectedProductionPlant);
                  formData.append('form_id', '16');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }


                try{
                    if(!prodPlantFlagEdit){
                    // if(true){
                        const submitProductionPlant = await axios.post(`${apiUrl}api/production-plant-cirtification-create`, formData, getConfigWithToken())
                        if(submitProductionPlant.data["status"] === true){
                            Swal.fire(
                                `${submitProductionPlant.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataProductionPlant())
                        }else{
                            Swal.fire(
                                `${submitProductionPlant.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", prodPlantIdEdit)
                        const submitProductionPlant = await axios.post(`${apiUrl}api/production-plant-cirtification-update`, formData, getConfigWithToken())
            
                        if(submitProductionPlant.data["status"] === true){
                            Swal.fire(
                                `${submitProductionPlant.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataProductionPlant())
                        }else{
                            Swal.fire(
                                `${submitProductionPlant.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting Production Plant form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting Production Plant form',
                    'Please try again later',
                    'error'
                );
            }
        }
    }

    const handleSupportTechSubmit = async (e) => {
        e.preventDefault()

        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["supportTech"];
            const errorMessages = {
                promoters_exp_incurred: 'Promoter Expenditure Incurred is required',
                term_loan_exp_incurred:'Term Loan Expenditure Incurred is required',
            };

            if(isMatchingGrant === true){
                errorMessages["matching_grant_exp_incurred"]= 'Matching Grant Expenditure Incurred is required'
            }
            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsSupportTech(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };


        if(checkFPOSelect() && validateForm()){
            try{
                const supportTech = dd["payload"]["supportTech"]

                const formData = new FormData();
                 for (const key in supportTech) {
                    formData.append(key, supportTech[key]!==null?supportTech[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('beneficiary_id', selectbeniFiApplicant);
                  formData.append('district_id', nurseryTissueBeneficiary.district.district_id);
                  formData.append('division_id', nurseryTissueBeneficiary.divisions.division_id);
                  formData.append('taluka_id', nurseryTissueBeneficiary.taluka.taluka_id);
                  formData.append('beneficiary_type_id', nurseryTissueBeneficiary.beneficiary_type.beneficiary_type_id);
                  formData.append('new_technology_id', selectSupportComponent);
                  formData.append('form_id', '26');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }


                try{
                    if(!nurseryFlagEdit){
                    // if(true){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitSupportTechData = await axios.post(`${apiUrl}api/support-new-technology-create`, formData, getConfigWithToken())
                        if(submitSupportTechData.data["status"] === true){
                            Swal.fire(
                                `${submitSupportTechData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataSupportTech())
                        }else{
                            Swal.fire(
                                `${submitSupportTechData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", nurseryIdEdit);
                        const submitSupportTechData = await axios.post(`${apiUrl}api/support-new-technology-update`, formData, getConfigWithToken())
            
                        if(submitSupportTechData.data["status"] === true){
                            Swal.fire(
                                `${submitSupportTechData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataSupportTech())
                        }else{
                            Swal.fire(
                                `${submitSupportTechData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting Support Tech form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting Support Tech form',
                    'Please try again later',
                    'error'
                );
            }
        }
    }

    const handleStudyTourSubmit = async (e) => {
        e.preventDefault()
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["studyTourExpo"];
            const errorMessages = {
                country_id: 'Country is Required Field',
                from_date: 'Please Select From Date',
                to_date: 'Please Select To Date',
                location_address: 'Location is Required Field',
                purpose: 'Purpose is Required Field',
                participants_male: 'Participants Male is Required Field',
                participants_female: 'Participants Female is Required Field',
                participants_total: 'Participants Total is Required Field',
            };

            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsStudy(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        if(validateForm()){
            try{
                const studyTour = dd["payload"]["studyTourExpo"]

                const formData = new FormData();
                 for (const key in studyTour) {
                    formData.append(key, studyTour[key]!==null?studyTour[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('training_type_id', selectedStudyTourExpVisit)
                  formData.append('division_id', selectDivision)
                  formData.append('pmu_id', selectPMU)
                  formData.append('form_id', '25');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }


                try{
                    if(!studyTourFlagEdit){
                    // if(true){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitStudyTourData = await axios.post(`${apiUrl}api/study-tour-for-magnet-create`, formData, getConfigWithToken())
                        if(submitStudyTourData.data["status"] === true){
                            Swal.fire(
                                `${submitStudyTourData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataStudy())
                        }else{
                            Swal.fire(
                                `${submitStudyTourData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", studyTourIdEdit);
                        const submitStudyTourData = await axios.post(`${apiUrl}api/study-tour-for-magnet-update`, formData, getConfigWithToken())
            
                        if(submitStudyTourData.data["status"] === true){
                            Swal.fire(
                                `${submitStudyTourData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataStudy())
                        }else{
                            Swal.fire(
                                `${submitStudyTourData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting Support Tech form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting Support Tech form',
                    'Please try again later',
                    'error'
                );
            }
        }
    }

    const handleStudyTourFarmerSubmit = async (e) => {
        e.preventDefault()
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["studyTourFarmer"];
            const errorMessages = {
                division_id: 'Division is a Required Field',
                beneficiary_id: 'Beneficiary is a Required Field',
                magnet_crop_id: 'Magnet Crop is a Required Field',
                from_date: 'From Date is a Required Field',
                to_date: 'To Date is a Required Field',
                location_address: 'Location Address is a Required Field',
                purpose: 'Purpose is a Required Field',
                sc_male: 'SC Male is a Required Field',
                sc_female: 'SC Female is a Required Field',
                sc_total: 'SC Total is a Required Field',
                st_male: 'ST Male is a Required Field',
                st_female: 'ST Female is a Required Field',
                st_total: 'ST Total is a Required Field',
                pwd_male: 'PWD Male is a Required Field',
                pwd_female: 'PWD Female is a Required Field',
                pwd_total: 'PWD Total is a Required Field',
                bpl_male: 'BPL Male is a Required Field',
                bpl_female: 'BPL Female is a Required Field',
                bpl_total: 'BPL Total is a Required Field',
                obc_minority_open_male: 'OBC Minority Open Male is a Required Field',
                obc_minority_open_female: 'OBC Minority Open Female is a Required Field',
                obc_minority_open_total: 'OBC Minority Open Total is a Required Field',
              };

            if(selectedStudyTourFarmer === 14){
                errorMessages["district_id"]= 'District is a Required Field'
            }else if (selectedStudyTourFarmer === 15){
                errorMessages["state_id"]= 'State is a Required Field'
            }else if (selectedStudyTourFarmer === 16){
                errorMessages["country_id"]= 'Country is a Required Field'
            }
              

            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsFarmer(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };

        if(validateForm()){
            try{
                const studyTourFarmer = dd["payload"]["studyTourFarmer"]

                const jsonMagnetCrop = studyTourFarmer["magnet_crop_id"].map(id => {
                    const matchingObject = magentCrop.find(item => item.id === id);
                    if (matchingObject) {
                      return {
                        id: matchingObject.id,
                        name: matchingObject.crop,
                      };
                    }
                    return null; // Handle cases where there's no matching ID
                  }).filter(item => item !== null); // Remove any null entries
                
                const resultMagnetCrop = { data: jsonMagnetCrop };

                const formData = new FormData();
                 for (const key in studyTourFarmer) {
                    formData.append(key, studyTourFarmer[key]!==null?studyTourFarmer[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('training_type_id', selectedStudyTourFarmer)
                  formData.append('division_id', selectDivision)
                  formData.append('magnet_crop_id', JSON.stringify(resultMagnetCrop["data"]));
                  formData.append('beneficiary_id', beneStudyTourFarm)
                  formData.append('form_id', '24');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }


                try{
                    if(!studyTourFlagEdit){
                    // if(true){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitStudyTourFarmData = await axios.post(`${apiUrl}api/study-tour-for-farmers-create`, formData, getConfigWithToken())
                        if(submitStudyTourFarmData.data["status"] === true){
                            Swal.fire(
                                `${submitStudyTourFarmData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataFarmer())
                        }else{
                            Swal.fire(
                                `${submitStudyTourFarmData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", studyTourIdEdit);
                        const submitStudyTourFarmData = await axios.post(`${apiUrl}api/study-tour-for-farmers-update`, formData, getConfigWithToken())
            
                        if(submitStudyTourFarmData.data["status"] === true){
                            Swal.fire(
                                `${submitStudyTourFarmData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataFarmer())
                        }else{
                            Swal.fire(
                                `${submitStudyTourFarmData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting Study Farm Tech form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting Study Farm Tech form',
                    'Please try again later',
                    'error'
                );
            }
        }
    }

    const handleTradeFairSubmit = async (e) => {
        e.preventDefault()
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["tradeFair"];
            const errorMessages = {
                from_date: 'From Date is a Required Field',
                to_date: 'To Date is a Required Field',
                location_address: 'Location Address is a Required Field',
                name_of_trade_fair_exhibition: 'Name of Trade Fair Exhibition is a Required Field',
                participants_male: 'Participants Male is a Required Field',
                participants_female: 'Participants Female is a Required Field',
              };

            if (selectedTradeFareExhitbiton === 20){
                errorMessages["state_id"]= 'State is a Required Field'
            }else if (selectedTradeFareExhitbiton === 21){
                errorMessages["country_id"]= 'Country is a Required Field'
            }
              

            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsTradeFair(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };
        if(validateForm()){
            try{
                const tradeFair = dd["payload"]["tradeFair"]

                const formData = new FormData();
                 for (const key in tradeFair) {
                    formData.append(key, tradeFair[key]!==null?tradeFair[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('training_type_id', selectedTradeFareExhitbiton)
                  formData.append('division_id', selectKPMGDivision)
                  formData.append('beneficiary_id', beneStudyTourFarm)
                  formData.append('form_id', '33');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }


                try{
                    if(!studyTourFlagEdit){
                    // if(true){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitTradeFairData = await axios.post(`${apiUrl}api/trade-fair-exhibition-input-one-create`, formData, getConfigWithToken())
                        if(submitTradeFairData.data["status"] === true){
                            Swal.fire(
                                `${submitTradeFairData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataTradeFair())
                        }else{
                            Swal.fire(
                                `${submitTradeFairData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", studyTourIdEdit);
                        const submitTradeFairData = await axios.post(`${apiUrl}api/trade-fair-exhibition-input-one-update`, formData, getConfigWithToken())
            
                        if(submitTradeFairData.data["status"] === true){
                            Swal.fire(
                                `${submitTradeFairData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataTradeFair())
                        }else{
                            Swal.fire(
                                `${submitTradeFairData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting Study Farm Tech form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting Study Farm Tech form',
                    'Please try again later',
                    'error'
                );
            }
        }
    }
    
    const handleMarketPromSubmit = async (e) => {
        e.preventDefault()
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["marketPromotion"];
            const errorMessages = {
                from_date: 'From Date is a Required Field',
                to_date: 'To Date is a Required Field',
                location_taluka: 'Location Taluka is a Required Field',
                participants_male: 'Participants Male is a Required Field',
                participants_female: 'Participants Female is a Required Field',
              };

            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsMarketProm(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };
        
        if(validateForm()){
            try{
                const marketProm = dd["payload"]["marketPromotion"]

                const formData = new FormData();
                 for (const key in marketProm) {
                    formData.append(key, marketProm[key]!==null?marketProm[key]:"");
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('training_type_id', selectedMarketDevProm)
                  formData.append('division_id', selectKPMGDivision)
                  formData.append('beneficiary_id', beneStudyTourFarm)
                  formData.append('form_id', '32');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }


                try{
                    if(!studyTourFlagEdit){
                    // if(true){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitMarketPromData = await axios.post(`${apiUrl}api/market-dev-promotion-input-one-create`, formData, getConfigWithToken())
                        if(submitMarketPromData.data["status"] === true){
                            Swal.fire(
                                `${submitMarketPromData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataMarketProm())
                        }else{
                            Swal.fire(
                                `${submitMarketPromData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", studyTourIdEdit);
                        const submitMarketPromData = await axios.post(`${apiUrl}api/market-dev-promotion-input-one-update`, formData, getConfigWithToken())
            
                        if(submitMarketPromData.data["status"] === true){
                            Swal.fire(
                                `${submitMarketPromData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataMarketProm())
                        }else{
                            Swal.fire(
                                `${submitMarketPromData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting Study Farm Tech form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting Study Farm Tech form',
                    'Please try again later',
                    'error'
                );
            }
        }
    }

    const handleTrainingOfTrainerSubmit = async (e) => {
        e.preventDefault()
        
        const validateForm = () => {
            const errors = {};
            const formData = dd["payload"]["totTraining"];
            const errorMessages = {
                from_date: 'From Date is a Required Field',
                to_date: 'To Date is a Required Field',
                location_taluka: 'Taluka is required',
                location_address: 'Address is required',
                name_of_organization: 'Name of Organisation is required',
                nomination_by_division: 'Nomination of Division is required',
                participants_male: 'Participant Male is required',
                participants_female: 'Participant Female is required',
            };
        
            for (const field in errorMessages) {
                const value = formData[field];
                if (!value || (Array.isArray(value) && value.length === 0)) {
                    errors[field] = errorMessages[field];
                }
            }
            dispatch(setValidationErrorsTotTraining(errors));
            // Return true if the form is valid, otherwise return false
            return Object.keys(errors).length === 0;
        };
        
        if(validateForm()){
            try{
                const totTraining = dd["payload"]["totTraining"]

                const formData = new FormData();
                 for (const key in totTraining) {
                    if(key === 'nomination_by_division'){
                        formData.append(key, totTraining[key].join(','));
                    }else{
                        formData.append(key, totTraining[key]!==null?totTraining[key]:"");
                    }
                  }

                  formData.append('quarter', selectQuarter);
                  formData.append('division_id', selectDivision)
                  formData.append('name_of_the_person', nameOfThePerson)
                  formData.append('magnet_crop_id', tot_Magnet_crop_id.join(','))
                  formData.append('form_id', '23');

                
                let size = 0
                const maxSize = 5 * 1024 * 1024;
                if(selectCapacityFile.length !== 0){
                    for(let i=0; i<selectCapacityFile.length;i++){
                        size += selectCapacityFile[i].size
                        formData.append("remarks[]", selectCapacityFile[i])
                    }
                }
                if(size >= maxSize){
                    setErrorCapacityFile("File size should be less than 5 MB")
                    return
                }

                try{
                    if(!totTrainingFlagEdit){
                        if(selectCapacityFile.length === 0){
                            setErrorCapacityFile("Please Select File")
                            return
                        }
                        const submitToTData = await axios.post(`${apiUrl}api/tot-inputs-create`, formData, getConfigWithToken())
                        if(submitToTData.data["status"] === true){
                            Swal.fire(
                                `${submitToTData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataTotTraining())
                        }else{
                            Swal.fire(
                                `${submitToTData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }else{
                        // also have to set Id in object
                        formData.append("id", totTrainingIdEdit);
                        const submitToTData = await axios.post(`${apiUrl}api/tot-inputs-update`, formData, getConfigWithToken())
                        if(submitToTData.data["status"] === true){
                            Swal.fire(
                                `${submitToTData.data["message"]}`,
                                '',
                                'success'
                            )
                            dispatch(resetDataTotTraining())
                            setEditInput1Files([])
                        }else{
                            Swal.fire(
                                `${submitToTData.data["message"]}`,
                                '',
                                'warning'
                            )
                        }
                    }
                    
                }catch(error){
                    Swal.fire(
                        'An error occurred while submitting WorkShop form',
                        'Please try again later',
                        'error'
                    );
                }
            }catch(error){
                Swal.fire(
                    'An error occurred while submitting WorkShop form',
                    'Please try again later',
                    'error'
                );
            }
        } 

    }

    useEffect(()=>{
        const quarterData = async () => {
            try {
              const response = await axios.get(`${apiUrl}api/get-all-quarters`,getConfigWithToken());
              const applicant = await axios.get(`${apiUrl}api/get-all-beneficiary-master?all=1`,getConfigWithToken());
              const magnetCrop = await axios.get(`${apiUrl}api/getAllCrop?all=1`,getConfigWithToken())
              const taluka = await axios.get(`${apiUrl}api/get-all-taluka-with-village?all=1`,getConfigWithToken())
              const pmu = await axios.get(`${apiUrl}api/pmu-listing?all=1`, getConfigWithToken())
              const divisi = await axios.get(`${apiUrl}api/getAllDivisions?all=1`, getConfigWithToken())
              
              const pmuDivision = pmu.data["data"].map((e)=> ({'id':e.id, 'crop': e.pmu_name}))
              const applicantData = applicant.data["data"].map((e)=>({"id":e["id"],"beneficiary_name":e["beneficiary_name"]}))
              const studyFarm = applicant.data["data"].map((e)=>({"id":e["id"],"crop":e["beneficiary_name"]}))
              const quarter = response.data["data"].map((e)=>({"id":e["id"], "value":e["value"]}))
              const magnetCropData = magnetCrop.data["data"].map((e)=>({"id":e["id"], "crop":e["crop"]}))
console.log(magnetCropData)
              setStudyFarmApplicant(studyFarm)
              setQuarter(quarter);
              setSelectedQuarters(quarter[quarter.length-1]["id"])
              setMagentCrop(magnetCropData)
              setTaluka(taluka.data["data"])
    
              setBeniApplicant(applicantData); // Pass applicantData to setApplicant
              setDivision(divisi.data["data"])
              setPMU_PUNE(pmuDivision)
              setLoading(false);
            } catch (error) {
              console.error("Error fetching data:", error);
            }
          };
          
          quarterData();
        
        setLoading(false);
    },[])

    if (loading) {
        return (<>
            <Box sx={{ display: 'flex' }}>
            <CircularProgress />
            </Box>
        </>);
      }

    return(<>
        {/* <div class="contain"><h2>Example 3 </h2></div> */}
        <Online>
            <main id="main" class="main">
            <section class="section">
            <div class="row">
                <div id="exTab3" class="contain" style={{width: '78rem'}}>	
                
                <div class="card">
                <Box sx={{ width: '100%', typography: 'body1', bgcolor: 'background.paper' }}>
                    <TabContext value={value}>
                        <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                        <Tabs variant="scrollable" scrollButtons="auto" aria-label="scrollable auto tabs example" value={value} onChange={handleChange} sx={{ width: '100%', overflowX: 'auto' }}>
                            <Tab label="Capacity Building on GAP" value="1" className={`${value === '1' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Nursery Development" value="2" className={`${value === '2' ? 'activeClickCSS' : ''}`} />
                            <Tab label="Tissue Culture" value="3" className={`${value === '3' ? 'activeClickCSS' : ''}`}/>
                            {/* <Tab label="Capacity Building on GAP" value="4" className={`${value === '4' ? 'activeClickCSS' : ''}`}/> */}
                            <Tab label="Demonstrations: High-Density Plantation (HDP)" value="5" className={`${value === '5' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Capacity building on post-harvest management" value="6" className={`${value === '6' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Capacity building on SME Linkages" value="7" className={`${value === '7' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Market Development Assistense" value="8" className={`${value === '8' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Other Certification Cost" value="13" className={`${value === '13' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Production Plant" value="18" className={`${value === '18' ? 'activeClickCSS' : ''}`}/>
                            {/* <Tab label="Trade Fair & Exhibition" value="8" className={`${value === '8' ? 'activeClickCSS' : ''}`}/> */}
                            <Tab label="ValueChain acceleration Services" value="9" className={`${value === '9' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Study Tours For Farmers" value="0" className={`${value === '0' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Study Tours, Exposure visits" value="10" className={`${value === '10' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="G_Workshop" value="11" className={`${value === '11' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Training of Trainers (ToT) - 5 Days" value="12" className={`${value === '12' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Research Dev" value="15" className={`${value === '15' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Bio Waste" value="20" className={`${value === '20' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Innovation & Packaging" value="16" className={`${value === '16' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Intro to New Variety" value="17" className={`${value === '17' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Support New Technology" value="19" className={`${value === '19' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Trade Fair & Exhibition" value="21" className={`${value === '21' ? 'activeClickCSS' : ''}`}/>
                            <Tab label="Market Development and Promotion" value="22" className={`${value === '22' ? 'activeClickCSS' : ''}`}/>
                        </Tabs>
                        </Box>

                        {/*  Capacity Building on GAP  */}
                        <TabPanel value="1" >
                            <div className="tab-pane">
                                <form className="row g-3" onSubmit={handleCapacityBuildingSubmit}>
                                        <div className="quarterSelect">
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                    <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                        label="Quarter"
                                                        onChange={handleQuarterChange}
                                                    >
                                                    {(quarters).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                </Box>
                                            </div>
                                        </div>
                                    <div className="word-with-line">
                                        <span>Beneficiary Information</span>
                                        <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
                                    </div>
                                    <div className='col-md-4'>
                                        <Box sx={{ minWidth: "100%" }}>
                                            <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                <InputLabel htmlFor="demo-simple-select-label">FPO/VCO</InputLabel>
                                                <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectbeniFiApplicant}
                                                label="FPO/VCO"
                                                onChange={handleApplicantChangeESIFPO}
                                        
                                                >
                                                {(beniFiApplicant).map((e)=>(
                                                    <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                ))}
                                                </Select>
                                                <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                            </FormControl>
                                        </Box>
                                    </div>
                                    <InputField label="Division" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.divisions ? nurseryTissueBeneficiary.divisions["divisions"].toString(): ''}/>
                                    <InputField label="District" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.district ? nurseryTissueBeneficiary.district["district"].toString(): ''}/>
                                    <InputField label="Taluka" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.taluka ? nurseryTissueBeneficiary.taluka["taluka"].toString(): ''}/>
                                    <InputField label="Beneficiary Type" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.beneficiary_type ? nurseryTissueBeneficiary.beneficiary_type["beneficiary_type"].toString(): ''}/>
                                

                                <div className="word-with-line" style={{ margin: "1.5rem 0" }}>
                                    <span>{(selectedGAP===5 || selectedGAP === "")?"Details for": "Project Wise Information"} - {selectedGAP && GAPPH1.find(e=>e['id']===selectedGAP).type}</span>
                                    <div>
                                    <Box sx={{ minWidth: 390 }} style={{ backgroundColor: "#ededed" }}
                                    >
                                        <FormControl fullWidth>
                                            <InputLabel id="demo-simple-select-label">
                                                Training Type
                                            </InputLabel>
                                            <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectedGAP}
                                                onChange={handleOptionsGAP}
                                                label="Trainin"
                                            >
                                                {GAPPH1.map((option) => (
                                                <MenuItem value={option.id}>
                                                    {option.type}
                                                </MenuItem>
                                                ))}
                                            </Select>
                                        </FormControl>
                                    </Box>
                                    </div>
                                </div>

                                <div>
                                    {selectedGAP && <GapTraining magentCrop={magentCrop} compValue={selectedGAP}/>}
                                </div>

                                 {selectedGAP && <>
                                <InputField col="4" label="Action points/ Recommendation made" type="text" value={capacityActionPoint} onChange={handleFieldChangeActionPoints} />
                                    <div class="col-md-6" style={{ position: "relative", right: "15px", bottom: "10px", }}>
                                            <label for="inputNumber" class="col-lg col-form-label" >
                                            Choose File- upload GAP Report/Photos
                                            </label>
                                            <div class="col-sm-10">
                                            <input class="form-control" type="file" multiple id="formFile" onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                            <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                        </div>
                                    </div>
                                    <div>
                                        {editInput1Files && editInput1Files.map((e)=>(
                                            <div className="icon-container">
                                                <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                </a>
                                                <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                            </div>
                                        ))
                                        }   
                                    </div>
                                </>}
                                <div style={{ textAlign: "center" }}>
                                    <button type="submit" class="btn submitbtn">
                                    {capacityBuildingFlagEdit === true ? "Update":"Submit"}
                                    </button>
                                    <button type="reset" class="btn btn-secondary">
                                    {" "}
                                    Reset
                                    </button>
                                </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* Nursery Development */}
                        <TabPanel value="2">
                            <div className='tab-pane'>
                                <form class="row g-3" onSubmit={handleNurserySubmit}>
                                    <div className='quarterSelect'>
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                    <FormControl fullWidth>
                                                        <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                        <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                            label="Quarter"
                                                            onChange={handleQuarterChange}
                                                        >
                                                        {(quarters).map((e,key)=>(
                                                            <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                        ))}
                                                        </Select>
                                                    </FormControl>
                                                </Box>
                                            </div>
                                    </div>
                                        <div className='word-with-line'>
                                            <span>Beneficiary Information</span>
                                            <i class="fa-solid fa-angle-down" style={{color: "#4e2683"}}></i>
                                        </div>
                                        <div className='col-md-4'>
                                            <Box sx={{ minWidth: "100%" }}>
                                                <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                    <InputLabel htmlFor="demo-simple-select-label">FPO/VCO</InputLabel>
                                                    <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={selectbeniFiApplicant}
                                                    label="FPO/VCO"
                                                    onChange={handleApplicantChangeESIFPO}
                                            
                                                    >
                                                    {(beniFiApplicant).map((e)=>(
                                                        <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                    ))}
                                                    </Select>
                                                    <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                                </FormControl>
                                            </Box>
                                        </div>
                                        {/* <SelectField selectData={farmerCompanies}/> */}
                                        <InputField label="Division" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.divisions ? nurseryTissueBeneficiary.divisions["divisions"].toString(): ''}/>
                                        <InputField label="District" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.district ? nurseryTissueBeneficiary.district["district"].toString(): ''}/>
                                        <InputField label="Taluka" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.taluka ? nurseryTissueBeneficiary.taluka["taluka"].toString(): ''}/>
                                        <InputField label="Beneficiary Type" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.beneficiary_type ? nurseryTissueBeneficiary.beneficiary_type["beneficiary_type"].toString(): ''}/>
                                        {/* <MultiSelect magentCrop={magentCrop} value=""/> */}
                                        <div className='col-md-4' style={{paddingRight:0}}>
                                            <MultiSelect label="MAGNET Crops" data={magentCrop} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.magnet_crop_id ? nurseryTissueBeneficiary.magnet_crop_id :[] } readOnly={true}/>
                                        </div>
                                        <div className='col-md-4'>
                                            <Box sx={{ minWidth: "100%" }}>
                                                <FormControl fullWidth >
                                                    <InputLabel htmlFor="demo-simple-select-label">Location (Taluka)</InputLabel>
                                                    <Select
                                                        readOnly={true}
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.location ? nurseryTissueBeneficiary.location["location_taluka_id"]: ''}
                                                        label="Location (Taluka)"
                                                        // onChange={handleOptionstaluka}
                                                    >
                                                    {(taluka).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.taluka}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                {/* <FormHelperText style={{color: "#d32f2f"}}>{valueChain.validationErrors.location_taluka}</FormHelperText> */}
                                            </Box>
                                        </div>
                                        <InputField label="Area of Nursery in (Ha)" type="text" value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.area ? nurseryTissueBeneficiary.area :'' } readOnly={true}/>
                                        <InputField label="Capacity (Total Plants)" type="text" value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.capacity_total_plants ? nurseryTissueBeneficiary.capacity_total_plants : '' } readOnly={true}/>
                                    <br/>
                                        <div className='word-with-line' style={{margin: "1.5rem 0"}}>
                                                <span>Sanctioned cost break-up {selectNurseryComponent && `for ${component.find(e=>e['id']===selectNurseryComponent).technical_component}`}</span>
                                                <div>
                                            <Box sx={{ minWidth: 390 }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel id="demo-simple-select-label">Component</InputLabel>
                                                    <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={selectNurseryComponent} onChange={handleTechComponentChangeChange}
                                                    label="Comp"
                                                    >
                                                    {component.map((e) => (
                                                        <MenuItem key={e.id} value={e.id}>{e["technical_component"]}</MenuItem>
                                                    ))}
                                                    
                                                    </Select>
                                                </FormControl>
                                            </Box>
                                        </div>
                                        </div>
                                    <br/>

                                        {selectNurseryComponent && 
                                            <div>
                                                <NurseryMasterForm compName={selectNurseryComponent && component.find(e=>e['id']===selectNurseryComponent).technical_component} tranche={nurseryTissueBeneficiary && nurseryTissueBeneficiary.payment_tranche ? nurseryTissueBeneficiary.payment_tranche: ''} 
                                                matchingGrant={isMatchingGrant} />
                                                <div style={{    display: "flex", alignItems: "center"}}>
                                                    <div class="col-md-6" style={{position: "relative", right: "15px", bottom: "10px"}}>
                                                        <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                            <div class="col-sm-10">
                                                            <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                            <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                            </div>
                                                    </div>
                                                    <div>
                                                            {editInput1Files && editInput1Files.map((e)=>(
                                                                <div className="icon-container">
                                                                    <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                                    <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                                    </a>
                                                                    <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                                </div>
                                                            ))
                                                            }   
                                                    </div>
                                                </div>
                                            </div>}
                                            
                                    
                                        <div style={{textAlign:"center"}}>
                                            <button type="submit" class="btn submitbtn">{nurseryFlagEdit === true ? "Update":"Submit"}</button>
                                            <button type="reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    {/* Conditionally render the BasicModal component */}
                                </form>
                            </div>
                        </TabPanel>

                        {/* Tissue Culture */}
                        <TabPanel value='3'>
                            <div className='tab-pane'>
                                <form class="row g-3" onSubmit={handleTissueSubmit}>
                                    <div className='quarterSelect'>
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                    <FormControl fullWidth>
                                                        <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                        <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                            label="Quarter"
                                                            onChange={handleQuarterChange}
                                                        >
                                                        {(quarters).map((e,key)=>(
                                                            <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                        ))}
                                                        </Select>
                                                    </FormControl>
                                                </Box>
                                            </div>
                                        </div>
                                        <div className='word-with-line'>
                                            <span>Beneficiary Information</span>
                                            <i class="fa-solid fa-angle-down" style={{color: "#4e2683"}}></i>
                                        </div>
                                        <div className='col-md-4'>
                                            <Box sx={{ minWidth: "100%" }}>
                                                <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                    <InputLabel htmlFor="demo-simple-select-label">FPO/VCO</InputLabel>
                                                    <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={selectbeniFiApplicant}
                                                    label="FPO/VCO"
                                                    onChange={handleApplicantChangeESIFPO}
                                            
                                                    >
                                                    {(beniFiApplicant).map((e)=>(
                                                        <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                    ))}
                                                    </Select>
                                                    <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                                </FormControl>
                                            </Box>
                                        </div>
                                        {/* <SelectField selectData={farmerCompanies}/> */}
                                        <InputField label="Division" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.divisions ? nurseryTissueBeneficiary.divisions["divisions"].toString(): ''}/>
                                        <InputField label="District" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.district ? nurseryTissueBeneficiary.district["district"].toString(): ''}/>
                                        <InputField label="Taluka" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.taluka ? nurseryTissueBeneficiary.taluka["taluka"].toString(): ''}/>
                                        <InputField label="Beneficiary Type" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.beneficiary_type ? nurseryTissueBeneficiary.beneficiary_type["beneficiary_type"].toString(): ''}/>
                                        {/* <MultiSelect magentCrop={magentCrop} value=""/> */}
                                        <div className='col-md-4' style={{paddingRight:0}}>
                                            <MultiSelect label="MAGNET Crops" data={magentCrop} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.magnet_crop_id ? nurseryTissueBeneficiary.magnet_crop_id :[] } readOnly={true}/>
                                        </div>
                                        <div className='col-md-4'>
                                            <Box sx={{ minWidth: "100%" }}>
                                                <FormControl fullWidth >
                                                    <InputLabel htmlFor="demo-simple-select-label">Location (Taluka)</InputLabel>
                                                    <Select
                                                        readOnly={true}
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.location ? nurseryTissueBeneficiary.location["location_taluka_id"]: ''}
                                                        label="Location (Taluka)"
                                                        // onChange={handleOptionstaluka}
                                                    >
                                                    {(taluka).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.taluka}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                {/* <FormHelperText style={{color: "#d32f2f"}}>{valueChain.validationErrors.location_taluka}</FormHelperText> */}
                                            </Box>
                                        </div>
                                        <InputField label="Area of Nursery in (Ha)" type="text" value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.area ? nurseryTissueBeneficiary.area :'' } readOnly={true}/>
                                        <InputField label="Capacity (Total Plants)" type="text" value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.capacity_total_plants ? nurseryTissueBeneficiary.capacity_total_plants : '' } readOnly={true}/>
                                    <br/>
                                        <div className='word-with-line' style={{margin: "1.5rem 0"}}>
                                                <span>Sanctioned cost break-up {selectTissueComponent && `for ${component.find(e=>e['id']===selectTissueComponent).technical_component}`}</span>
                                                <div>
                                            <Box sx={{ minWidth: 390 }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel id="demo-simple-select-label">Component</InputLabel>
                                                    <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={selectTissueComponent} onChange={handleTechComponentChangeChange}
                                                    label="Comp"
                                                    >
                                                    {component.map((e) => (
                                                        <MenuItem key={e.id} value={e.id}>{e["technical_component"]}</MenuItem>
                                                    ))}
                                                    
                                                    </Select>
                                                </FormControl>
                                            </Box>
                                        </div>
                                        </div>
                                    <br/>

                                        {selectTissueComponent && 
                                            <div>
                                                <TissueCulture  compName={selectTissueComponent && component.find(e=>e['id']===selectTissueComponent).technical_component} tranche={nurseryTissueBeneficiary && nurseryTissueBeneficiary.payment_tranche ? nurseryTissueBeneficiary.payment_tranche: ''} 
                                                matchingGrant={isMatchingGrant} />
                                                <div style={{    display: "flex", alignItems: "center"}}>
                                                    <div class="col-md-6" style={{position: "relative", right: "15px", bottom: "10px"}}>
                                                        <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                            <div class="col-sm-10">
                                                            <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                            <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                            </div>
                                                    </div>
                                                    <div>
                                                            {editInput1Files && editInput1Files.map((e)=>(
                                                                <div className="icon-container">
                                                                    <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                                    <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                                    </a>
                                                                    <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                                </div>
                                                            ))
                                                            }   
                                                    </div>
                                                </div>
                                            </div>}
                                    
                                        <div style={{textAlign:"center"}}>
                                            <button type="submit" class="btn submitbtn">{nurseryFlagEdit === true ? "Update":"Submit"}</button>
                                            <button type="reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    {/* Conditionally render the BasicModal component */}
                                </form>
                            </div>
                        </TabPanel>
                        
                        {/* Demonstrations: High-Density Plantation (HDP) */}
                        <TabPanel value='5'>
                            <div className='tab-pane'>
                                <form class="row g-3" onSubmit={handleHighDensitySubmit}>
                                <div className='quarterSelect'>
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                    <FormControl fullWidth>
                                                        <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                        <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                            label="Quarter"
                                                            onChange={handleQuarterChange}
                                                        >
                                                        {(quarters).map((e,key)=>(
                                                            <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                        ))}
                                                        </Select>
                                                    </FormControl>
                                                </Box>
                                            </div>
                                    </div>
                                    <div className="word-with-line">
                                        <span>Beneficiary Information</span>
                                        <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
                                    </div>
                                    <div className='col-md-4'>
                                            <Box sx={{ minWidth: "100%" }}>
                                                <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                    <InputLabel htmlFor="demo-simple-select-label">FPO/VCO</InputLabel>
                                                    <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={selectbeniFiApplicant}
                                                    label="FPO/VCO"
                                                    onChange={handleHighDensityFPO}
                                            
                                                    >
                                                    {(beniFiApplicant).map((e)=>(
                                                        <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                    ))}
                                                    </Select>
                                                    <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                                </FormControl>
                                            </Box>
                                    </div>
                                    <InputField label="Division" type="text" readOnly={true} value={hdpbeneficiary && hdpbeneficiary.divisions ? hdpbeneficiary.divisions["divisions"].toString(): ''}/>
                                    <InputField label="District" type="text" readOnly={true} value={hdpbeneficiary && hdpbeneficiary.district ? hdpbeneficiary.district["district"].toString(): ''}/>
                                    <InputField label="Taluka" type="text" readOnly={true} value={hdpbeneficiary && hdpbeneficiary.taluka ? hdpbeneficiary.taluka["taluka"].toString(): ''}/>
                                    <InputField label="Beneficiary Type" type="text" readOnly={true} value={hdpbeneficiary && hdpbeneficiary.beneficiary_type ? hdpbeneficiary.beneficiary_type["beneficiary_type"].toString(): ''}/>
                                    <div className='col-md-4' style={{paddingRight:0}}>
                                        <MultiSelect label="Magnet Crops" data={magentCrop} value={hdpbeneficiary && hdpbeneficiary.magnet_crop_id ? hdpbeneficiary.magnet_crop_id: []}/>
                                    </div>
                                    {/* <div className='col-md-4'>
                                        <Box sx={{ minWidth: "100%" }}>
                                            <FormControl fullWidth placeholder="Location(taluka)">
                                                <InputLabel id="demo-simple-select-label">Location(Taluka)</InputLabel>
                                                    <Select labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectedtaluka}
                                                            // onChange={handleOptionstaluka}
                                                            label="location(taluka)"
                                                    >
                                                    {(taluka).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.taluka}</MenuItem>
                                                    ))}
                                                    </Select>
                                            </FormControl>
                                        </Box>
                                    </div> */}
                                    <InputField label="Location(Taluka)" type="text" readOnly={true} value={hdpbeneficiary && hdpbeneficiary.location ? hdpbeneficiary.location["location_taluka"].toString(): ''}/>
                                    <InputField label="Nos. of Farmers  Demonstrated" type="number" readOnly={true} value={hdpbeneficiary && hdpbeneficiary.no_of_farmers ? hdpbeneficiary.no_of_farmers.toString(): ''}/>
                                    <InputField label="Area of Plantation" type="text" readOnly={true} value={hdpbeneficiary && hdpbeneficiary.area ? hdpbeneficiary.area.toString(): ''}/>
                                    <InputField label="Total Plantation cost" type="number" endAr="(Lakhs)" readOnly={true} value={hdpbeneficiary && hdpbeneficiary.total_plantation_cost ? hdpbeneficiary.total_plantation_cost.toString(): ''}/>
                                    <InputField label="Total Saction Grant from MAGNET" type="number"  endAr="(Lakhs)" readOnly={true} value={hdpbeneficiary && hdpbeneficiary.total_saction_grant ? hdpbeneficiary.total_saction_grant.toString(): ''}/>
                                    <InputField label="Payment Tranche" readOnly={true} value={hdpbeneficiary && hdpbeneficiary.tranche ? hdpbeneficiary.tranche.payment_tranche.toString() : hdpbeneficiary.payment_tranche}/>
                                    <InputField label="Grant Disbursement Amount (in lakhs)" type="number" readOnly={true} value={hdpbeneficiary && hdpbeneficiary.grant_disbursement_amount ? hdpbeneficiary.grant_disbursement_amount.toString(): ''}/>
                                    {/* <InputField label="Grant Disbursement Date" type="number" readOnly={true} value={hdpbeneficiary && hdpbeneficiary.divisions ? hdpbeneficiary.divisions["divisions"].toString(): ''}/> */}
                                    <DatePick label="Grant Disbursement Date" className="custom-date-picker" disbrushDate={hdpbeneficiary && hdpbeneficiary.grant_disbursement_date ? hdpbeneficiary.grant_disbursement_date.toString(): ''} readOnly={true}/>
                                    <div className="word-with-line">
                                        <span>Details for - High Density Plantation (HDP)</span>
                                        <i class="fa-solid fa-angle-down"  style={{ color: "#4e2683" }}></i>
                                    </div>
                                    <HighDensity/>
                                <div style={{ textAlign: "center" }}>
                                        <button type="submit" class="btn submitbtn">{hdpDensityFlagEdit === true ? "Update":"Submit"}</button>
                                        <button type="reset" class="btn btn-secondary"> Reset</button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* Capacity building on post-harvest management */}
                        <TabPanel value='6'>
                            <div className="tab-pane">
                                <form class="row g-3" onSubmit={handlePostHarvest}>
                                        <div className="quarterSelect">
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                    <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                        label="Quarter"
                                                        onChange={handleQuarterChange}
                                                    >
                                                    {(quarters).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                </Box>
                                            </div>
                                        </div>
                                    <div className="word-with-line" style={{ margin: "1.5rem 0" }}>
                                        <span>Details of trainings for Current Quarter of - PH training for 3 days</span>
                                    </div>
                                    <br />
                                    <div className='col-md-4' style={{paddingRight:0}}>
                                        <Box sx={{ minWidth: "100%" }}>
                                            <FormControl fullWidth >
                                                <InputLabel id="demo-simple-select-label">
                                                Division
                                                </InputLabel>
                                                <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectKPMGDivision}
                                                onChange={handleDivisionKMPGChange}
                                                label="Division"
                                                >
                                                {division.map((e) => (
                                                    <MenuItem value={e.id}>{e.divisions}</MenuItem>
                                                ))}
                                                </Select>
                                                {/* <FormHelperText>{studyTourExpo.validationErrors.division_id}</FormHelperText> */}
                                            </FormControl>
                                        </Box>
                                    </div>
                                    <div className='col-md-4' style={{paddingRight:0}}>
                                        <MultiSelect label="Beneficiary" data={studyFarmApplicant} value={beneStudyTourFarm} onChange={handleBenefiStudyTourFarm}/>
                                    </div>
                                    <PHMtraining magentCrop={magentCrop}/>
                                    <div style={{    display: "flex", alignItems: "center"}}>
                                        <div class="col-md-6" style={{position: "relative", right: "15px", bottom: "10px"}}>
                                            <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                <div class="col-sm-10">
                                                <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                </div>
                                        </div>
                                        <div>
                                                {editInput1Files && editInput1Files.map((e)=>(
                                                    <div className="icon-container">
                                                        <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                        <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                        </a>
                                                        <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                    </div>
                                                ))
                                                }   
                                        </div>
                                    </div>
                                    <div style={{ textAlign: "center" }}>
                                        <button type="submit" class="btn submitbtn">{postHarvestFlagEdit === true ? "Update":"Submit"}</button>
                                        <button type="reset" class="btn btn-secondary">Reset </button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* Lead firm SME linkages */}
                        <TabPanel value='7'>
                            <div className="tab-pane">
                                <form class="row g-3" onSubmit={handleSMELinkageSubmit}>
                                        <div className="quarterSelect">
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                    <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                        label="Quarter"
                                                        onChange={handleQuarterChange}
                                                    >
                                                    {(quarters).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                </Box>
                                            </div>
                                        </div>
                                <div className="word-with-line">
                                    <span>Beneficiary Information</span>
                                    <i
                                    class="fa-solid fa-angle-down"
                                    style={{ color: "#4e2683" }}
                                    ></i>
                                </div>
                                    <div className='col-md-4'>
                                        <Box sx={{ minWidth: "100%" }}>
                                            <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                <InputLabel htmlFor="demo-simple-select-label">FPO/VCO</InputLabel>
                                                <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectbeniFiApplicant}
                                                label="FPO/VCO"
                                                onChange={handleApplicantChangeESIFPO}
                                        
                                                >
                                                {(beniFiApplicant).map((e)=>(
                                                    <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                ))}
                                                </Select>
                                                <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                            </FormControl>
                                        </Box>
                                    </div>
                                    <InputField label="Division" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.divisions ? nurseryTissueBeneficiary.divisions["divisions"].toString(): ''}/>
                                    <InputField label="District" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.district ? nurseryTissueBeneficiary.district["district"].toString(): ''}/>
                                    <InputField label="Taluka" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.taluka ? nurseryTissueBeneficiary.taluka["taluka"].toString(): ''}/>
                                    <InputField label="Beneficiary Type" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.beneficiary_type ? nurseryTissueBeneficiary.beneficiary_type["beneficiary_type"].toString(): ''}/>
                                <div className="word-with-line" style={{ margin: "1.5rem 0" }}>
                                    <span>Details for - Capacity building on SME Linkages</span>
                                </div>

                                <SMELinkage/>

                                <div style={{ textAlign: "center" }}>
                                        <button type="submit" class="btn submitbtn">{smeLinkageFlagEdit === true ? "Update":"Submit"}</button>
                                        <button type="reset" class="btn btn-secondary">Reset </button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* Market Level Assistence */}
                        <TabPanel value='8'>
                            <div className="tab-pane">
                                <form class="row g-3" onSubmit={handleMarketAssistSubmit}>
                                        <div className="quarterSelect">
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                    <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                        label="Quarter"
                                                        onChange={handleQuarterChange}
                                                    >
                                                    {(quarters).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                </Box>
                                            </div>
                                        </div>
                                <div className="word-with-line">
                                    <span>Beneficiary Information</span>
                                    <i
                                    class="fa-solid fa-angle-down"
                                    style={{ color: "#4e2683" }}
                                    ></i>
                                </div>
                                
                                <div className='col-md-4'>
                                        <Box sx={{ minWidth: "100%" }}>
                                            <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                <InputLabel htmlFor="demo-simple-select-label">FPO/VCO</InputLabel>
                                                <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectbeniFiApplicant}
                                                label="FPO/VCO"
                                                onChange={handleApplicantChangeESIFPO}
                                        
                                                >
                                                {(beniFiApplicant).map((e)=>(
                                                    <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                ))}
                                                </Select>
                                                <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                            </FormControl>
                                        </Box>
                                    </div>
                                    <InputField label="Division" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.divisions ? nurseryTissueBeneficiary.divisions["divisions"].toString(): ''}/>
                                    <InputField label="District" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.district ? nurseryTissueBeneficiary.district["district"].toString(): ''}/>
                                    <InputField label="Taluka" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.taluka ? nurseryTissueBeneficiary.taluka["taluka"].toString(): ''}/>
                                    <InputField label="Beneficiary Type" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.beneficiary_type ? nurseryTissueBeneficiary.beneficiary_type["beneficiary_type"].toString(): ''}/>

                                <div className="word-with-line" style={{ margin: "1.5rem 0" }}>
                                    <span> Project Wise Information - {selectedMarketLevel && MarketDevAss.find(e=>e['id']===selectedMarketLevel).type}</span>
                                        <div>
                                            <Box sx={{ minWidth: 390 }} style={{ backgroundColor: "#ededed" }}>
                                                <FormControl fullWidth>
                                                    <InputLabel id="demo-simple-select-label">
                                                        Certification
                                                    </InputLabel>
                                                    <Select
                                                        labelId="demo-simple-select-label"
                                                        id="demo-simple-select"                                                    
                                                        value={selectedMarketLevel}
                                                        onChange={handleOptionsMarketLevel}
                                                        label="Trainin"                                                   
                                                    >
                                                        {MarketDevAss.map((e) => (
                                                        <MenuItem value={e.id}>
                                                            {e.type}
                                                        </MenuItem>
                                                        ))}
                                                    </Select>
                                                </FormControl>
                                            </Box>
                                        </div>
                                </div>
                                <br />

                                <div>
                                    {selectedMarketLevel && <MarketAssistence compName={selectedMarketLevel && MarketDevAss.find(e=>e['id']===selectedMarketLevel).type}/>}
                                </div>

                                <div style={{ textAlign: "center" }}>
                                    <button type="submit" class="btn submitbtn">
                                    {marketLevelFlagEdit === true ? "Update":"Submit"}
                                    </button>
                                    <button type="reset" class="btn btn-secondary">
                                    {" "}
                                    Reset
                                    </button>
                                </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* ValueChain acceleration Services */}
                        <TabPanel value='9'>
                            <div className='tab-pane'>
                                <form class="row g-3" onSubmit={handleValueChainSubmit}>
                                        <div className="quarterSelect">
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                    <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                        label="Quarter"
                                                        onChange={handleQuarterChange}
                                                    >
                                                    {(quarters).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                </Box>
                                            </div>
                                        </div>
                                    <div className="word-with-line" style={{ margin: "1.5rem 0" }}>
                                        <span>Details of trainings for Current Quarter of - {selectedValueChain && ValueChain.find(e=>e['id']===selectedValueChain).type}</span>
                                        <div>
                                            <Box sx={{ minWidth: 390 }} style={{ backgroundColor: "#ededed" }}>
                                                <FormControl fullWidth>
                                                    <InputLabel id="demo-simple-select-label">
                                                        Training Type
                                                    </InputLabel>
                                                    <Select
                                                        labelId="demo-simple-select-label"
                                                        id="demo-simple-select"                                                    
                                                        value={selectedValueChain}
                                                        onChange={handleOptionsValueChain}
                                                        label="Trainin"                                                   
                                                    >
                                                        {ValueChain.map((e) => (
                                                        <MenuItem value={e.id}>
                                                            {e.type}
                                                        </MenuItem>
                                                        ))}
                                                    </Select>
                                                </FormControl>
                                            </Box>
                                        </div>
                                    </div>
                                    <br />
                                    
                                    <div>
                                        {selectedValueChain &&
                                    <>
                                    <div class="row g-3" style={{marginBottom:"10px"}}>
                                        <div className='col-md-4' style={{paddingRight:0}}>
                                            <Box sx={{ minWidth: "100%" }}>
                                                <FormControl fullWidth >
                                                    <InputLabel id="demo-simple-select-label">
                                                    Division
                                                    </InputLabel>
                                                    <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={selectKPMGDivision}
                                                    onChange={handleDivisionKMPGChange}
                                                    label="Division"
                                                    >
                                                    {division.map((e) => (
                                                        <MenuItem value={e.id}>{e.divisions}</MenuItem>
                                                    ))}
                                                    </Select>
                                                    {/* <FormHelperText>{studyTourExpo.validationErrors.division_id}</FormHelperText> */}
                                                </FormControl>
                                            </Box>
                                        </div>
                                        <div className='col-md-4' style={{paddingRight:0}}>
                                            <MultiSelect label="Beneficiary" data={studyFarmApplicant} value={beneStudyTourFarm} onChange={handleBenefiStudyTourFarm}/>
                                        </div>
                                        <WomenLedtraining magentCrop={magentCrop}/>
                                        <div style={{display: "flex", alignItems: "center"}}>
                                            <div class="col-md-6" style={{position: "relative", right: "25px"}}>
                                                <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                    <div class="col-sm-10">
                                                    <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                    <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                    </div>
                                            </div>
                                            <div>
                                                    {editInput1Files && editInput1Files.map((e)=>(
                                                        <div className="icon-container">
                                                            <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                            <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                            </a>
                                                            <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                        </div>
                                                    ))
                                                    }   
                                            </div>
                                        </div>
                                    </div>
                                    </>}
                                    </div>
                                    
                                    <div style={{ textAlign: "center" }}>
                                        <button type="submit" class="btn submitbtn">{valueChainFlagEdit === true ? "Update":"Submit"}</button>
                                        <button type="reset" class="btn btn-secondary">Reset </button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* Study Tour For Farmers*/}
                        <TabPanel value="0">
                        <div className="tab-pane">
                            <form class="row g-3" onSubmit={handleStudyTourFarmerSubmit}>
                                    <div className="quarterSelect">
                                        <div className='support'>
                                        </div>
                                        <div className='col-md-4' style={{paddingRight:0}}>
                                                    <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                    <FormControl fullWidth>
                                                        <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                        <Select
                                                                labelId="demo-simple-select-label"
                                                                id="demo-simple-select"
                                                                value={selectQuarter}
                                                            label="Quarter"
                                                            onChange={handleQuarterChange}
                                                        >
                                                        {(quarters).map((e,key)=>(
                                                            <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                        ))}
                                                        </Select>
                                                    </FormControl>
                                                    </Box>
                                        </div>
                                    </div>
                                    
                                    <div className="word-with-line" style={{ margin: "1.5rem 0" }}>
                                        <span>
                                            Details of trainings for Current Quarter of - {selectedStudyTourFarmer && StudyTourFarm.find(e=>e['id']=== selectedStudyTourFarmer).type}
                                        </span>
                                        <div>
                                        <Box
                                            sx={{ minWidth: 390 }}
                                            style={{ backgroundColor: "#ededed" }}
                                        >
                                            <FormControl fullWidth>
                                            <InputLabel id="demo-simple-select-label">
                                                Training Type
                                            </InputLabel>
                                            <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectedStudyTourFarmer}
                                                onChange={handleOptionsStudyFarmer}
                                                label="Trainin"
                                            >
                                                {StudyTourFarm.map((e) => (
                                                <MenuItem value={e.id}>
                                                    {e.type}
                                                </MenuItem>
                                                ))}
                                            </Select>
                                            </FormControl>
                                        </Box>
                                        </div>
                                    </div>
                                    <br />

                            <div>
                                {selectedStudyTourFarmer && 
                                <>
                                <div class="row g-3" style={{marginBottom:"10px"}}>
                                    <div className='col-md-4' style={{paddingRight:0}}>
                                        <Box sx={{ minWidth: "100%" }}>
                                            <FormControl fullWidth >
                                                <InputLabel id="demo-simple-select-label">
                                                Division
                                                </InputLabel>
                                                <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectDivision}
                                                onChange={handleDivisionIdChange}
                                                label="Division"
                                                >
                                                {division.map((e) => (
                                                    <MenuItem value={e.id}>{e.divisions}</MenuItem>
                                                ))}
                                                </Select>
                                                {/* <FormHelperText>{studyTourExpo.validationErrors.division_id}</FormHelperText> */}
                                            </FormControl>
                                        </Box>
                                    </div>
                                    <div className='col-md-4' style={{paddingRight:0}}>
                                        <MultiSelect label="Beneficiary" data={studyFarmApplicant} value={beneStudyTourFarm} onChange={handleBenefiStudyTourFarm}/>
                                    </div>
                                </div>
                                <StudyTourFarmer magnetCrop={magentCrop} compValue={selectedStudyTourFarmer}/>
                                
                                <div style={{display: "flex", alignItems: "center"}}>
                                    <div class="col-md-6" style={{position: "relative", right: "25px"}}>
                                        <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                            <div class="col-sm-10">
                                            <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                            <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                            </div>
                                    </div>
                                    <div>
                                            {editInput1Files && editInput1Files.map((e)=>(
                                                <div className="icon-container">
                                                    <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                    <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                    </a>
                                                    <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                </div>
                                            ))
                                            }   
                                    </div>
                                </div>
                                </>
                                }
                            </div>

                            <div style={{ textAlign: "center" }}>
                                <button type="submit" class="btn submitbtn">
                                {studyTourFlagEdit === true ? "Update":"Submit"}
                                </button>
                                <button type="reset" class="btn btn-secondary">
                                Reset
                                </button>
                            </div>
                            </form>
                        </div>
                        </TabPanel>

                        {/* Study Tours, Exposure visits */}
                        <TabPanel value='10'>
                            <div className="tab-pane">
                                <form class="row g-3" onSubmit={handleStudyTourSubmit}>
                                    <div className="quarterSelect">
                                        <div className='support'>
                                        </div>
                                        <div className='col-md-4' style={{paddingRight:0}}>
                                                    <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                    <FormControl fullWidth>
                                                        <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                        <Select
                                                                labelId="demo-simple-select-label"
                                                                id="demo-simple-select"
                                                                value={selectQuarter}
                                                            label="Quarter"
                                                            onChange={handleQuarterChange}
                                                        >
                                                        {(quarters).map((e,key)=>(
                                                            <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                        ))}
                                                        </Select>
                                                    </FormControl>
                                                    </Box>
                                        </div>
                                    </div>
                                    
                                    <div className="word-with-line" style={{ margin: "1.5rem 0" }}>
                                        <span>
                                            Details of trainings for Current Quarter of - {selectedStudyTourExpVisit && StudyExpVisit.find(e=>e['id']===selectedStudyTourExpVisit).type}
                                        </span>
                                        <div>
                                        <Box
                                            sx={{ minWidth: 390 }}
                                            style={{ backgroundColor: "#ededed" }}
                                        >
                                            <FormControl fullWidth>
                                            <InputLabel id="demo-simple-select-label">
                                                Training Type
                                            </InputLabel>
                                            <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectedStudyTourExpVisit}
                                                onChange={handleOptionsStudyTourExpVisit}
                                                label="Trainin"
                                            >
                                                {StudyExpVisit.map((e) => (
                                                <MenuItem value={e.id}>
                                                    {e.type}
                                                </MenuItem>
                                                ))}
                                            </Select>
                                            </FormControl>
                                        </Box>
                                        </div>
                                    </div>
                                    <br />
                                    <div>
                                        {selectedStudyTourExpVisit && 
                                        <>
                                        <div class="row g-3" style={{marginBottom:"10px"}}>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }}>
                                                    <FormControl fullWidth >
                                                        <InputLabel id="demo-simple-select-label">
                                                        Division
                                                        </InputLabel>
                                                        <Select
                                                        labelId="demo-simple-select-label"
                                                        id="demo-simple-select"
                                                        value={selectDivision}
                                                        onChange={handleDivisionIdChange}
                                                        label="Division"
                                                        >
                                                        {division.map((e) => (
                                                            <MenuItem value={e.id}>{e.divisions}</MenuItem>
                                                        ))}
                                                        </Select>
                                                        {/* <FormHelperText>{studyTourExpo.validationErrors.division_id}</FormHelperText> */}
                                                    </FormControl>
                                                </Box>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <MultiSelect label="PMU_Pune /PIU-Division Name" data={PMU_Pune} value={selectPMU} onChange={handlePmuIdChange}/>
                                            </div>
                                        </div>
                                        <StudyTourMagnet compValue={selectedStudyTourExpVisit} />
                                        <div style={{display: "flex", alignItems: "center"}}>
                                            <div class="col-md-6" style={{position: "relative", right: "25px"}}>
                                                <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                    <div class="col-sm-10">
                                                    <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                    <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                    </div>
                                            </div>
                                            <div>
                                                    {editInput1Files && editInput1Files.map((e)=>(
                                                        <div className="icon-container">
                                                            <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                            <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                            </a>
                                                            <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                        </div>
                                                    ))
                                                    }   
                                            </div>
                                        </div>
                                        </>
                                        }
                                    </div>

                                    <div style={{ textAlign: "center" }}>
                                        <button type="submit" class="btn submitbtn">
                                        {studyTourFlagEdit === true ? "Update":"Submit"}
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                        Reset
                                        </button>
                                    </div>
                                </form>
                            </div>                       
                        </TabPanel>
                        
                        {/* G_Workshop */}
                        <TabPanel value='11'>
                        <div className='tab-pane'>
                                <form class="row g-3" onSubmit={handleWorkShopSubmit}>
                                    <div className="quarterSelect">
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                    <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                        label="Quarter"
                                                        onChange={handleQuarterChange}
                                                    >
                                                    {(quarters).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                </Box>
                                            </div>
                                        </div>

                                    <div className="word-with-line">
                                        <span>Details of Training workshop for IAs, PIU, FPOs, Value chain operators, and other stakeholders</span>
                                        <i class="fa-solid fa-angle-down" style={{color: "#4e2683"}}></i>
                                    </div>
                                    <div className='col-md-4'>
                                        <Box sx={{ minWidth: "100%" }}>
                                            <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                <InputLabel htmlFor="demo-simple-select-label">FPO/VCO</InputLabel>
                                                <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectbeniFiApplicant}
                                                label="FPO/VCO"
                                                onChange={handleApplicantChangeESIFPO}
                                        
                                                >
                                                {(beniFiApplicant).map((e)=>(
                                                    <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                ))}
                                                </Select>
                                                <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                            </FormControl>
                                        </Box>
                                    </div>
                                    <Workshop magentCrop={magentCrop}/>
                                    <div style={{display: "flex", alignItems: "center"}}>
                                        <div class="col-md-6" style={{position: "relative", right: "25px"}}>
                                            <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                <div class="col-sm-10">
                                                <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                </div>
                                        </div>
                                        <div>
                                                {editInput1Files && editInput1Files.map((e)=>(
                                                    <div className="icon-container">
                                                        <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                        <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                        </a>
                                                        <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                    </div>
                                                ))
                                                }   
                                        </div>
                                    </div>
                                    <div style={{ textAlign: "center" }}>
                                        <button type="submit" class="btn submitbtn"> {workShopFlagEdit === true ? "Update":"Submit"}</button>
                                        <button type="reset" class="btn btn-secondary">Reset</button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* 1.(2) TOT */}
                        <TabPanel value="12">
                            <div className='tab-pane'>
                                <form class="row g-3" onSubmit={handleTrainingOfTrainerSubmit}>
                                        <div className="quarterSelect">
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                    <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                        label="Quarter"
                                                        onChange={handleQuarterChange}
                                                    >
                                                    {(quarters).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                </Box>
                                            </div>
                                        </div>
                                    <div className="word-with-line">
                                        <span>Trainer training (ToT) GAP & PH- Input Records</span>
                                        <i class="fa-solid fa-angle-down" style={{color: "#4e2683"}}></i>
                                    </div>
                                    <InputField label="Name of the Person" type="text" value={nameOfThePerson} onChange={handleFieldNameOfPerson} />
                                    <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }}>
                                                    <FormControl fullWidth >
                                                        <InputLabel id="demo-simple-select-label">
                                                        Division
                                                        </InputLabel>
                                                        <Select
                                                        labelId="demo-simple-select-label"
                                                        id="demo-simple-select"
                                                        value={selectDivision}
                                                        onChange={handleDivisionIdChange}
                                                        label="Division"
                                                        >
                                                        {division.map((e) => (
                                                            <MenuItem value={e.id}>{e.divisions}</MenuItem>
                                                        ))}
                                                        </Select>
                                                        {/* <FormHelperText>{studyTourExpo.validationErrors.division_id}</FormHelperText> */}
                                                    </FormControl>
                                                </Box>
                                            </div>
                                    <div className='col-md-4' style={{paddingRight:0}}>
                                        <MultiSelect label="MAGNET Crops" data={magentCrop} value={tot_Magnet_crop_id} onChange={handleTotMagnetCrop}/>
                                    </div>
                                    <TotTraining/>
                                    <div style={{    display: "flex", alignItems: "center"}}>
                                        <div class="col-md-6" style={{position: "relative", right: "15px", bottom: "10px"}}>
                                            <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                <div class="col-sm-10">
                                                <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                </div>
                                        </div>
                                        <div>
                                                {editInput1Files && editInput1Files.map((e)=>(
                                                    <div className="icon-container">
                                                        <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                        <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                        </a>
                                                        <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                    </div>
                                                ))
                                                }   
                                        </div>
                                    </div>
                                <div style={{ textAlign: "center" }}>
                                        <button type="submit" class="btn submitbtn">{totTrainingFlagEdit === true ? "Update":"Submit"}</button>
                                        <button type="reset" class="btn btn-secondary">Reset </button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* Other Certification Cost */}
                        <TabPanel value='13'>
                            <div className="tab-pane">
                                <form class="row g-3" onSubmit={handleOtherCertificationSubmit}>
                                        <div className="quarterSelect">
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                    <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                        label="Quarter"
                                                        onChange={handleQuarterChange}
                                                    >
                                                    {(quarters).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                </Box>
                                            </div>
                                        </div>
                                    <div className="word-with-line">
                                        <span>Beneficiary Information</span>
                                        <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
                                    </div>
                                    <div className='col-md-4'>
                                        <Box sx={{ minWidth: "100%" }}>
                                            <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                <InputLabel htmlFor="demo-simple-select-label">FPO/VCO</InputLabel>
                                                <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectbeniFiApplicant}
                                                label="FPO/VCO"
                                                onChange={handleApplicantChangeESIFPO}
                                        
                                                >
                                                {(beniFiApplicant).map((e)=>(
                                                    <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                ))}
                                                </Select>
                                                <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                            </FormControl>
                                        </Box>
                                    </div>
                                    <InputField label="Division" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.divisions ? nurseryTissueBeneficiary.divisions["divisions"].toString(): ''}/>
                                    <InputField label="District" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.district ? nurseryTissueBeneficiary.district["district"].toString(): ''}/>
                                    <InputField label="Taluka" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.taluka ? nurseryTissueBeneficiary.taluka["taluka"].toString(): ''}/>
                                    <InputField label="Beneficiary Type" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.beneficiary_type ? nurseryTissueBeneficiary.beneficiary_type["beneficiary_type"].toString(): ''}/>

                                <div
                                    className="word-with-line"
                                    style={{ margin: "1.5rem 0" }}
                                >
                                    <span>
                                    Project Wise Information - {selectedOtherCertification && OtherCert.find(e=>e['id']===selectedOtherCertification).type}
                                    </span>
                                    <div>
                                    <Box
                                        sx={{ minWidth: 390 }}
                                        style={{ backgroundColor: "#ededed" }}
                                    >
                                        <FormControl fullWidth>
                                        <InputLabel id="demo-simple-select-label">
                                            Certification
                                        </InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={selectedOtherCertification}
                                            onChange={handleOptionsOtherCertCost}
                                            label="Trainin"
                                        >
                                            {OtherCert.map((e) => (
                                            <MenuItem value={e.id}>
                                                {e.type}
                                            </MenuItem>
                                            ))}
                                        </Select>
                                        </FormControl>
                                    </Box>
                                    </div>
                                </div>
                                <br />

                                <div>
                                    {selectedOtherCertification && <OtherCertification compName={selectedOtherCertification && OtherCert.find(e=>e['id']===selectedOtherCertification).type}/>}
                                </div>
                                {selectedOtherCertification && 

                                <div class="col-md-6" style={{position: "relative", right: "15px", bottom: "10px"}}>
                                    <label for="inputNumber" class="col-lg col-form-label">Remarks (Upload Progress Report / Photos)</label>
                                    <div class="col-sm-10">
                                        <input class="form-control" type="file" multiple id="formFile" onChange={handleMultiFilesOtherCertifChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                        <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                    </div>
                                </div>}
                                {/* <div className='col-md-4'>
                                    {editInput1Files && 
                                    (
                                        (editInput1Files).map((e)=>(
                                            <div className="icon-container">
                                            <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                            <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                            </a>
                                            <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.file_id)}></i>
                                        </div>
                                        ))
                                        
                                    )
                                    }   
                                </div>
                                     */}
                                
                                
                                <div style={{ textAlign: "center" }}>
                                    <button type="submit" class="btn submitbtn">
                                    {otherCertFlagEdit === true ? "Update":"Submit"}
                                    </button>
                                    <button type="reset" class="btn btn-secondary">
                                    {" "}
                                    Reset
                                    </button>
                                </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* Research Dev */}
                        <TabPanel value='15'>
                            <div className="tab-pane">
                                <form class="row g-3" onSubmit={handleResearchSubmit}>
                                    <div className="quarterSelect">
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                    <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                        label="Quarter"
                                                        onChange={handleQuarterChange}
                                                    >
                                                    {(quarters).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                </Box>
                                            </div>
                                        </div>
                                        <div className="word-with-line">
                                        <span>Beneficiary Information</span>
                                        <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
                                    </div>
                                    <div className='col-md-4'>
                                        <Box sx={{ minWidth: "100%" }}>
                                            <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                <InputLabel htmlFor="demo-simple-select-label">FPO/VCO/CMRC</InputLabel>
                                                <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectbeniFiApplicant}
                                                label="FPO/VCO/CMRC"
                                                onChange={handleApplicantChangeESIFPO}
                                        
                                                >
                                                {(beniFiApplicant).map((e)=>(
                                                    <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                ))}
                                                </Select>
                                                <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                            </FormControl>
                                        </Box>
                                    </div>
                                    <InputField label="Division" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.divisions ? nurseryTissueBeneficiary.divisions["divisions"].toString(): ''}/>
                                    <InputField label="District" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.district ? nurseryTissueBeneficiary.district["district"].toString(): ''}/>
                                    <InputField label="Taluka" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.taluka ? nurseryTissueBeneficiary.taluka["taluka"].toString(): ''}/>
                                    <InputField label="Beneficiary Type" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.beneficiary_type ? nurseryTissueBeneficiary.beneficiary_type["beneficiary_type"].toString(): ''}/>
                                    <div className='col-md-4' style={{paddingRight:0}}>
                                            <MultiSelect label="MAGNET Crops" data={magentCrop} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.magnet_crop_id ? nurseryTissueBeneficiary.magnet_crop_id :[] } readOnly={true}/>
                                        </div>
                                    <InputField label="Sub Project Title" type="text" value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.sub_project_title ? nurseryTissueBeneficiary.sub_project_title :''} readOnly={true} />
                                    <DatePick label="MOUSigningdate" className="custom-date-picker" disbrushDate={nurseryTissueBeneficiary && nurseryTissueBeneficiary.signing_date ? nurseryTissueBeneficiary.signing_date.toString(): ''} readOnly={true}/>
                                    
                                    {/* <div className='col-md-4' style={{paddingRight:0}}>
                                        <MultiSelect label="Magnet Crops" data={magentCrop} value={BioWastebeneficiary && BioWastebeneficiary.magnet_crop_id ? BioWastebeneficiary.magnet_crop_id: []}/>
                                    </div> */}
                                    <ResearchDev/>
                                    <div style={{    display: "flex", alignItems: "center"}}>
                                        <div class="col-md-6" style={{position: "relative", right: "15px", bottom: "10px"}}>
                                            <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                <div class="col-sm-10">
                                                <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                </div>
                                        </div>
                                        <div>
                                                {editInput1Files && editInput1Files.map((e)=>(
                                                    <div className="icon-container">
                                                        <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                        <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                        </a>
                                                        <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                    </div>
                                                ))
                                                }   
                                        </div>
                                    </div>
                                    
                                    <div style={{ textAlign: "center" }}>
                                        <button type="submit" class="btn submitbtn">
                                        {researchDevFlagEdit === true ? "Update":"Submit"}
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                        Reset
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* Innovation & Packaging */}
                        <TabPanel value='16'>
                            <div className="tab-pane">
                                <form class="row g-3" onSubmit={handleInnovationSubmit}>
                                    <div className="quarterSelect">
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                    <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                        label="Quarter"
                                                        onChange={handleQuarterChange}
                                                    >
                                                    {(quarters).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                </Box>
                                            </div>
                                        </div>
                                        <div className="word-with-line">
                                        <span>Beneficiary Information</span>
                                        <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
                                    </div>
                                    <div className='col-md-4'>
                                        <Box sx={{ minWidth: "100%" }}>
                                            <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                <InputLabel htmlFor="demo-simple-select-label">FPO/VCO/CMRC</InputLabel>
                                                <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectbeniFiApplicant}
                                                label="FPO/VCO/CMRC"
                                                onChange={handleApplicantChangeESIFPO}
                                        
                                                >
                                                {(beniFiApplicant).map((e)=>(
                                                    <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                ))}
                                                </Select>
                                                <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                            </FormControl>
                                        </Box>
                                    </div>
                                    <InputField label="Division" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.divisions ? nurseryTissueBeneficiary.divisions["divisions"].toString(): ''}/>
                                    <InputField label="District" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.district ? nurseryTissueBeneficiary.district["district"].toString(): ''}/>
                                    <InputField label="Taluka" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.taluka ? nurseryTissueBeneficiary.taluka["taluka"].toString(): ''}/>
                                    <InputField label="Beneficiary Type" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.beneficiary_type ? nurseryTissueBeneficiary.beneficiary_type["beneficiary_type"].toString(): ''}/>
                                    <div className='col-md-4' style={{paddingRight:0}}>
                                            <MultiSelect label="MAGNET Crops" data={magentCrop} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.magnet_crop_id ? nurseryTissueBeneficiary.magnet_crop_id :[] } readOnly={true}/>
                                        </div>
                                    <InputField label="Sub Project Title" type="text" value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.sub_project_title ? nurseryTissueBeneficiary.sub_project_title :''} readOnly={true} />
                                    <DatePick label="MOUSigningdate" className="custom-date-picker" disbrushDate={nurseryTissueBeneficiary && nurseryTissueBeneficiary.signing_date ? nurseryTissueBeneficiary.signing_date.toString(): ''} readOnly={true}/>
                                    
                                    {/* <div className='col-md-4' style={{paddingRight:0}}>
                                        <MultiSelect label="Magnet Crops" data={magentCrop} value={BioWastebeneficiary && BioWastebeneficiary.magnet_crop_id ? BioWastebeneficiary.magnet_crop_id: []}/>
                                    </div> */}
                                    <InnovationPackaging/>
                                    <div style={{    display: "flex", alignItems: "center"}}>
                                        <div class="col-md-6" style={{position: "relative", right: "15px", bottom: "10px"}}>
                                            <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                <div class="col-sm-10">
                                                <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                </div>
                                        </div>
                                        <div>
                                                {editInput1Files && editInput1Files.map((e)=>(
                                                    <div className="icon-container">
                                                        <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                        <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                        </a>
                                                        <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                    </div>
                                                ))
                                                }   
                                        </div>
                                    </div>
                                    
                                    <div style={{ textAlign: "center" }}>
                                        <button type="submit" class="btn submitbtn">
                                        {researchDevFlagEdit === true ? "Update":"Submit"}
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                        Reset
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* Intro to New Variety */}
                        <TabPanel value='17'>
                            <div className="tab-pane">
                                <form class="row g-3" onSubmit={handleIntroVarietySubmit}>
                                    <div className="quarterSelect">
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                    <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                        label="Quarter"
                                                        onChange={handleQuarterChange}
                                                    >
                                                    {(quarters).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                </Box>
                                            </div>
                                        </div>
                                        <div className="word-with-line">
                                        <span>Beneficiary Information</span>
                                        <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
                                    </div>
                                    <div className='col-md-4'>
                                        <Box sx={{ minWidth: "100%" }}>
                                            <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                <InputLabel htmlFor="demo-simple-select-label">FPO/VCO/CMRC</InputLabel>
                                                <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectbeniFiApplicant}
                                                label="FPO/VCO/CMRC"
                                                onChange={handleApplicantChangeESIFPO}
                                        
                                                >
                                                {(beniFiApplicant).map((e)=>(
                                                    <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                ))}
                                                </Select>
                                                <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                            </FormControl>
                                        </Box>
                                    </div>
                                    <InputField label="Division" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.divisions ? nurseryTissueBeneficiary.divisions["divisions"].toString(): ''}/>
                                    <InputField label="District" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.district ? nurseryTissueBeneficiary.district["district"].toString(): ''}/>
                                    <InputField label="Taluka" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.taluka ? nurseryTissueBeneficiary.taluka["taluka"].toString(): ''}/>
                                    <InputField label="Beneficiary Type" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.beneficiary_type ? nurseryTissueBeneficiary.beneficiary_type["beneficiary_type"].toString(): ''}/>
                                    <div className='col-md-4' style={{paddingRight:0}}>
                                        <MultiSelect label="MAGNET Crops" data={magentCrop} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.magnet_crop_id ? nurseryTissueBeneficiary.magnet_crop_id :[] } readOnly={true}/>
                                    </div>
                                    <InputField label="Variety Name" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.variety_name ? nurseryTissueBeneficiary.variety_name.toString(): ''}/>
                                    <InputField label="Sub Project Title" type="text" value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.sub_project_title ? nurseryTissueBeneficiary.sub_project_title :''} readOnly={true} />
                                    <DatePick label="MOUSigningdate" className="custom-date-picker" disbrushDate={nurseryTissueBeneficiary && nurseryTissueBeneficiary.signing_date ? nurseryTissueBeneficiary.signing_date.toString(): ''} readOnly={true}/>
                                    
                                    {/* <div className='col-md-4' style={{paddingRight:0}}>
                                        <MultiSelect label="Magnet Crops" data={magentCrop} value={BioWastebeneficiary && BioWastebeneficiary.magnet_crop_id ? BioWastebeneficiary.magnet_crop_id: []}/>
                                    </div> */}
                                    <IntroVariety/>
                                    <div style={{    display: "flex", alignItems: "center"}}>
                                        <div class="col-md-6" style={{position: "relative", right: "15px", bottom: "10px"}}>
                                            <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                <div class="col-sm-10">
                                                <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                </div>
                                        </div>
                                        <div>
                                                {editInput1Files && editInput1Files.map((e)=>(
                                                    <div className="icon-container">
                                                        <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                        <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                        </a>
                                                        <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                    </div>
                                                ))
                                                }   
                                        </div>
                                    </div>
                                    
                                    <div style={{ textAlign: "center" }}>
                                        <button type="submit" class="btn submitbtn">
                                        {researchDevFlagEdit === true ? "Update":"Submit"}
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                        Reset
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* Production Plant Certification */}
                        <TabPanel value='18'>
                            <div className="tab-pane">
                                <form class="row g-3" onSubmit={handleProductionSubmit}>
                                        <div className="quarterSelect">
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                    <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                        label="Quarter"
                                                        onChange={handleQuarterChange}
                                                    >
                                                    {(quarters).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                </Box>
                                            </div>
                                        </div>
                                    <div className="word-with-line">
                                        <span>Beneficiary Information</span>
                                        <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
                                    </div>
                                    <div className='col-md-4'>
                                        <Box sx={{ minWidth: "100%" }}>
                                            <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                <InputLabel htmlFor="demo-simple-select-label">FPO/VCO</InputLabel>
                                                <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectbeniFiApplicant}
                                                label="FPO/VCO"
                                                onChange={handleApplicantChangeESIFPO}
                                        
                                                >
                                                {(beniFiApplicant).map((e)=>(
                                                    <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                ))}
                                                </Select>
                                                <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                            </FormControl>
                                        </Box>
                                    </div>
                                    <InputField label="Division" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.divisions ? nurseryTissueBeneficiary.divisions["divisions"].toString(): ''}/>
                                    <InputField label="District" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.district ? nurseryTissueBeneficiary.district["district"].toString(): ''}/>
                                    <InputField label="Taluka" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.taluka ? nurseryTissueBeneficiary.taluka["taluka"].toString(): ''}/>
                                    <InputField label="Beneficiary Type" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.beneficiary_type ? nurseryTissueBeneficiary.beneficiary_type["beneficiary_type"].toString(): ''}/>
                                                    
                                <div
                                    className="word-with-line"
                                    style={{ margin: "1.5rem 0" }}
                                >
                                    <span>
                                    Project Wise Information - {selectedProductionPlant && ProdPlant.find(e=>e['id']===selectedProductionPlant).type}
                                    </span>
                                    <div>
                                    <Box
                                        sx={{ minWidth: 390 }}
                                        style={{ backgroundColor: "#ededed" }}
                                    >
                                        <FormControl fullWidth>
                                        <InputLabel id="demo-simple-select-label">
                                            Certification
                                        </InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={selectedProductionPlant}
                                            onChange={handleOptionsProductionPlant}
                                            label="Trainin"
                                        >
                                            {ProdPlant.map((e) => (
                                            <MenuItem value={e.id}>
                                                {e.type}
                                            </MenuItem>
                                            ))}
                                        </Select>
                                        </FormControl>
                                    </Box>
                                    </div>
                                </div>
                                <br />

                                <div>
                                    {selectedProductionPlant && 
                                    <>
                                    <ProductionPlant compName={selectedProductionPlant && ProdPlant.find(e=>e['id']===selectedProductionPlant).type}/>
                                    <div style={{display: "flex", alignItems: "center"}}>
                                        <div class="col-md-6" style={{position: "relative", right: "15px", bottom: "10px"}}>
                                            <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                <div class="col-sm-10">
                                                <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                </div>
                                        </div>
                                        <div>
                                                { editInput1Files && editInput1Files.map((e)=>(
                                                    <div className="icon-container">
                                                        <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                        <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                        </a>
                                                        <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                    </div>
                                                ))
                                                }   
                                        </div>
                                    </div>
                                    </>
                                    }
                                </div>
                                    
                                
                                
                                <div style={{ textAlign: "center" }}>
                                    <button type="submit" class="btn submitbtn">
                                    {prodPlantFlagEdit === true ? "Update":"Submit"}
                                    </button>
                                    <button type="reset" class="btn btn-secondary">
                                    {" "}
                                    Reset
                                    </button>
                                </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* Support New Technology */}
                        <TabPanel value="19">
                            <div className='tab-pane'>
                                <form class="row g-3" onSubmit={handleSupportTechSubmit}>
                                    <div className='quarterSelect'>
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                    <FormControl fullWidth>
                                                        <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                        <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                            label="Quarter"
                                                            onChange={handleQuarterChange}
                                                        >
                                                        {(quarters).map((e,key)=>(
                                                            <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                        ))}
                                                        </Select>
                                                    </FormControl>
                                                </Box>
                                            </div>
                                    </div>
                                        <div className='word-with-line'>
                                            <span>Beneficiary Information</span>
                                            <i class="fa-solid fa-angle-down" style={{color: "#4e2683"}}></i>
                                        </div>
                                        <div className='col-md-4'>
                                            <Box sx={{ minWidth: "100%" }}>
                                                <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                    <InputLabel htmlFor="demo-simple-select-label">FPO/VCO</InputLabel>
                                                    <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={selectbeniFiApplicant}
                                                    label="FPO/VCO"
                                                    onChange={handleApplicantChangeESIFPO}
                                            
                                                    >
                                                    {(beniFiApplicant).map((e)=>(
                                                        <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                    ))}
                                                    </Select>
                                                    <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                                </FormControl>
                                            </Box>
                                        </div>
                                        {/* <SelectField selectData={farmerCompanies}/> */}
                                        <InputField label="Division" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.divisions ? nurseryTissueBeneficiary.divisions["divisions"].toString(): ''}/>
                                        <InputField label="District" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.district ? nurseryTissueBeneficiary.district["district"].toString(): ''}/>
                                        <InputField label="Taluka" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.taluka ? nurseryTissueBeneficiary.taluka["taluka"].toString(): ''}/>
                                        <InputField label="Beneficiary Type" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.beneficiary_type ? nurseryTissueBeneficiary.beneficiary_type["beneficiary_type"].toString(): ''}/>
                                    <br/>
                                        <div className='word-with-line' style={{margin: "1.5rem 0"}}>
                                                <span>Sanctioned cost break-up {selectSupportComponent && `for ${component.find(e=>e['id']===selectSupportComponent).technical_component}`}</span>
                                                <div>
                                            <Box sx={{ minWidth: 390 }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel id="demo-simple-select-label">Component</InputLabel>
                                                    <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    value={selectSupportComponent} onChange={handleTechComponentChangeChange}
                                                    label="Comp"
                                                    >
                                                    {component.map((e) => (
                                                        <MenuItem key={e.id} value={e.id}>{e["technical_component"]}</MenuItem>
                                                    ))}
                                                    
                                                    </Select>
                                                </FormControl>
                                            </Box>
                                        </div>
                                        </div>
                                    <br/>

                                        {selectSupportComponent && 
                                            <div>
                                                <SupportTechnology compName={selectSupportComponent && component.find(e=>e['id']===selectSupportComponent).technical_component} tranche={nurseryTissueBeneficiary && nurseryTissueBeneficiary.payment_tranche ? nurseryTissueBeneficiary.payment_tranche: ''} 
                                                matchingGrant={isMatchingGrant} />
                                                <div style={{    display: "flex", alignItems: "center"}}>
                                                    <div class="col-md-6" style={{position: "relative", right: "15px", bottom: "10px"}}>
                                                        <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                            <div class="col-sm-10">
                                                            <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                            <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                            </div>
                                                    </div>
                                                    <div>
                                                            { editInput1Files && editInput1Files.map((e)=>(
                                                                <div className="icon-container">
                                                                    <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                                    <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                                    </a>
                                                                    <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                                </div>
                                                            ))
                                                            }   
                                                    </div>
                                                </div>
                                            </div>}
                                            
                                    
                                        <div style={{textAlign:"center"}}>
                                            <button type="submit" class="btn submitbtn">{nurseryFlagEdit === true ? "Update":"Submit"}</button>
                                            <button type="reset" class="btn btn-secondary">Reset</button>
                                        </div>
                                    {/* Conditionally render the BasicModal component */}
                                </form>
                            </div>
                        </TabPanel>
                        
                        {/* BioWaste */}
                        <TabPanel value='20'>
                            <div className="tab-pane">
                                <form class="row g-3" onSubmit={handleBioWasteSubmit}>
                                    <div className="quarterSelect">
                                            <div className='support'>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                <FormControl fullWidth>
                                                    <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                    <Select
                                                            labelId="demo-simple-select-label"
                                                            id="demo-simple-select"
                                                            value={selectQuarter}
                                                        label="Quarter"
                                                        onChange={handleQuarterChange}
                                                    >
                                                    {(quarters).map((e,key)=>(
                                                        <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                    ))}
                                                    </Select>
                                                </FormControl>
                                                </Box>
                                            </div>
                                        </div>
                                        <div className="word-with-line">
                                        <span>Beneficiary Information</span>
                                        <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
                                    </div>
                                    <div className='col-md-4'>
                                        <Box sx={{ minWidth: "100%" }}>
                                            <FormControl fullWidth error={errorSelectBeniApplicant !== ""?true:false}>
                                                <InputLabel htmlFor="demo-simple-select-label">FPO/VCO/CMRC</InputLabel>
                                                <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectbeniFiApplicant}
                                                label="FPO/VCO/CMRC"
                                                onChange={handleApplicantChangeESIFPO}
                                        
                                                >
                                                {(beniFiApplicant).map((e)=>(
                                                    <MenuItem value={e.id} key={e.id}>{e["beneficiary_name"]}</MenuItem>
                                                ))}
                                                </Select>
                                                <FormHelperText>{errorSelectBeniApplicant}</FormHelperText>
                                            </FormControl>
                                        </Box>
                                    </div>
                                    <InputField label="Division" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.divisions ? nurseryTissueBeneficiary.divisions["divisions"].toString(): ''}/>
                                    <InputField label="District" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.district ? nurseryTissueBeneficiary.district["district"].toString(): ''}/>
                                    <InputField label="Taluka" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.taluka ? nurseryTissueBeneficiary.taluka["taluka"].toString(): ''}/>
                                    <InputField label="Beneficiary Type" type="text" readOnly={true} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.beneficiary_type ? nurseryTissueBeneficiary.beneficiary_type["beneficiary_type"].toString(): ''}/>
                                    <div className='col-md-4' style={{paddingRight:0}}>
                                            <MultiSelect label="MAGNET Crops" data={magentCrop} value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.magnet_crop_id ? nurseryTissueBeneficiary.magnet_crop_id :[] } readOnly={true}/>
                                        </div>
                                    <InputField label="Sub Project Title" type="text" value={nurseryTissueBeneficiary && nurseryTissueBeneficiary.sub_project_title ? nurseryTissueBeneficiary.sub_project_title :''} readOnly={true} />
                                    <DatePick label="MOUSigningdate" className="custom-date-picker" disbrushDate={nurseryTissueBeneficiary && nurseryTissueBeneficiary.signing_date ? nurseryTissueBeneficiary.signing_date.toString(): ''} readOnly={true}/>
                                    
                                   
                                    <BioWaste/>
                                    <div style={{    display: "flex", alignItems: "center"}}>
                                        <div class="col-md-6" style={{position: "relative", right: "15px", bottom: "10px"}}>
                                            <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                <div class="col-sm-10">
                                                <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                </div>
                                        </div>
                                        <div>
                                                {editInput1Files && editInput1Files.map((e)=>(
                                                    <div className="icon-container">
                                                        <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                        <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                        </a>
                                                        <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                    </div>
                                                ))
                                                }   
                                        </div>
                                    </div>
                                    
                                    <div style={{ textAlign: "center" }}>
                                        <button type="submit" class="btn submitbtn">
                                        {researchDevFlagEdit === true ? "Update":"Submit"}
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                        Reset
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* Trade Fair & Exhibition */}
                        <TabPanel value='21'>
                            <div className="tab-pane">
                                <form class="row g-3" onSubmit={handleTradeFairSubmit}>
                                    <div className="quarterSelect">
                                        <div className='support'>
                                        </div>
                                        <div className='col-md-4' style={{paddingRight:0}}>
                                                    <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                    <FormControl fullWidth>
                                                        <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                        <Select
                                                                labelId="demo-simple-select-label"
                                                                id="demo-simple-select"
                                                                value={selectQuarter}
                                                            label="Quarter"
                                                            onChange={handleQuarterChange}
                                                        >
                                                        {(quarters).map((e,key)=>(
                                                            <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                        ))}
                                                        </Select>
                                                    </FormControl>
                                                    </Box>
                                        </div>
                                    </div>
                                    
                                    <div className="word-with-line" style={{ margin: "1.5rem 0" }}>
                                        <span>
                                            Details of trainings for Current Quarter of - {selectedTradeFareExhitbiton && TradeFairExhibition.find(e=>e['id']===selectedTradeFareExhitbiton).type}
                                        </span>
                                        <div>
                                        <Box
                                            sx={{ minWidth: 390 }}
                                            style={{ backgroundColor: "#ededed" }}
                                        >
                                            <FormControl fullWidth>
                                            <InputLabel id="demo-simple-select-label">
                                                Training Type
                                            </InputLabel>
                                            <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectedTradeFareExhitbiton}
                                                onChange={handleOptionTradeFare}
                                                label="Trainin"
                                            >
                                                {TradeFairExhibition.map((e) => (
                                                <MenuItem value={e.id}>
                                                    {e.type}
                                                </MenuItem>
                                                ))}
                                            </Select>
                                            </FormControl>
                                        </Box>
                                        </div>
                                    </div>
                                    <br />
                                    <div>
                                        {selectedTradeFareExhitbiton && 
                                        <>
                                        <div class="row g-3" style={{marginBottom:"10px"}}>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }}>
                                                    <FormControl fullWidth >
                                                        <InputLabel id="demo-simple-select-label">
                                                        Division
                                                        </InputLabel>
                                                        <Select
                                                        labelId="demo-simple-select-label"
                                                        id="demo-simple-select"
                                                        value={selectKPMGDivision}
                                                        onChange={handleDivisionKMPGChange}
                                                        label="Division"
                                                        >
                                                        {division.map((e) => (
                                                            <MenuItem value={e.id}>{e.divisions}</MenuItem>
                                                        ))}
                                                        </Select>
                                                        {/* <FormHelperText>{studyTourExpo.validationErrors.division_id}</FormHelperText> */}
                                                    </FormControl>
                                                </Box>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <MultiSelect label="Beneficiary" data={studyFarmApplicant} value={beneStudyTourFarm} onChange={handleBenefiStudyTourFarm}/>
                                            </div>
                                        </div>
                                        <TradeFareExhibition compValue={selectedTradeFareExhitbiton}/>
                                        <div style={{display: "flex", alignItems: "center"}}>
                                            <div class="col-md-6" style={{position: "relative", right: "25px"}}>
                                                <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                    <div class="col-sm-10">
                                                    <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                    <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                    </div>
                                            </div>
                                            <div>
                                                    {editInput1Files && editInput1Files.map((e)=>(
                                                        <div className="icon-container">
                                                            <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                            <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                            </a>
                                                            <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                        </div>
                                                    ))
                                                    }   
                                            </div>
                                        </div>
                                        </>
                                        }
                                    </div>

                                    <div style={{ textAlign: "center" }}>
                                        <button type="submit" class="btn submitbtn">
                                        {studyTourFlagEdit === true ? "Update":"Submit"}
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                        Reset
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>

                        {/* Market Development and Promotion */}
                        <TabPanel value='22'>
                            <div className="tab-pane">
                                <form class="row g-3" onSubmit={handleMarketPromSubmit}>
                                    <div className="quarterSelect">
                                        <div className='support'>
                                        </div>
                                        <div className='col-md-4' style={{paddingRight:0}}>
                                                    <Box sx={{ minWidth: "100%" }} style={{backgroundColor:"#ededed"}}>
                                                    <FormControl fullWidth>
                                                        <InputLabel htmlFor="demo-simple-select-label">Quarter</InputLabel>
                                                        <Select
                                                                labelId="demo-simple-select-label"
                                                                id="demo-simple-select"
                                                                value={selectQuarter}
                                                            label="Quarter"
                                                            onChange={handleQuarterChange}
                                                        >
                                                        {(quarters).map((e,key)=>(
                                                            <MenuItem value={e.id} key={key}>{e.value}</MenuItem>
                                                        ))}
                                                        </Select>
                                                    </FormControl>
                                                    </Box>
                                        </div>
                                    </div>
                                    
                                    <div className="word-with-line" style={{ margin: "1.5rem 0" }}>
                                        <span>
                                            Details of trainings for Current Quarter of - {selectedMarketDevProm && MarketDevProm.find(e=>e['id']===selectedMarketDevProm).type}
                                        </span>
                                        <div>
                                        <Box
                                            sx={{ minWidth: 390 }}
                                            style={{ backgroundColor: "#ededed" }}
                                        >
                                            <FormControl fullWidth>
                                            <InputLabel id="demo-simple-select-label">
                                                Training Type
                                            </InputLabel>
                                            <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                value={selectedMarketDevProm}
                                                onChange={handleOptionMarketPromotion}
                                                label="Trainin"
                                            >
                                                {MarketDevProm.map((e) => (
                                                <MenuItem value={e.id}>
                                                    {e.type}
                                                </MenuItem>
                                                ))}
                                            </Select>
                                            </FormControl>
                                        </Box>
                                        </div>
                                    </div>
                                    <br />
                                    <div>
                                        {selectedMarketDevProm && 
                                        <>
                                        <div class="row g-3" style={{marginBottom:"10px"}}>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <Box sx={{ minWidth: "100%" }}>
                                                    <FormControl fullWidth >
                                                        <InputLabel id="demo-simple-select-label">
                                                        Division
                                                        </InputLabel>
                                                        <Select
                                                        labelId="demo-simple-select-label"
                                                        id="demo-simple-select"
                                                        value={selectKPMGDivision}
                                                        onChange={handleDivisionKMPGChange}
                                                        label="Division"
                                                        >
                                                        {division.map((e) => (
                                                            <MenuItem value={e.id}>{e.divisions}</MenuItem>
                                                        ))}
                                                        </Select>
                                                        {/* <FormHelperText>{studyTourExpo.validationErrors.division_id}</FormHelperText> */}
                                                    </FormControl>
                                                </Box>
                                            </div>
                                            <div className='col-md-4' style={{paddingRight:0}}>
                                                <MultiSelect label="Beneficiary" data={studyFarmApplicant} value={beneStudyTourFarm} onChange={handleBenefiStudyTourFarm}/>
                                            </div>
                                        </div>
                                        <MarketPromotion compValue={selectedMarketDevProm}/>
                                        <div style={{display: "flex", alignItems: "center"}}>
                                            <div class="col-md-6" style={{position: "relative", right: "25px"}}>
                                                <label for="inputNumber" class="col-lg col-form-label">Upload supporting document. (e.g QPR, Photos etc.)</label>
                                                    <div class="col-sm-10">
                                                    <input class="form-control" type="file" id="formFile" name="remarks" multiple onChange={handleMultiFilesGAPChange} onFocus={()=>{setErrorCapacityFile("")}}/>
                                                    <span style={{color: 'red'}}>{errorCapacityFile}</span>
                                                    </div>
                                            </div>
                                            <div>
                                                    {editInput1Files && editInput1Files.map((e)=>(
                                                        <div className="icon-container">
                                                            <a href={`${fileUrl}${e.file_name}`} rel="noreferrer" target="_blank" style={{marginRight:"10px"}}>
                                                            <img src={(e.type==="pdf")?pdf:imgIcon} alt="" height= "30px" width= "26px" />
                                                            </a>
                                                            <i class="fa-regular fa-circle-xmark" style={{cursor:"pointer"}} onClick={()=>handleDeleteFile(e.id)}></i>
                                                        </div>
                                                    ))
                                                    }   
                                            </div>
                                        </div>
                                        </>
                                        }
                                    </div>

                                    <div style={{ textAlign: "center" }}>
                                        <button type="submit" class="btn submitbtn">
                                        {studyTourFlagEdit === true ? "Update":"Submit"}
                                        </button>
                                        <button type="reset" class="btn btn-secondary">
                                        Reset
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </TabPanel>

                    </TabContext>
                </Box>
                </div>
            </div>

            </div>
        </section>
        </main>
        </Online>
        <Offline>
            You're Offline, Please Check your Connection
        </Offline>
    </>)
}