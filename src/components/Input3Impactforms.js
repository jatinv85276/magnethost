import "../assets/css/inputForm.css";
// import { SelectField } from "../subcomponents/SelectField"
import React, { useState, useEffect } from "react";
import axios from "axios";
import pdf from "../assets/PDF_file_icon.svg.png";
import imgIcon from "../assets/image_icon-icons.com_50366.png";
import { getConfigWithToken } from "../utils/Config/Config";
import { useDispatch, useSelector } from "react-redux";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import TabContext from "@mui/lab/TabContext";
import TabPanel from "@mui/lab/TabPanel";
import { InputField } from "../subcomponents/InputField";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import { MultiSelect } from "../subcomponents/MultiSelect";
import Swal from "sweetalert2";
import {
  setInputFieldData,
} from "../redux/slice/InputTwoImpactForms/BeneficiaryProfit/BeneficiaryProfitSlice";
import { UtilizationTPDMSAMB } from "../subcomponents/Input3impactforms/Utilization2TPD/UtilizationTPDMSAMB";
import { UtilizationColdStorageMSAMB } from "../subcomponents/Input3impactforms/Utilization3ColdStorgae/UtilizationColdStorageMSAMB";
import {
  resetDataTPDThree,
  setValidationErrorsTPDThree,
  updateApiDataToTPDThree,
} from "../redux/slice/InputThreeImpactForms/UtilizationThreeTPD/UtilizationThreeTPDSlice";
import {
  resetDataColdStorageThree,
  setValidationErrorsColdStorageThree,
  updateApiDataToColdStorageThree,
} from "../redux/slice/InputThreeImpactForms/UtilizationThreeColdStorage/UtilizationThreeColdStorageSlice";

export const InputForm3ImapctForms = ({
  quarter,
  years,
  divisi,
  magentCrp,
  exisitingmagnetCrp,
  currentFinancialYear,
}) => {
  const apiUrl = process.env.REACT_APP_API_URL;
  const fileUrl = process.env.REACT_APP_INPUT1_API_FILE_URL;
  const dd = useSelector(setInputFieldData);

  const dispatch = useDispatch();
  const [value, setValue] = useState("0");
  const [Year, setYear] = useState([]);
  const [selectYear, setSelectedYear] = useState("");
  const [loading, setLoading] = useState(false);

  const [quarters, setQuarter] = useState([]);
  const [selectQuarter, setSelectedQuarters] = useState("");
  const [selectKPMGDivision, setSelectKPMGDivision] = useState("");
  const [division, setDivision] = useState([]);
  const [studyFarmApplicant, setStudyFarmApplicant] = useState([]);
  const [beneStudyTourFarm, setBeneStudyTourFarm] = useState([]);
  const [magentCrop, setMagentCrop] = useState([]);
  const [existingmagentCrop, setExisitingMagentCrop] = useState([]);

  const [esiTargetCrop, setEsiTargetCrop] = useState("");
  const [esiPackHouse, setEsiPackHouse] = useState("");

  const [postHarvestFlagEdit, setPostHarvestFlagEdit] = useState(false);
  const [postHarvestIdEdit, setPostHarvestIdEdit] = useState("");

  const [validationErrors, setValidationErrors] = useState({});

  const [permission, setPermission] = useState([]);
  const [editInput1Files, setEditInput1Files] = useState([]);
  const [selectCapacityFile, setSelectCapacityFile] = useState([]);
  const [errorCapacityFile, setErrorCapacityFile] = useState("");

  const resetAllState = () => {
    setBeneStudyTourFarm([]);
    setSelectKPMGDivision("");
    setEsiTargetCrop("");
    setEsiPackHouse("");
    setStudyFarmApplicant([]);
    dispatch(resetDataTPDThree({}));
    dispatch(resetDataColdStorageThree({}));
    setPostHarvestFlagEdit("");
    setEditInput1Files([]);
    setSelectCapacityFile([]);
    setErrorCapacityFile("");
  };

  const handleChange = async (event, newValue) => {
    setValue(newValue);
    resetAllState();
  };

  const handleFinYearChange = (event) => {
    refershdata();
    setSelectedYear(event.target.value);
  };

  const refershdata = () => {
    setBeneStudyTourFarm([]);
    setSelectKPMGDivision("");
    setStudyFarmApplicant([]);
    setEsiTargetCrop("");
    setEsiPackHouse("");

    dispatch(resetDataColdStorageThree({}));
    dispatch(resetDataTPDThree({}));

    setEditInput1Files([]);
    setSelectCapacityFile([]);
    setErrorCapacityFile("");
  };

  const handleQuarterChange = async (event) => {
    refershdata();
    setSelectedQuarters(event.target.value);
  };

  const handleDivisionKMPGChange = async (event) => {
    resetAllState();
    const selectedDivision = event.target.value;
    setSelectKPMGDivision(selectedDivision);
    setStudyFarmApplicant([]);
    setBeneStudyTourFarm([]);
    try {
      const beneficiaryByFormDivision = await axios.get(
        `${apiUrl}api/getMsambFacilityByDivision?division_id=${selectedDivision}`,
        getConfigWithToken()
      );
      const studyFarm = beneficiaryByFormDivision.data.data[0].map((e) => ({
        id: e.msamb_facility_id,
        name: e.msamb_facility_name,
      }));
      setStudyFarmApplicant(studyFarm);
    } catch (error) {
      Swal.fire(`${error.message}`, "", "warning");
    }
  };

  const handleFPOVCOUtilizationChange = async (event) => {
    const selectedApplicant = event.target.value;
    setBeneStudyTourFarm(selectedApplicant);
    try {
      const DataByApplicantSelection = await axios.get(
        `${apiUrl}api/getMagnetCropByMsambFacility?msamb_facility_id=${selectedApplicant}`,
        getConfigWithToken()
      );

      const magnetCropData = DataByApplicantSelection.data.data[0];
      const magnetCropIds = magnetCropData.magnet_crop_id
        ? JSON.parse(magnetCropData.magnet_crop_id).map((crop) => crop.id)
        : [];

      const totalCapacityTpd =
        magnetCropData.total_capacity_tpd !== null
          ? magnetCropData.total_capacity_tpd
          : 0;

      setEsiPackHouse(totalCapacityTpd);
      setEsiTargetCrop(magnetCropIds);
      if (value === "0") {
        const getStudyTourEditData = await axios.get(
          `${apiUrl}api/if-utilisation-facility-tpd-input-msamb-three-edit?quarter=${selectQuarter}&msamb_facility_id=${selectedApplicant}&division_id=${selectKPMGDivision}`,
          getConfigWithToken()
        );
        if (getStudyTourEditData.data["status"] === true) {
          getStudyTourEditData.data["flag"]
            ? setPostHarvestFlagEdit(true)
            : setPostHarvestFlagEdit(false);
          const data = getStudyTourEditData.data["data"][0];
          data["magnet_crop_id"] = data["magnet_crop_id"].split(",");
          data["existing_crops"] = data["existing_crops"].split(",");

          console.log(data["magnet_crop_id"], ' data["magnet_crop_id"]');
          console.log(data["existing_crops"], ' data["existing_crops"]');
          if (getStudyTourEditData.data["remarks"] !== null) {
            setEditInput1Files(getStudyTourEditData.data["remarks"]);
          }

          setPostHarvestIdEdit(data["id"]);
          dispatch(updateApiDataToTPDThree(data));
        } else {
          setEditInput1Files([]);
          setPostHarvestFlagEdit(false);
          dispatch(resetDataTPDThree());
        }
      }
      if (value === "1") {
        const getStudyTourEditData = await axios.get(
          `${apiUrl}api/if-utilisation-facility-cs-msamb-input-msamb-three-edit?quarter=${selectQuarter}&msamb_facility_id=${selectedApplicant}&division_id=${selectKPMGDivision}`,
          getConfigWithToken()
        );
        if (getStudyTourEditData.data["status"] === true) {
          getStudyTourEditData.data["flag"]
            ? setPostHarvestFlagEdit(true)
            : setPostHarvestFlagEdit(false);
          const data = getStudyTourEditData.data["data"][0];
          data["magnet_crop_id"] = data["magnet_crop_id"].split(",");

           // Check if remarks is not null and then set the state
          if (getStudyTourEditData.data["remarks"] !== null) {
            setEditInput1Files(getStudyTourEditData.data["remarks"]);
          }
       
          setPostHarvestIdEdit(data["id"]);
          dispatch(updateApiDataToColdStorageThree(data));
        } else {
          setEditInput1Files([]);
          setPostHarvestFlagEdit(false);
          dispatch(resetDataColdStorageThree());
        }
      }
    } catch (err) {
      Swal.fire(`${err.message}`, "", "warning");
    }
  };

  const handleMultiFilesGAPChange = (event) => {
    setSelectCapacityFile(event.target.files);
  };

  const handleDeleteFile = async (id) => {
    const deleteFile = await axios.get(
      `${apiUrl}api/delete-impact-form-remarks?id=${id}`,
      getConfigWithToken()
    );
    if (deleteFile.data["status"]) {
      const newRemarks = editInput1Files.filter((remark) => remark.id !== id);
      setEditInput1Files(newRemarks);
      Swal.fire(`File deleted Successfully`, "", "success");
    }
  };

  const handleSubmitUtilizationTPD = async (e) => {
    e.preventDefault();
    const formData = dd["payload"]["utilizationTPDThree"];
    const validateForm = () => {
      const errors = {};
      const errorMessages = {
        existing_crops: "This Field is required",
        is_facility_in_opration: "This Field is required",
        total_quantity_handled_tpd: "This Field is required",
        total_capacity_utilised_tpd: "This Field is required",
        capacity_utilised_job_work: "This Field is required",
        capacity_utilised_captive_used: "This Field is required",
        total_amount: "This Field is required",
        service_rental_charges: "This Field is required",
        processing_charges: "This Field is required",
      };

      for (const field in errorMessages) {
        const value = formData[field];
        if (value === "" || (Array.isArray(value) && value.length === 0)) {
          errors[field] = errorMessages[field];
        }
      }
      dispatch(setValidationErrorsTPDThree(errors)); // Dispatch errors
      console.log(errors, "errors");
      return Object.keys(errors).length === 0;
    };

    if (validateForm()) {
      try {
        setLoading(true);
        const postHarvest = dd["payload"]["utilizationTPDThree"];

        const formData = new FormData();
        for (const key in postHarvest) {
          if (key === "existing_crops") {
            formData.append(
              "existing_crops",
              postHarvest["existing_crops"].join(",")
            );
          } else {
            formData.append(
              key,
              postHarvest[key] !== null ? postHarvest[key] : ""
            );
          }
        }
        formData.append("quarter", selectQuarter);
        formData.append("division_id", selectKPMGDivision);
        formData.append("msamb_facility_id", beneStudyTourFarm);
        formData.append("magnet_crop_id", esiTargetCrop);
        formData.append(
          "total_capacity_tpd",
          esiPackHouse !== null ? esiPackHouse : 0
        );
      
        let size = 0;
        const maxSize = 5 * 1024 * 1024;
        if (selectCapacityFile.length !== 0) {
          for (let i = 0; i < selectCapacityFile.length; i++) {
            size += selectCapacityFile[i].size;
            formData.append("remarks[]", selectCapacityFile[i]);
          }
        }
        if (size >= maxSize) {
          setErrorCapacityFile("File size should be less than 5 MB");
          setLoading(false);
          return;
        }

        try {
          if (!postHarvestFlagEdit) {
            const submitPostHarvestData = await axios.post(
              `${apiUrl}api/if-utilisation-facility-tpd-input-msamb-three-create`,
              formData,
              getConfigWithToken()
            );
            if (submitPostHarvestData.data["status"] === true) {
              Swal.fire(
                `${submitPostHarvestData.data["message"]}`,
                "",
                "success"
              ).then(() => {
                dispatch(resetDataTPDThree());
                setEditInput1Files([]);
                setLoading(false);
              });
            } else {
              Swal.fire(
                `${submitPostHarvestData.data["message"]}`,
                "",
                "warning"
              ).then(() => {
                setLoading(false);
              });
            }
          } else {
            // also have to set Id in object
            formData.append("id", postHarvestIdEdit);
            const submitPostHarvestData = await axios.post(
              `${apiUrl}api/if-utilisation-facility-tpd-input-msamb-three-update`,
              formData,
              getConfigWithToken()
            );

            if (submitPostHarvestData.data["status"] === true) {
              Swal.fire(
                `${submitPostHarvestData.data["message"]}`,
                "",
                "success"
              ).then(() => {
                dispatch(resetDataTPDThree());
                setEditInput1Files([]);
                setLoading(false); // Reset loading state after Swal confirmation
              });
            } else {
              Swal.fire(
                `${submitPostHarvestData.data["message"]}`,
                "",
                "warning"
              ).then(() => {
                setLoading(false); // Reset loading state after Swal confirmation
              });
            }
          }
        } catch (error) {
          Swal.fire(`${error.message}`, "", "warning");
        }
      } catch (error) {
        Swal.fire(`${error.message}`, "", "warning").then(() => {
          setLoading(false); // Reset loading state after Swal confirmation
        });
      }
     
    }
  };

  const handleSubmitUtilizationColdStorage = async (e) => {
    e.preventDefault();
    const formData = dd["payload"]["utilizationColdStorageThree"];
    const validateForm = () => {
      const errors = {};
      const errorMessages = {
        existing_crops: "This Field is required",
        is_facility_in_opration: "This Field is required",
        total_quantity_handled: "This Field is required",
      };

      for (const field in errorMessages) {
        const value = formData[field];
        if (value === "" || (Array.isArray(value) && value.length === 0)) {
          errors[field] = errorMessages[field];
        }
      }
      dispatch(setValidationErrorsColdStorageThree(errors)); // Dispatch errors
      console.log(errors, "errors");
      return Object.keys(errors).length === 0;
    };

    if (validateForm()) {
      try {
        setLoading(true);
        const postHarvest = dd["payload"]["utilizationColdStorageThree"];

        const formData = new FormData();
        for (const key in postHarvest) {
          if (key === "existing_crops") {
            formData.append(
              "existing_crops",
              postHarvest["existing_crops"].join(",")
            );
          } else {
            formData.append(
              key,
              postHarvest[key] !== null ? postHarvest[key] : ""
            );
          }
        }
        formData.append("quarter", selectQuarter);
        formData.append("division_id", selectKPMGDivision);
        formData.append("msamb_facility_id", beneStudyTourFarm);
        formData.append("magnet_crop_id", esiTargetCrop);
        
        let size = 0;
        const maxSize = 5 * 1024 * 1024;
        if (selectCapacityFile.length !== 0) {
          for (let i = 0; i < selectCapacityFile.length; i++) {
            size += selectCapacityFile[i].size;
            formData.append("remarks[]", selectCapacityFile[i]);
          }
        }
        if (size >= maxSize) {
          setErrorCapacityFile("File size should be less than 5 MB");
          setLoading(false);
          return;
        }

        try {
          if (!postHarvestFlagEdit) {
            const submitPostHarvestData = await axios.post(
              `${apiUrl}api/if-utilisation-facility-cs-msamb-input-three-create`,
              formData,
              getConfigWithToken()
            );
            if (submitPostHarvestData.data["status"] === true) {
              Swal.fire(
                `${submitPostHarvestData.data["message"]}`,
                "",
                "success"
              ).then(() => {
                dispatch(resetDataColdStorageThree());
                setEditInput1Files([]);
                setLoading(false);
              });
            } else {
              Swal.fire(
                `${submitPostHarvestData.data["message"]}`,
                "",
                "warning"
              ).then(() => {
                setLoading(false);
              });
            }
          } else {
            // also have to set Id in object
            formData.append("id", postHarvestIdEdit);
            const submitPostHarvestData = await axios.post(
              `${apiUrl}api/if-utilisation-facility-cs-msamb-input-three-update`,
              formData,
              getConfigWithToken()
            );

            if (submitPostHarvestData.data["status"] === true) {
              Swal.fire(
                `${submitPostHarvestData.data["message"]}`,
                "",
                "success"
              ).then(() => {
                dispatch(resetDataColdStorageThree());
                setEditInput1Files([]);
                setLoading(false);
              });
            } else {
              Swal.fire(
                `${submitPostHarvestData.data["message"]}`,
                "",
                "warning"
              ).then(() => {
                setLoading(false);
              });
            }
          }
        } catch (error) {
          Swal.fire(`${error.message}`, "", "warning");
        }
      } catch (error) {
        Swal.fire(`${error.message}`, "", "warning").then(() => {
          setLoading(false);
        });
      }
    }
  };

  const handleDeleteTPD = async () => {
    const result = await Swal.fire({
      title: 'Are you sure?',
      text: 'Do you want to delete this record?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'Cancel',
    });
  
    if (result.isConfirmed) {
    try {
      const response = await axios.get(
        `${apiUrl}api/if-utilisation-facility-tpd-input-msamb-three-delete?id=${postHarvestIdEdit}`,
        getConfigWithToken()
      );

      if (response.data.status === true) {
        Swal.fire(`${response.data.message}`, "", "success").then(() => {
          refershdata();
          setPostHarvestFlagEdit("");
          dispatch(resetDataTPDThree());
          setLoading(false);
        });
      } else {
        Swal.fire(`${response.data.message}`, "", "warning").then(() => {
          setLoading(false);
        });
      }
    } catch (error) {
      Swal.fire(`${error.message}`, "", "warning");
    }
  };
};

  const handleDeleteColdStorage = async () => {
    const result = await Swal.fire({
      title: 'Are you sure?',
      text: 'Do you want to delete this record?',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Yes, delete it!',
      cancelButtonText: 'Cancel',
    });
  
    if (result.isConfirmed) {
    try {
      const response = await axios.get(
        `${apiUrl}api/if-utilisation-facility-cs-msamb-input-msamb-three-delete?id=${postHarvestIdEdit}`,
        getConfigWithToken()
      );

      if (response.data.status === true) {
        Swal.fire(`${response.data.message}`, "", "success").then(() => {
          refershdata();
          setPostHarvestFlagEdit("");
          dispatch(resetDataColdStorageThree());
          setLoading(false);
        });
      } else {
        Swal.fire(`${response.data.message}`, "", "warning").then(() => {
          setLoading(false);
        });
      }
    } catch (error) {
      Swal.fire(`${error.message}`, "", "warning");
    }
  };
  };
  
  useEffect(() => {
    const quarterData = async () => {
      try {
        const storedArrayAsString = localStorage.getItem("permission");
        setPermission(storedArrayAsString);

        setYear(years);
        setQuarter(quarter);
        setDivision(divisi);
        setMagentCrop(magentCrp);
        setExisitingMagentCrop(exisitingmagnetCrp);
        const defaultQuarter = quarter.find((q) => q.selected === "Selected");
        if (defaultQuarter) {
          setSelectedQuarters(defaultQuarter.id);
        }

        const defaultYear = years.find((y) => y.value === currentFinancialYear);
        if (defaultYear) {
          setSelectedYear(defaultYear.id);
        }
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    };
    console.log("Current Financial Year:", currentFinancialYear);
    quarterData();
  }, [
    quarter,
    magentCrp,
    years,
    divisi,
    exisitingmagnetCrp,
    currentFinancialYear,
  ]);

  return (
    <>
      {/* <Online> */}

      <main id="main" class="main">
        <section class="section">
          <div class="row">
            <div id="exTab3" class="contain">
              <div class="card">
                <Box
                  sx={{
                    width: "100%",
                    typography: "body1",
                    bgcolor: "background.paper",
                  }}
                >
                  <TabContext value={value}>
                    <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                      <Tabs
                        variant="scrollable"
                        scrollButtons="auto"
                        aria-label="scrollable auto tabs example"
                        value={value}
                        onChange={handleChange}
                      >
                       
                        <Tab
                          label="Utilisation of Facility-TPD"
                          value="0"
                          className={`${value === "0" ? "activeClickCSS" : ""}`}
                        />
                        <Tab
                          label="Utilisation of Facility-Cold Storage"
                          value="1"
                          className={`${value === "1" ? "activeClickCSS" : ""}`}
                        />
                      </Tabs>
                    </Box>

                    {/* Utilization - TPD */}
                    <TabPanel value="0">
                      <div className="tab-pane">
                        <form
                          class="row g-3"
                          onSubmit={handleSubmitUtilizationTPD}
                        >
                          <div className="quarterSelect">
                            <div className="support"></div>
                            <div
                              className="col-md-4"
                              style={{ paddingRight: 0 }}
                            >
                              <Box
                                sx={{ minWidth: "100%" }}
                                style={{ backgroundColor: "#ededed" }}
                              >
                                <FormControl fullWidth>
                                  <InputLabel htmlFor="demo-simple-select-label">
                                    Quarter
                                  </InputLabel>
                                  <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={selectQuarter}
                                    label="Quarter"
                                    onChange={handleQuarterChange}
                                  >
                                    {quarters.map((e, key) => (
                                      <MenuItem value={e.id} key={key}>
                                        {e.value}
                                      </MenuItem>
                                    ))}
                                  </Select>
                                </FormControl>
                              </Box>
                            </div>
                          </div>
                          <div className="word-with-line">
                            <span>Beneficiary Information</span>
                            <i
                              class="fa-solid fa-angle-down"
                              style={{ color: "#4e2683" }}
                            ></i>
                          </div>

                          <div className="col-md-4" style={{ paddingRight: 0 }}>
                            <Box sx={{ minWidth: "100%" }}>
                              <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label">
                                  Division
                                </InputLabel>
                                <Select
                                  labelId="demo-simple-select-label"
                                  id="demo-simple-select"
                                  value={selectKPMGDivision}
                                  onChange={handleDivisionKMPGChange}
                                  label="Division"
                                >
                                  {division.map((e) => (
                                    <MenuItem value={e.id}>
                                      {e.divisions}
                                    </MenuItem>
                                  ))}
                                </Select>
                              </FormControl>
                            </Box>
                          </div>

                          <div className="col-md-4" style={{ paddingRight: 0 }}>
                            <Box sx={{ minWidth: "100%" }}>
                              <FormControl fullWidth>
                                <InputLabel id="beneficiary-select-label">
                                  Facility
                                </InputLabel>
                                <Select
                                  labelId="beneficiary-select-label"
                                  id="beneficiary-select"
                                  value={beneStudyTourFarm}
                                  onChange={handleFPOVCOUtilizationChange}
                                  label="Facility"
                                >
                                  {studyFarmApplicant.map((e) => (
                                    <MenuItem key={e.id} value={e.id}>
                                      {e.name}
                                    </MenuItem>
                                  ))}
                                </Select>
                              </FormControl>
                            </Box>
                          </div>
                          <div className="col-md-4" style={{ paddingRight: 0 }}>
                            <MultiSelect
                              label="Target Crops"
                              data={magentCrop}
                              value={esiTargetCrop || []}
                              readOnly={true}
                            />
                          </div>
                          <InputField
                            label="Total Capacity - TPD"
                            type="text"
                            readOnly={true}
                            value={esiPackHouse}
                          />
                          <br />
                          <div className="word-with-line">
                            <span>Details of current Quarter</span>
                            <i
                              class="fa-solid fa-angle-down"
                              style={{ color: "#4e2683" }}
                            ></i>
                          </div>
                          <UtilizationTPDMSAMB
                            existingmagentCrop={existingmagentCrop}
                          />

                          <div
                            style={{ display: "flex", alignItems: "center" }}
                          >
                            <div
                              class="col-md-6"
                              style={{
                                position: "relative",
                                right: "15px",
                                bottom: "10px",
                              }}
                            >
                              <label
                                for="inputNumber"
                                class="col-lg col-form-label"
                              >
                                Remarks
                              </label>
                              <div class="col-sm-10">
                                <input
                                  class="form-control"
                                  type="file"
                                  id="formFile"
                                  name="remarks"
                                  multiple
                                  onChange={handleMultiFilesGAPChange}
                                  onFocus={() => {
                                    setErrorCapacityFile("");
                                  }}
                                />
                                <span style={{ color: "red" }}>
                                  {errorCapacityFile}
                                </span>
                              </div>
                            </div>

                            <div>
                              {Array.isArray(editInput1Files) &&
                                editInput1Files.map((e) => (
                                  <div className="icon-container" key={e.id}>
                                    <a
                                      href={`${fileUrl}${e.file_name}`}
                                      rel="noreferrer"
                                      target="_blank"
                                      style={{ marginRight: "10px" }}
                                    >
                                      <img
                                        src={e.type === "pdf" ? pdf : imgIcon}
                                        alt=""
                                        height="30px"
                                        width="26px"
                                      />
                                    </a>
                                    <i
                                      className="fa-regular fa-circle-xmark"
                                      style={{ cursor: "pointer" }}
                                      onClick={() => handleDeleteFile(e.id)}
                                    ></i>
                                  </div>
                                ))}
                            </div>

                          </div>

                          <div style={{ textAlign: "center" }}>
                            {permission.includes(
                              "If-Utilisation-Facility-tpd-Input-Three-view"
                            ) ? (
                              <>
                                <button
                                  type="submit"
                                  class="btn submitbtn"
                                  disabled={loading}
                                >
                                  {postHarvestFlagEdit === true
                                    ? "Update"
                                    : "Submit"}
                                </button>
                                {postHarvestFlagEdit === true && (
                                  <button
                                    type="button"
                                    className="btn submitbtn"
                                    disabled={loading}
                                    onClick={handleDeleteTPD}
                                  >
                                    Delete
                                  </button>
                                )}
                              </>
                            ) : (
                              <></>
                            )}
                          </div>
                        </form>
                      </div>
                    </TabPanel>

                    {/* Utilization - Cold Storage */}
                    <TabPanel value="1">
                      <div className="tab-pane">
                        <form
                          class="row g-3"
                          onSubmit={handleSubmitUtilizationColdStorage}
                        >
                          <div className="quarterSelect">
                            <div className="support"></div>
                            <div
                              className="col-md-4"
                              style={{ paddingRight: 0 }}
                            >
                              <Box
                                sx={{ minWidth: "100%" }}
                                style={{ backgroundColor: "#ededed" }}
                              >
                                <FormControl fullWidth>
                                  <InputLabel htmlFor="demo-simple-select-label">
                                    Quarter
                                  </InputLabel>
                                  <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={selectQuarter}
                                    label="Quarter"
                                    onChange={handleQuarterChange}
                                  >
                                    {quarters.map((e, key) => (
                                      <MenuItem value={e.id} key={key}>
                                        {e.value}
                                      </MenuItem>
                                    ))}
                                  </Select>
                                </FormControl>
                              </Box>
                            </div>
                          </div>
                          <div className="word-with-line">
                            <span>Beneficiary Information</span>
                            <i
                              class="fa-solid fa-angle-down"
                              style={{ color: "#4e2683" }}
                            ></i>
                          </div>

                          <div className="col-md-4" style={{ paddingRight: 0 }}>
                            <Box sx={{ minWidth: "100%" }}>
                              <FormControl fullWidth>
                                <InputLabel id="demo-simple-select-label">
                                  Division
                                </InputLabel>
                                <Select
                                  labelId="demo-simple-select-label"
                                  id="demo-simple-select"
                                  value={selectKPMGDivision}
                                  onChange={handleDivisionKMPGChange}
                                  label="Division"
                                >
                                  {division.map((e) => (
                                    <MenuItem value={e.id}>
                                      {e.divisions}
                                    </MenuItem>
                                  ))}
                                </Select>
                              </FormControl>
                            </Box>
                          </div>

                          <div className="col-md-4" style={{ paddingRight: 0 }}>
                            <Box sx={{ minWidth: "100%" }}>
                              <FormControl fullWidth>
                                <InputLabel id="beneficiary-select-label">
                                  Facility
                                </InputLabel>
                                <Select
                                  labelId="beneficiary-select-label"
                                  id="beneficiary-select"
                                  value={beneStudyTourFarm}
                                  onChange={handleFPOVCOUtilizationChange}
                                  label="Facility"
                                >
                                  {studyFarmApplicant.map((e) => (
                                    <MenuItem key={e.id} value={e.id}>
                                      {e.name}
                                    </MenuItem>
                                  ))}
                                </Select>
                              </FormControl>
                            </Box>
                          </div>
                          <div className="col-md-4" style={{ paddingRight: 0 }}>
                            <MultiSelect
                              label="Target Crops"
                              data={magentCrop}
                              value={esiTargetCrop || []}
                              readOnly={true}
                            />
                          </div>
                          <br />
                          <div className="word-with-line">
                            <span>Details of current Quarter</span>
                            <i
                              class="fa-solid fa-angle-down"
                              style={{ color: "#4e2683" }}
                            ></i>
                          </div>
                          <UtilizationColdStorageMSAMB />

                          <div
                            style={{ display: "flex", alignItems: "center" }}
                          >
                            <div
                              class="col-md-6"
                              style={{
                                position: "relative",
                                right: "15px",
                                bottom: "10px",
                              }}
                            >
                              <label
                                for="inputNumber"
                                class="col-lg col-form-label"
                              >
                                Remarks
                              </label>
                              <div class="col-sm-10">
                                <input
                                  class="form-control"
                                  type="file"
                                  id="formFile"
                                  name="remarks"
                                  multiple
                                  onChange={handleMultiFilesGAPChange}
                                  onFocus={() => {
                                    setErrorCapacityFile("");
                                  }}
                                />
                                <span style={{ color: "red" }}>
                                  {errorCapacityFile}
                                </span>
                              </div>
                            </div>

                            <div>
                              {Array.isArray(editInput1Files) &&
                                editInput1Files.map((e) => (
                                  <div className="icon-container" key={e.id}>
                                    <a
                                      href={`${fileUrl}${e.file_name}`}
                                      rel="noreferrer"
                                      target="_blank"
                                      style={{ marginRight: "10px" }}
                                    >
                                      <img
                                        src={e.type === "pdf" ? pdf : imgIcon}
                                        alt=""
                                        height="30px"
                                        width="26px"
                                      />
                                    </a>
                                    <i
                                      className="fa-regular fa-circle-xmark"
                                      style={{ cursor: "pointer" }}
                                      onClick={() => handleDeleteFile(e.id)}
                                    ></i>
                                  </div>
                                ))}
                            </div>

                          </div>

                          <div style={{ textAlign: "center" }}>
                            {permission.includes(
                              "If-Utilisation-Facility-Cold-Storage-Input-Three-view"
                            ) ? (
                              <>
                                <button
                                  type="submit"
                                  class="btn submitbtn"
                                  disabled={loading}
                                >
                                  {postHarvestFlagEdit === true
                                    ? "Update"
                                    : "Submit"}
                                </button>
                                {postHarvestFlagEdit === true && (
                                  <button
                                    type="button"
                                    className="btn submitbtn"
                                    disabled={loading}
                                    onClick={handleDeleteColdStorage}
                                  >
                                    Delete
                                  </button>
                                )}
                              </>
                            ) : (
                              <></>
                            )}
                          </div>
                        </form>
                      </div>
                    </TabPanel>
                  </TabContext>
                </Box>
              </div>
            </div>
          </div>
        </section>
      </main>
   
    </>
  );
};
