import { Box, FormControl, InputLabel, Select } from "@mui/material";
import { InputTwoTabFil } from "../subcomponents/Input2TabFil/InputTwoTabFil";
import { InputField } from "../subcomponents/InputField";
import { setInputFieldData } from "../redux/slice/InputTwo/MasterSubSlice";
import Swal from "sweetalert2";
import { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import axios from "axios";
import { getConfigWithToken } from "../utils/Config/Config";

export const InputFilForm = () => {
  const apiUrl = process.env.REACT_APP_API_URL;
  const dd = useSelector(setInputFieldData);
  const [existCrop, setExistCrop] = useState([]);
  const [magentCrop, setMagentCrop] = useState([]);
  const [entity, setEntity] = useState([]);

  const handleFIL = async (e) => {
    e.preventDefault();

    const jsonExistCrop = selectExistingCrop
      .map((id) => {
        const matchingObject = existCrop.find((item) => item.id === id);
        if (matchingObject) {
          return {
            id: matchingObject.id,
            name: matchingObject.crop,
          };
        }
        return null; // Handle cases where there's no matching ID
      })
      .filter((item) => item !== null); // Remove any null entries

    const resultExistCrop = { data: jsonExistCrop };

    const jsonMagnetCrop = selectMagnetCrop
      .map((id) => {
        const matchingObject = magentCrop.find((item) => item.id === id);
        if (matchingObject) {
          return {
            id: matchingObject.id,
            name: matchingObject.crop,
          };
        }
        return null; // Handle cases where there's no matching ID
      })
      .filter((item) => item !== null); // Remove any null entries

    const resultMagnetCrop = { data: jsonMagnetCrop };

    console.log(resultMagnetCrop, "wqreqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqqq");
    console.log(resultExistCrop, "222222222222222223333333333");

    const filData = dd["payload"]["filSlice"];
    const filSlice = {
      ...filData,
      // "quarter": selectQuarter,
      applicant_id: esiFpoApplicants,
      district_id: esiDistrict.district_id,
      division_id: esiDivision.division_id,
      taluka_id: esiTaluka.taluka_id,
      entity_type_id: selectEntity,
      magnet_crop_id: JSON.stringify(resultMagnetCrop["data"]),
      existing_crop_id: JSON.stringify(resultExistCrop["data"]),
    };

    try {
      const filForm = await axios.post(
        `${apiUrl}api/fill-create`,
        filSlice,
        getConfigWithToken()
      );
      console.log(filForm, "jfdbvkjfdoivonidsnvinecoineicnreicmvoiemoineoinre");

      if (filForm.data["status"] === true) {
        Swal.fire(`${filForm.data["message"]}`, "", "success");
        handleResetButtonClick();
      } else {
        Swal.fire(`${filForm.data["message"]}`, "", "warning");
      }
    } catch (err) {
      Swal.fire(
        "An error occurred while submitting the FIL form",
        "Please try again later",
        "error"
      );
    }
  };

  useEffect(() => {
    try {
      const getData = async () => {
        const entity = await axios.get(
          `${apiUrl}api/getEntityType`,
          getConfigWithToken()
        );
        const existCrop = await axios.get(
          `${apiUrl}api/getAllExistingCrop`,
          getConfigWithToken()
        );
        const magentCrop = await axios.get(
          `${apiUrl}api/getAllCrop`,
          getConfigWithToken()
        );
        const existCropData = existCrop.data["data"].map((e) => ({
          id: e["id"],
          crop: e["crop"],
        }));
        const magnetCropData = magentCrop.data["data"].map((e) => ({
          id: e["id"],
          crop: e["crop"],
        }));
        const entityData = entity.data["data"].map((e) => ({
          id: e["entity_type_id"],
          entity_type: e["entity_type"],
        }));
        setEntity(entityData);
        setExistCrop(existCropData);
        setMagentCrop(magnetCropData);
      };
      getData();
    } catch (error) {}
  }, []);

  return (
    <>
      <div className="tab-pane">
        <form class="row g-3" onSubmit={handleFIL}>
          <div className="word-with-line">
            <span>Beneficiary Information</span>
            <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
          </div>
          <InputField label="Name of Borrower" type="text" value={filBorrow} />
          <InputField
            label="Division"
            type="text"
            readOnly={true}
            value={
              esiDivision && esiDivision.divisions
                ? esiDivision.divisions.toString()
                : ""
            }
          />
          <InputField
            label="District"
            type="text"
            readOnly={true}
            value={
              esiDistrict && esiDistrict.district
                ? esiDistrict.district.toString()
                : ""
            }
          />
          <InputField
            label="Taluka"
            type="text"
            readOnly={true}
            value={
              esiTaluka && esiTaluka.taluka ? esiTaluka.taluka.toString() : ""
            }
          />

          <div className="col-md-4">
            <Box>
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">
                  Entity Type
                </InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={selectEntity}
                  onChange={handleEntity}
                  label="Entity Type"
                >
                  {entity.map((e) => (
                    <MenuItem key={e.id} value={e.id}>
                      {e["entity_type"]}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </div>
          <div className="col-md-4">
            <MultiSelect
              label="Existing Crops"
              data={existCrop}
              value={selectExistingCrop}
              onChange={handleMultiChange}
            />
          </div>
          <div className="col-md-4">
            <MultiSelect
              label="MAGNET Crops"
              data={magentCrop}
              value={selectMagnetCrop}
              onChange={handleMultiMagnetCropChange}
            />
          </div>
          
          <div className="word-with-line">
            <span>Input for</span>
            <i class="fa-solid fa-angle-down" style={{ color: "#4e2683" }}></i>
          </div>
          <InputTwoTabFil />
          <div style={{ textAlign: "center" }}>
            <button type="submit" class="btn submitbtn">
              Submit
            </button>
            <button type="reset" class="btn btn-secondary">
              Reset
            </button>
          </div>
        </form>
      </div>
    </>
  );
};
