import React, { useEffect, useState } from "react";
import { Box, Pagination } from "@mui/material";
import { Header } from "../../../components/Header";
import { LeftNavigation } from "../../../components/LeftNavigation";
import axios from "axios";
import { getConfigWithToken } from "../../../utils/Config/Config";
import Swal from "sweetalert2";
import "../../../assets/css/masterdata.css";

export const BankAttribute = () => {
  const [attributeOptions, setAttributeOptions] = useState([]);
  const [getAllData, setGetAllData] = useState({});
  const [page, setPage] = useState(1);
  const apiUrl = process.env.REACT_APP_API_URL;

  useEffect(() => {
    fetchData();
  }, []);

  // Fetching list of Banks 
  const fetchData = async () => {
    try {
      const getfacility = await axios.get(
        `${apiUrl}api/get-all-banks`,
        getConfigWithToken()
      );
      if (getfacility.data && getfacility.data.data.data) {
        const facility = getfacility.data.data.data.map((e) => ({
          id: e.id,
          bank_name: e.bank_name,
        }));
        console.log(facility, "fac");
        setAttributeOptions(facility);
        setGetAllData(getfacility.data.data);
      } else {
        console.error("Unexpected response structure:", getfacility.data);
      }
    } catch (error) {
      console.error("Error fetching Bank:", error);
    }
  };

  // Add New Attribute
  const handleAddOption = async () => {
    try {
      const { value: enterValue } = await Swal.fire({
        title: "Enter Bank Attribute",
        input: "text",
        inputLabel: "Bank Attribute",
        inputValue: "",
        showCancelButton: true,
        confirmButtonText: "Save",
        inputValidator: (value) => {
          if (!value) {
            return "Enter Bank Attribute";
          }
        },
      });

      if (enterValue) {
        const response = await axios.post(
          `${apiUrl}api/bank-create`,
          {
            bank_name: enterValue,
          },
          getConfigWithToken()
        );

        if (response.data && response.data.status === true) {
          Swal.fire(`Attribute Added Successfully`, "", "success");
          await fetchData();
        } else {
          throw new Error("Unexpected response structure");
        }
      }
    } catch (error) {
      console.error("Error adding Bank attribute:", error);
      Swal.fire(
        `${error.message}`,
        "",
        "warning"
      );
    }
  };

  // Edit the exisiting attribute
  const handleEditFunction = async (value, id) => {
    try {
      const { value: editValue } = await Swal.fire({
        title: "Edit Bank Attribute",
        input: "text",
        inputLabel: "Your Bank Attribute",
        inputValue: value,
        showCancelButton: true,
        confirmButtonText: "Edit",
        inputValidator: (value) => {
          if (!value) {
            return "Enter Bank Attribute";
          }
        },
      });

      if (editValue) {
        const response = await axios.post(
          `${apiUrl}api/bank-update`,
          {
            bank_name: editValue,
            id: id,
          },
          getConfigWithToken()
        );

        if (response.data && response.data.status === true) {
          Swal.fire(`Attribute Edited Successfully`, "", "success");
          await fetchData();
        } else {
          throw new Error("Unexpected response structure");
        }
      }
    } catch (error) {
      console.error("Error editing Bank attribute:", error);
      Swal.fire(
        `${error.message}`,
        "",
        "warning"
      );
    }
  };

  // Delete the Attibute
  const handleDeleteFunction = async (id) => {
    try {
      const result = await Swal.fire({
        title: "Are you sure?",
        text: "You won't be able to revert this!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Yes, delete it!",
      });

      if (result.isConfirmed) {
        const response = await axios.get(
          `${apiUrl}api/bank-delete?id=${id}`,
          getConfigWithToken()
        );

        if (response.data && response.data.status === true) {
          Swal.fire({
            title: "Deleted!",
            text: "Attribute has been deleted.",
            icon: "success",
          });
          await fetchData();
        } else {
          throw new Error("Unexpected response structure");
        }
      }
    } catch (error) {
      console.error("Error deleting Bank attribute:", error);
      Swal.fire(
        `${error.message}`,
        "",
        "warning"
      );
    }
  };

  // onChange of Pagination
  const handleChange = async (event, value) => {
    try {
      const getfacilitiestype = await axios.get(
        `${apiUrl}api/get-all-beneficiary-type?page=${value}`,
        getConfigWithToken()
      );
      const facilitiestype = getfacilitiestype.data["data"]["data"].map(
        (e) => ({
          id: e.id,
          bank_name: e.bank_name,
        })
      );
      setAttributeOptions(facilitiestype);
      setGetAllData(getfacilitiestype.data.data.data);
      setPage(value);
    } catch (error) {
      console.error("Error fetching Bank:", error);
    }
  };

  return (
    <>
      <Header />
      <LeftNavigation />
      <main id="main" className="main">
        <section className="section">
          <div className="row">
            <div id="exTab3" className="contain" style={{ width: "92%" }}>
              <div
                className="cardMaster"
                id=""
                style={{ width: "100%", marginTop: "1rem" }}
              >
                <form class="row g-3">
                  <Box
                    className="your-component"
                    sx={{ typography: "body1", width: "88%" }}
                  >
                    <div className="words-with-lines">
                      <span>Bank</span>
                    </div>
                    <div className="your-link">
                      <a href="./masterdata" style={{ marginRight: "1rem" }}>
                        <i className="fa-solid fa-arrow-left"></i>
                        Back
                      </a>
                    </div>
                  </Box>
                  <Box className="your-box" sx={{ typography: "body1" }}>
                    <div>
                      <span>Manage Options (Value of your Attribute)</span>
                    </div>
                  </Box>
                  <div className="your-button-container">
                    <button
                      type="button"
                      style={{ width: "30%" }}
                      onClick={(e) => handleAddOption(e)}
                    >
                      <i className="fa-duotone fa-plus"></i> Add Attributes
                      Options{" "}
                    </button>
                  </div>
                  <Box className="custom-box" style={{ marginLeft: "2rem" }}>
                    <table className="custom-table">
                      <thead>
                        <tr>
                          <th>Attribute Options</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody className="your-tbody">
                        {attributeOptions.map((option, index) => (
                          <tr key={index}>
                            <td style={{ border: "1px solid black" }}>
                              <input
                                type="text"
                                value={option.bank_name}
                                disabled
                              />
                            </td>
                            <td>
                              <i
                                class="fa-solid fa-pen-to-square"
                                onClick={() =>
                                  handleEditFunction(
                                    option.bank_name,
                                    option.id
                                  )
                                }
                                style={{ cursor: "pointer", color: "#4e2683" }}
                              ></i>
                              &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                              <i
                                className="fa-regular fa-trash-can"
                                onClick={() => handleDeleteFunction(option.id)}
                                style={{ cursor: "pointer", color: "#4e2683" }}
                              ></i>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                      <Pagination
                        count={getAllData.last_page}
                        page={page}
                        onChange={handleChange}
                        color="primary"
                      />
                    </table>
                  </Box>
                </form>
              </div>
            </div>
          </div>
        </section>
      </main>
    </>
  );
};
