import React, { useEffect, useState } from "react";
import { Box, Pagination } from "@mui/material";
import { Header } from "../../../components/Header";
import { LeftNavigation } from "../../../components/LeftNavigation";
import axios from "axios";
import { getConfigWithToken } from "../../../utils/Config/Config";
import Swal from "sweetalert2";
import '../../../assets/css/masterdata.css'

export const TalukaAttribute = () => {
  const [attributeOptions, setAttributeOptions] = useState([]);
  const [getAllData, setGetAllData] = useState({});
  const [page, setPage] = useState(1);
  const apiUrl = process.env.REACT_APP_API_URL;
  const [districts, setDistricts] = useState('');

  // Add new attribute
  const handleAddOption = async () => {
    let value = "";
    const { value: enterValue, isConfirmed } = await Swal.fire({
      title: "Enter Taluka Attribute",
      html: `<div>
          <label for="district-select">Select District:</label>
          <select id="district-select" class="swal2-select">
            ${districts.map((district) => `<option value="${district.id}">${district.district}</option>`)}
          </select>
        </div><br><br>
        <label for="taluka-input">Enter Taluka Attribute:</label>
        <input type="text" id="swal-input1" class="swal2-input" placeholder="Taluka Attribute" value="${value}">
        `,
      inputLabel: "Taluka Attribute",
      inputValue: value,
      showCancelButton: true,
      confirmButtonText: "Save",
      preConfirm: () => {
        const inputValue = document.getElementById("swal-input1").value;
        const districtSelect = document.getElementById("district-select");
        if (!inputValue || districtSelect === null) {
          Swal.showValidationMessage("Please enter both values");
        }

        return {
          taluka: inputValue,
          districtId: districtSelect.value,
        };
      },
    });

    if (isConfirmed) {
      try {
        const editAttribute = await axios.post(
          `${apiUrl}api/taluka-create`,
          {
            taluka: enterValue.taluka,
            district_id: enterValue.districtId,
          },
          getConfigWithToken()
        );

        if (editAttribute.data["status"] === true) {
          Swal.fire(`Attribute Added Successfully`, "", "success");
          // Fetch updated data after successful addition
          fetchData();
        } else {
          Swal.fire(`${editAttribute.data["message"]}`, "", "warning");

        }
      } catch (error) {
        console.error("API Error:", error);
        Swal.fire(
          `${error.message}`,
          "",
          "warning"
        );
      }
    }
  };

  // Edit exisitng data
  const handleEditFunction = async (value, id, district_id, option) => {
    try {
      // Fetch the district information for the selected taluka
      const talukaEditResponse = await axios.get(
        `${apiUrl}api/taluka-edit?id=${id}`,
        getConfigWithToken()
      );

      const talukaData = talukaEditResponse.data.data[0];
      const selectedDistrictId = talukaData.district_id;

      const { value: editValue, isConfirmed } = await Swal.fire({
        title: "Edit Taluka Attribute",
        html: `<div>
            <label for="district-select">Select District:</label>
            <select id="district-select" class="swal2-select">
              ${districts.map((district) => `<option value="${district.id}" ${district.id === selectedDistrictId ? 'selected' : ''}>${district.district}</option>`)}
            </select>
          </div><br><br>
          <label for="taluka-input">Edit Taluka Attribute:</label>
          <input type="text" id="swal-input1" class="swal2-input" placeholder="Taluka Attribute" value="${value}">
          `,
        inputLabel: "Your Taluka Attribute",
        inputValue: value,
        showCancelButton: true,
        confirmButtonText: "Edit",
        preConfirm: () => {
          const inputValue = document.getElementById("swal-input1").value;
          const districtSelect = document.getElementById("district-select");
          if (!inputValue || districtSelect === null) {
            Swal.showValidationMessage("Please enter both values");
          }

          return {
            taluka: inputValue,
            district_id: districtSelect.value,
          };
        },
        inputValidator: (value) => {
          if (!value) {
            return "Enter Taluka Attribute";
          }
        },
      });

      if (isConfirmed) {
        const editAttribute = await axios.post(
          `${apiUrl}api/taluka-update`,
          {
            taluka: editValue.taluka,
            district_id: editValue.district_id,
            id: id,
          },
          getConfigWithToken()
        );

        if (editAttribute.data["status"] === true) {
          Swal.fire(`Attribute Edited Successfully`, "", "success");
          // Fetch updated data after successful edit
          fetchData();
        } else {
          Swal.fire(`${editAttribute.data["message"]}`, "", "warning");
        }
      }
    } catch (error) {
      console.error("API Error:", error);
      Swal.fire(
        `${error.message}`,
        "",
        "warning"
      );
    }
  };

  // Delete the attribute
  const handleDeleteFunction = (id) => {
    Swal.fire({
      title: "Are you sure?",
      text: "You won't be able to revert this!",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Yes, delete it!"
    }).then(async (result) => {
      if (result.isConfirmed) {
        try {
          const deleteAttribute = await axios.get(`${apiUrl}api/taluka-delete?id=${id}`, getConfigWithToken());
          if (deleteAttribute.data["status"] === true) {
            Swal.fire({
              title: "Deleted!",
              text: "Attribute has been deleted.",
              icon: "success"
            });
            // Fetch updated data after successful deletion
            fetchData();
          } else {
            Swal.fire(`${deleteAttribute.data["message"]}`, "", "warning");
          }
        } catch (error) {
          console.error("API Error:", error);
          Swal.fire(
            `${error.message}`,
            "",
            "warning"
          );
        }
      }
    });
  };

  // OnChange of pagination
  const handleChange = async (event, value) => {
    try {
      const getTalukas = await axios.get(
        `${apiUrl}api/get-all-taluka-with-village?page=${value}`,
        getConfigWithToken()
      );
      const talukas = getTalukas.data["data"]["data"].map((e) => ({
        id: e.id,
        taluka: e.taluka,
        district_id: e.district_id
      }));
      setAttributeOptions(talukas);
      setGetAllData(getTalukas.data.data);
      setPage(value);
    } catch (error) {
      console.error("Error fetching Talukas:", error);
    }
  };

  // Fetching the exisiting data
  const fetchData = async () => {
    try {
      const getTalukas = await axios.get(
        `${apiUrl}api/get-all-taluka-with-village`,
        getConfigWithToken()
      );
      if (getTalukas.data && getTalukas.data.data && Array.isArray(getTalukas.data.data.data)) {
        const talukas = getTalukas.data.data.data.map((e) => ({
          id: e.id,
          taluka: e.taluka,
          district_id: e.district_id
        }));
        setAttributeOptions(talukas);
        setGetAllData(getTalukas.data.data);
      } else {
        console.error('Unexpected response structure or empty data:', getTalukas.data);
        
      }
    } catch (error) {
      console.error('Error fetching Talukas:', error);
     
    }
  };

  useEffect(() => {
    async function getTalukasData() {
      try {
        const getTalukas = await axios.get(
          `${apiUrl}api/get-all-taluka-with-village`,
          getConfigWithToken()
        );

        const districtResponse = await axios.get(
          `${apiUrl}api/get-all-district?all=1`,
          getConfigWithToken()
        );
        const districtResponseData = districtResponse.data["data"].map((e) => ({
          "id": e["id"],
          "district": e["district"]
        }));
        setDistricts(districtResponseData);

        if (getTalukas.data && getTalukas.data.data && Array.isArray(getTalukas.data.data.data)) {
          const talukas = getTalukas.data.data.data.map((e) => ({
            id: e.id,
            taluka: e.taluka,
            district_id: e.district_id
          }));
          setAttributeOptions(talukas);
          setGetAllData(getTalukas.data.data);
        } else {
          console.error('Unexpected response structure or empty data:', getTalukas.data);
         
        }
      } catch (error) {
        console.error('Error fetching Talukas:', error);
       
      }
    }

    getTalukasData();
  }, []);


  return (
    <>
      <Header />
      <LeftNavigation />
      <main id="main" className="main">
      <section className="section">
        <div className="row">
          <div id="exTab3" className="contain" style={{ width: '100%', overflow: 'hidden' }}> 
            <div className="cardMaster" id="" style={{ width: '100%', marginTop: '0.5rem' }}>
              <form className="row g-3" style={{ width: '100%', maxWidth: '100%', margin: 'auto' }}>
                <Box className="your-component" sx={{ typography: 'body1', width: "88%" }}>
                <div className="words-with-lines">
                      <span>Taluka</span>
                    </div>
                    <div className="your-link">
                      <a href="./masterdata" style={{marginRight:'1rem'}}>
                        <i className="fa-solid fa-arrow-left"></i>
                        Back
                      </a>
                    </div>
                  </Box>
                  <Box className="your-box" sx={{typography: "body1"}}>
                    <div>
                      <span>Manage Options (Value of your Attribute)</span>
                    </div>
                  </Box>
                  <div className="your-button-container">
                    <button type="button" style={{width:'30%'}} onClick={(e) => handleAddOption(e)}>
                      <i className="fa-duotone fa-plus"></i> Add Attributes
                      Options{" "}
                    </button>
                  </div>
                  <Box className='custom-box' style={{marginLeft:'2rem'}}>
                    <table className="custom-table">
                      <thead>
                        <tr>
                          <th>Attribute Options</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody className="your-tbody">
                        {attributeOptions.map((option, index) => (
                          <tr key={index}>
                            <td style={{border: "1px solid black"}}>
                              <input type="text" value={option.taluka}  disabled/>
                            </td>
                            <td>
                              <i
                                class="fa-solid fa-pen-to-square"
                                onClick={()=>handleEditFunction(option.taluka, option.id, option.district_id, option)}
                                style={{ cursor: "pointer", color: "#4e2683" }}
                              ></i>
                              &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;
                              <i
                                className="fa-regular fa-trash-can"
                                onClick={()=> handleDeleteFunction(option.id)}
                                style={{ cursor: "pointer", color: "#4e2683" }}
                              ></i>
                            </td>
                          </tr>
                        ))}
                      </tbody>
                      <Pagination
                        count={getAllData.last_page}
                        page={page}
                        onChange={handleChange}
                        color="primary"
                        />
                    </table>
                  </Box>
                  </form>
                </div>
              </div>
            </div>
          </section>
        </main>
    
    </>
  );
};
