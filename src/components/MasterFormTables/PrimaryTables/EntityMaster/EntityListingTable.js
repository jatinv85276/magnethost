import React, { useEffect, useMemo, useState } from 'react';
import { useTable} from 'react-table';
import { Box, CircularProgress, FormControl, InputLabel, ListSubheader, MenuItem, Select } from '@mui/material';
import axios from 'axios';
import Swal from 'sweetalert2';
import AxiosRetry from 'axios-retry';

export const EntityMasterListingTable = () => {
  const apiUrl = process.env.REACT_APP_API_URL;
  const [loading, setLoading] = useState(true);
  const [tableData, setTableData] = useState([])

  // Create an Axios instance with retry settings
  const axiosInstance = axios.create({
    baseURL: apiUrl,
    timeout: 15000, // Set a timeout for requests
  });

  // Apply retry settings to the instance
  AxiosRetry(axiosInstance, {
    retries: 3, // Number of retry attempts
    retryDelay: AxiosRetry.exponentialDelay, // Exponential backoff
  });

    const columns = useMemo(
        () => [
          
          {
            Header: 'Name of Entity', 
            accessor: 'applicant_name',
          },
          
          {
            Header: 'Division', 
            accessor: 'divisions',
          },
          {
            Header: 'District', 
            accessor: '',
          },
          {
            Header: 'Taluka',
            accessor: '',
          },
          {
            Header: 'Entity Type',
            accessor: '',
          },
         {
            Header: 'Status',
            accessor: '',
         },
        
        ],
        []
      );
  
  // Calculate the total width for equally sized columns
  const totalWidthForEqualColumns = 800; // Adjust as needed (reduced from 900)

  // Calculate the width for each equally sized column
  const equalColumnWidth = totalWidthForEqualColumns / (columns.length - 1);

  // Set the width for each column (excluding "Components")
  columns.forEach((column, index) => {
    if (index !== 0) {
      column.width = equalColumnWidth;
    }
  });

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    rows,
    prepareRow,
  } = useTable({
    columns,
    data: tableData, // Assuming data is an array with one element containing the data array.
  });

  const cellStyle = {
    padding: '8px',
    border: '1px solid #ccc', // Add border style here
    textAlign: 'center', // Center text horizontally
    verticalAlign: 'middle', // Center text vertically
  };

  
  return (
    <>
     
        <div className="row" style={{ width: '92%', marginLeft:"5rem", marginTop:"1rem", typography: 'body1'}}>
            <table {...getTableProps()} className="my-table">
              <thead>
                  {headerGroups.map((headerGroup) => (
                      <tr {...headerGroup.getHeaderGroupProps()} >
                      {headerGroup.headers.map((column) => (
                          <th
                          {...column.getHeaderProps()}
                          style={{
                            borderBottom: '2px solid #4e2683',
                            background: '#4F2D7F', // Add grey background color
                            fontWeight: 'bold', // Optionally make text bold
                            // border: '2px solid #ccc', // Add border style here
                            color:'white',
                            padding:'15px'
                          }}
                          >
                          {column.render('Header')}
                          </th>
                      ))}
                      </tr>
                  ))}
              </thead>
              <tbody {...getTableBodyProps()}>
                  {rows.map((row) => {
                      prepareRow(row);
                      return (
                      <tr
                          {...row.getRowProps()}
                          style={{ borderBottom: '1px solid #ccc' }} // Add border style here
                      >
                          {row.cells.map((cell) => {
                          return (
                              <td
                              {...cell.getCellProps()}
                              style={{
                                  padding: '8px',
                                  border: '1px solid #ccc', // Add border style here
                              }}
                              >
                              {cell.render('Cell')}
                              </td>
                          );
                          })}
                      </tr>
                      );
                  })}
              </tbody>
          </table>
      </div>
    </>
  );
};


