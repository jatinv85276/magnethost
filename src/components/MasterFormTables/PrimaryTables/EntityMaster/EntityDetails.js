import { Offline, Online } from "react-detect-offline";
import React from 'react';
import { Box } from '@mui/material';


export const EntityDetailsMaster = () =>{ 
    
    const labelStyle = {
        marginBottom: "20px",
        marginRight:"20px",
        width: "200px", 
        textAlign: "right", 
        fontWeight:"400",
        marginLeft:"2rem",
    }

    return(<>   
    {/* <Online> */}
       <div id="exTab3" class="contain">
            <form>
                <div>
                    <label for="ApplicantName" style={labelStyle}>Name of Entity</label>
                    <input type="text" name="ApplicantName" style={{width:"50%",height:"30px"}}/>
                </div>
                <div>
                    <label for="Division" style={labelStyle}>Division</label>
                    <select name="Division" style={{width:"30%" , height:"30px"}}>
                        <option value="Division1">Division</option>                                              
                    </select>
                </div>
                <div>
                    <label for="District" style={labelStyle}>District</label>
                    <select name="District" style={{width:"30%" , height:"30px"}}>
                        <option value="District">District</option>                                              
                    </select>
                </div>
                <div>
                    <label for="Entity Type" style={labelStyle}>Entity Type</label>
                    <select name="Entity Type" style={{width:"20%" , height:"30px"}}>
                        <option value="Entity Type">Entity Type</option>                                              
                    </select>
                </div>
                <div>
                    <label for="Status" style={labelStyle}>Status</label>
                    <select name="Status" style={{width:"20%" , height:"30px"}}>
                        <option value="Status">Status</option>                                              
                    </select>
                </div>
                
            </form>

            <Box sx={{ width: '100%', height:'500px', marginLeft:"1rem", marginTop:"1rem", typography: 'body1', border: '1px solid red' }}>
            {/* add check boxes data here */}
            </Box>
        </div>     
    {/* </Online>
    <Offline>
        You're Offline, Please Check your Connection
    </Offline> */}
    </>)
}

