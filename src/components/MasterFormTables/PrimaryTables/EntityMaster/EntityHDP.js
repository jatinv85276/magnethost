import { Offline, Online } from "react-detect-offline";
import React from 'react';


export const EntityHDP = () =>{ 
    
    const labelStyle = {
        marginBottom: "20px",
        marginRight:"20px",
        width: "320px", 
        textAlign: "right", 
        fontWeight:"400",
        marginLeft:"2rem",
    };

    return(<>   
    {/* <Online> */}
       <div id="exTab3" class="contain">
            <form>
                <div>
                    <label for="Crop" style={labelStyle}>MAGNET Crops</label>
                    <select name="Crop" style={{width:"50%" , height:"30px"}}>
                        <option value="Crop"></option>                                              
                    </select>
                </div>
                <div>
                    <label for="Location" style={labelStyle}>Location (Taluka)</label>
                    <select name="Location" style={{width:"50%" , height:"30px"}}>
                        <option value="Location"></option>                                              
                    </select>
                </div>
                <div>
                    <label for="FarmersDemon" style={labelStyle}>Nos. of Farmers Demonstrated</label>
                    <input type="text" name="FarmersDemon" style={{width:"50%",height:"30px"}}/>
                </div>
                <div>
                    <label for="AreaPlantation" style={labelStyle}>Area of Plantation</label>
                    <input type="number" name="AreaPlantation" style={{width:"50%",height:"30px"}}/>
                </div>
                <div>
                    <label for="Cost" style={labelStyle}>Total Plantation Cost</label>
                    <input type="number" name="FruitCareCost" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="SanctionCost" style={labelStyle}>Total Saction Grant from MAGNET *</label>
                    <input type="number" name="SanctionCost" style={{width:"30%",height:"30px"}}/>
                </div>  
            </form>
            {/* Add Payment Tranche Details */}
        </div>     
    {/* </Online>
    <Offline>
        You're Offline, Please Check your Connection
    </Offline> */}
    </>)
}

