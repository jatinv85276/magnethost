import { Offline, Online } from "react-detect-offline";
import React from 'react';


export const EntityMRLTesting = () =>{ 
    
    const labelStyle = {
        marginBottom: "20px",
        marginRight:"20px",
        width: "320px", 
        textAlign: "right", 
        fontWeight:"400",
        marginLeft:"2rem",
    };

    return(<>   
    {/* <Online> */}
       <div id="exTab3" class="contain">
            <form>
                <div>
                    <label for="Crop" style={labelStyle}>MAGNET Crops</label>
                    <select name="Crop" style={{width:"50%" , height:"30px"}}>
                        <option value="Crop"></option>                                              
                    </select>
                </div>
                <div>
                    <label for="Location" style={labelStyle}>Location (Taluka)</label>
                    <select name="Location" style={{width:"50%" , height:"30px"}}>
                        <option value="Location"></option>                                              
                    </select>
                </div>
                <div>
                    <label for="FarmersTesting" style={labelStyle}>Nos. of Farmers applying of MRL Testing</label>
                    <input type="text" name="FarmersTesting" style={{width:"50%",height:"30px"}}/>
                </div>
                <div>
                    <label for="TestingLabName" style={labelStyle}>Name of Certification Testing Laboratories *</label>
                    <input type="text" name="TestingLabName" style={{width:"50%",height:"30px"}}/>
                </div>
                <div>
                    <label for="TestCost" style={labelStyle}>Total Testing Cost</label>
                    <input type="text" name="TestCost" style={{width:"50%",height:"30px"}}/>
                </div>
                <div>
                    <label for="SanctionGrant" style={labelStyle}>Total Sanction Grant from MAGNET</label>
                    <input type="number" name="SanctionGrant" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="DisbursementAmt" style={labelStyle}>Grant Disbursement Amount</label>
                    <input type="number" name="DisbursementAmt" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="DisbursementDate" style={labelStyle}>Grant Disbursement Date</label>
                    <input type="date" name="DisbursementDate" style={{width:"30%",height:"30px"}}/>
                </div>
            </form>
        </div>     
    {/* </Online>
    <Offline>
        You're Offline, Please Check your Connection
    </Offline> */}
    </>)
}

