import React, { useMemo, useState } from 'react';
import { useTable} from 'react-table';
import { Box, CircularProgress } from '@mui/material';
import axios from 'axios';
import AxiosRetry from 'axios-retry';

export const EntityProdPlantCertTable = () => {
  const apiUrl = process.env.REACT_APP_API_URL;
  const [loading, setLoading] = useState(true);
  
  const [tableData, setTableData] = useState([])

  // Create an Axios instance with retry settings
  const axiosInstance = axios.create({
    baseURL: apiUrl,
    timeout: 15000, // Set a timeout for requests
  });

  // Apply retry settings to the instance
  AxiosRetry(axiosInstance, {
    retries: 3, // Number of retry attempts
    retryDelay: AxiosRetry.exponentialDelay, // Exponential backoff
  });

  const filterTransformData = (data) =>{
    return(Object.values(data))
  }



  const columns = useMemo(
    () => [   
       {
          Header:"Certification",
          accessor:"",
       }, 
       {
          Header:"Certification Name",
          accessor:"",
       },
       {
          Header:"Certification Agency",
          accessor:"",
       },
       {
          Header:"Location(Taluka)",
          accessor:"",
       },  
       {
          Header:"Total cost of Certification",
          accessor:"",
       },
       {
          Header:"Total Sanction Grant from MAGNET",
          accessor:"",
       },
       {
          Header:"Beneficiary Contribution",
          accessor:"",
       },
    ],
    []
  );

  
  // Calculate the total width for equally sized columns
  const totalWidthForEqualColumns = 800; // Adjust as needed (reduced from 900)

  // Calculate the width for each equally sized column
  const equalColumnWidth = totalWidthForEqualColumns / (columns.length - 1);

  // Set the width for each column (excluding "Components")
  columns.forEach((column, index) => {
    if (index !== 0) {
      column.width = equalColumnWidth;
    }
  });

  const {
    getTableProps,
    headerGroups,
  } = useTable({
    columns,
    data: tableData, // Assuming data is an array with one element containing the data array.
  });

  const cellStyle = {
    padding: '8px',
    border: '1px solid #ccc', // Add border style here
    textAlign: 'center', // Center text horizontally
    verticalAlign: 'middle', // Center text vertically
  };

  return (
    <>
      <div style={{display: "flex", justifyContent: "space-between", marginBottom: "10px"}}>
        
      </div>
      <div style={{ width: '100%', overflowX: 'auto' }}>
          <table {...getTableProps()} className="table">
            <thead>
              {headerGroups.map(headerGroup => (
                <tr {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map(column => (
                    <th {...column.getHeaderProps()}
                    style={{
                        borderBottom: '2px solid #4e2683',
                        background: '#4e2683', // Add grey background color
                        fontWeight: 'bold', // Optionally make text bold
                        // border: '2px solid #ccc', // Add border style here
                        color:'white',
                        padding:'25px',
                      ...cellStyle // Add border style here
                  }}>
                  {column.render('Header')}</th>
                  ))}
                </tr>
              ))}
            </thead>
            {/* <tbody {...getTableBodyProps()}>
            {rows.map((row, index) => {
              prepareRow(row);
              return (
                <tr {...row.getRowProps()}>
                  {row.cells.map((cell, cellIndex) => {
                    return (
                      <td
                        {...cell.getCellProps()}
                        style={{
                          padding: '8px',
                          border: '1px solid #ccc',
                          ...cellStyle,
                        }}
                      >
                        {cellIndex === 0 && index === 0 ? (
                          <span>{cell.value}</span> // Display the applicant name only once
                        ) : (
                          cell.render('Cell')
                        )}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody> */}
          </table>
      </div>
    </>
  );
};


