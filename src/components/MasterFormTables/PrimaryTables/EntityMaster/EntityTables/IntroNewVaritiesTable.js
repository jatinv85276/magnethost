import { Offline, Online } from "react-detect-offline";
import React from 'react';
import { Box } from '@mui/material';


export const IntroNewVaritiesPaymentTable = () =>{ 

    const containerStyle = {
        display: "flex",
      };
      const paymenttable = {
        margin: "-7px -7px -8px -6px",
        width: "102%",
        border: "25px white",
        height: "49px",
        padding: "27px",
        
    };
    const cellStyle = {
        border: "1px solid black",
        padding: "8px",
        textAlign: "left",
        backgroundColor: "#E0E0E0",
      };
      const firstTableStyle = {
        marginRight: "20px", // Add margin-right to create space between tables
      };
    const paymentTrancheLabels = {
        border: "1px solid black",
        padding: "8px",
        textAlign: "left",
        backgroundColor: "#E7EFFF",
        fontWeight:"bold",
    }
    const cellStyle1 = {
        border: "1px solid black",
        padding: "8px",
        textAlign: "center",
        backgroundColor: "#E0E0E0",
      };
      const tableStyle = {
        borderCollapse: "collapse",
        width: "100%",
      };  
    return(<>   
    {/* <Online> */}
       <div id="exTab3" class="contain">
                   <div style={containerStyle}>
                        <table style={{ ...tableStyle}}>
                        <thead>
                        <tr>
                            <th  colSpan={2} style={{ ...cellStyle,backgroundColor:"#4e2683",color:"white",padding:"10px", fontSize:"20px",textAlign:"center" }}>Payment Tranche</th>
                            
                        </tr>
                        <table style={{ ...tableStyle, ...firstTableStyle }}>
                        <thead>
                        <tr>
                            {/* <th style={cellStyle}>Forms</th>
                            <th style={cellStyle1}>Appicability</th> */}
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td style={paymentTrancheLabels}>Matching Grant Tranche 1 Amount</td>
                            <td style={cellStyle1}>
                            <input type="text" name="grant1" style={paymenttable} />
                            </td>
                        </tr>
                        <tr>
                            <td style={paymentTrancheLabels}>Actual Disbursement date for T1</td>
                            <td style={cellStyle1}>
                            <input type="text" name="grant1" style={paymenttable}/>
                            </td>
                        </tr>
                        <tr>
                            <td style={paymentTrancheLabels}>Matching Grant Tranche 2 Amount</td>
                            <td style={cellStyle1}>
                            <input type="text" name="grant1" style={paymenttable}/>
                            </td>
                        </tr>
                        <tr>
                            <td style={paymentTrancheLabels}>Actual Disbursement date for T2</td>
                            <td style={cellStyle1}>
                            <input type="text" name="grant1" style={paymenttable}/>
                            </td>
                        </tr>
                        <tr>
                            <td style={paymentTrancheLabels}>Matching Grant Tranche 3 Amount</td>
                            <td style={cellStyle1}>
                            <input type="text" name="grant1" style={paymenttable}/>
                            </td>
                        </tr>
                        <tr>
                            <td style={paymentTrancheLabels}>Actual Disbursement date for T3</td>
                            <td style={cellStyle1}>
                            <input type="text" name="grant1"style={paymenttable} />
                            </td>
                        </tr>
                        <tr>
                            <td style={paymentTrancheLabels}>Matching Grant Tranche 4 Amount</td>
                            <td style={cellStyle1}>
                            <input type="text" name="grant1"style={paymenttable} />
                            </td>
                        </tr>
                        <tr>
                            <td style={paymentTrancheLabels}>Actual Disbursement date for T4</td>
                            <td style={cellStyle1}>
                            <input type="text" name="grant1"style={paymenttable} />
                            </td>
                        </tr>

                        {/* ... Repeat the table content for the first table */}
                        </tbody>
                    </table>
                        </thead>
                      </table>
                    </div>
        </div>     
    {/* </Online>
    <Offline>
        You're Offline, Please Check your Connection
    </Offline> */}
    </>)
}

