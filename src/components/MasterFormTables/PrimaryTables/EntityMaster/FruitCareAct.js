import { Offline, Online } from "react-detect-offline";
import React from 'react';


export const EntitySupportFruitCareActivities = () =>{ 
    
    const labelStyle = {
        marginBottom: "20px",
        marginRight:"20px",
        width: "320px", 
        textAlign: "right", 
        fontWeight:"400",
        marginLeft:"2rem",
    };

    return(<>   
    {/* <Online> */}
       <div id="exTab3" class="contain">
            <form>
                <div>
                    <label for="Crop" style={labelStyle}>MAGNET Crops</label>
                    <select name="Crop" style={{width:"50%" , height:"30px"}}>
                        <option value="Crop"></option>                                              
                    </select>
                </div>
                <div>
                    <label for="Location" style={labelStyle}>Location (Taluka)</label>
                    <select name="Location" style={{width:"50%" , height:"30px"}}>
                        <option value="Location"></option>                                              
                    </select>
                </div>
                <div>
                    <label for="FarmersCovered" style={labelStyle}>Nos. of Farmers Covered</label>
                    <input type="text" name="FarmersCovered" style={{width:"50%",height:"30px"}}/>
                </div>
                <div>
                    <label for="FruitCareCost" style={labelStyle}>Total Fruit Care Cost</label>
                    <input type="number" name="FruitCareCost" style={{width:"50%",height:"30px"}}/>
                </div>
                <div>
                    <label for="SanctionGrant" style={labelStyle}>Total Sanction Grant from MAGNET</label>
                    <input type="number" name="SanctionGrant" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="DisbursementAmt" style={labelStyle}>Grant Disbursement Amount</label>
                    <input type="number" name="DisbursementAmt" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="DisbursementDate" style={labelStyle}>Grant Disbursement Date</label>
                    <input type="date" name="DisbursementDate" style={{width:"30%",height:"30px"}}/>
                </div>
            </form>
        </div>     
    {/* </Online>
    <Offline>
        You're Offline, Please Check your Connection
    </Offline> */}
    </>)
}

