import { Offline, Online } from "react-detect-offline";
import React from 'react';


export const EntityBioWaste = () =>{ 
    
    const labelStyle = {
        marginBottom: "20px",
        marginRight:"20px",
        width: "320px", 
        textAlign: "right", 
        fontWeight:"400",
        marginLeft:"2rem",
    };

    return(<>   
    {/* <Online> */}
       <div id="exTab3" class="contain">
            <form>
                <div>
                    <label for="Crop" style={labelStyle}>MAGNET Crops</label>
                    <select name="Crop" style={{width:"50%" , height:"30px"}}>
                        <option value="Crop"></option>                                              
                    </select>
                </div>
                <div>
                    <label for="Sub Project Title" style={labelStyle}>Sub Project Title</label>
                    <input type="text" name="Sub Project Title" style={{width:"50%",height:"30px"}}/>
                </div>
                <div>
                    <label for="MOU Signing Date" style={labelStyle}>MOU Signing Date</label>
                    <input type="date" name="MOU Signing Date" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="ProjCost" style={labelStyle}>Total Project Cost</label>
                    <input type="number" name="ProjCost" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="BeneContribution" style={labelStyle}>Beneficiary Contribution</label>
                    <input type="number" name="BeneContribution" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="SanctionGrant" style={labelStyle}>Total Sanction Grant from MAGNET</label>
                    <input type="number" name="SanctionGrant" style={{width:"30%",height:"30px"}}/>
                </div>
            </form>
            {/* Add Payment Tranche Details */}
        </div>     
    {/* </Online>
    <Offline>
        You're Offline, Please Check your Connection
    </Offline> */}
    </>)
}

