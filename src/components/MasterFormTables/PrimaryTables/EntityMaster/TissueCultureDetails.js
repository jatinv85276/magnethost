import { Offline, Online } from "react-detect-offline";
import React from 'react';
import { Box } from '@mui/material';
import { TissueCultureTable } from "./EntityTables/TissueCultureTable";


export const EntityTissueCultureDetails = () =>{ 
    
    const labelStyle = {
        marginBottom: "20px",
        marginRight:"20px",
        width: "220px", 
        textAlign: "right", 
        fontWeight:"400",
        marginLeft:"2rem",
    }

    return(<>   
    {/* <Online> */}
       <div id="exTab3" class="contain">
            <form>
                <div>
                    <label for="MAGNET" style={labelStyle}>MAGNET Crops</label>
                    <input type="text" name="MAGNET" style={{width:"50%",height:"30px"}}/>
                </div>
                <div>
                    <label for="Location (Taluka)" style={labelStyle}>Location (Taluka)</label>
                    <input type="text" name="Location (Taluka)" style={{width:"50%",height:"30px"}}/>
                </div>
                <div>
                    <label for="Area" style={labelStyle}>Area of Nursery in (Ha)</label>
                    <input type="text" name="Area" style={{width:"50%",height:"30px"}}/>
                </div>
                <div>
                    <label for="Capacity" style={labelStyle}>Capacity (Total Plants)</label>
                    <input type="number" name="Capacity" style={{width:"50%",height:"30px"}}/>
                </div>
                <div>
                    <label for="ProjectCost" style={labelStyle}>Total Project Cost </label>
                    <input type="number" name="ProjectCost" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="PromContribution" style={labelStyle}>Total Promotors Contribution</label>
                    <input type="number" name="PromContribution" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="MatchingGrant" style={labelStyle}>Total Matching Grant </label>
                    <input type="number" name="MatchingGrant" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="TermLoan" style={labelStyle}>Total Terms Loan</label>
                    <input type="number" name="TermLoan" style={{width:"30%",height:"30px"}}/>
                </div>
            </form>
           <TissueCultureTable/>
        </div>     
    {/* </Online>
    <Offline>
        You're Offline, Please Check your Connection
    </Offline> */}
    </>)
}

