import { Offline, Online } from "react-detect-offline";
import React from 'react';


export const EntitySuppComNewTech = () =>{ 
    
    const labelStyle = {
        marginBottom: "20px",
        marginRight:"20px",
        width: "320px", 
        textAlign: "right", 
        fontWeight:"400",
        marginLeft:"2rem",
    };

    return(<>   
    {/* <Online> */}
       <div id="exTab3" class="contain">
            <form>
                <div>
                    <label for="Cost of Technology" style={labelStyle}>Cost of Technology</label>
                    <input type="number" name="Cost of Technology" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="ProContri" style={labelStyle}>Promotors Contribution</label>
                    <input type="number" name="ProContri" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="Loan" style={labelStyle}>Term Loans</label>
                    <input type="number" name="Loan" style={{width:"30%",height:"30px"}}/>
                </div>
                <div>
                    <label for="Grant" style={labelStyle}>Term Grant</label>
                    <input type="number" name="Grant" style={{width:"30%",height:"30px"}}/>
                </div>
            </form>
            {/* Add Payment Tranche Details */}
        </div>     
    {/* </Online>
    <Offline>
        You're Offline, Please Check your Connection
    </Offline> */}
    </>)
}

