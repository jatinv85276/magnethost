import '../assets/css/inputForm.css'
import { DatePick } from "../subcomponents/DatePick"
import { InputField } from "../subcomponents/InputField"
// import { SelectField } from "../subcomponents/SelectField"
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import Box from '@mui/material/Box';
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import TabPanel from "@mui/lab/TabPanel";
import TabContext from "@mui/lab/TabContext";
import InputLabel from "@mui/material/InputLabel";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
// import { setInputFieldData } from '../redux/slice/landDevDataSlice'; // Import your action creator
import { FullTime } from '../subcomponents/Input3/InputSubForm/Fullime'; 
import { PartTime } from '../subcomponents/Input3/InputSubForm/PartTime';  
import { Input3TabThree } from '../subcomponents/Input3/Input3TabForm/InputThreeTabThree'
// import { InputTwoTab } from '../subcomponents/Input3/Input3TabForm/InputThreeTabThree';

export const InputForm3 = () =>{
  const [value, setValue] = useState("PhyFin");

  const handleChange = (event, newValue) => {
    console.log(value);
    setValue(newValue);
  };

  
  function handlePhysicalFinanceSubmit(e) {
    e.preventDefault();
    const isValid = validateForm();

    if (isValid) {
      console.log("Form is valid and submitted!");
    }
  }

  function validateForm() {
    const errors = {};
    return Object.keys(errors).length === 0;
  }
  
    const [selectedOption, setSelectedOption] = useState('Full Time Employment');
    const [selectoptions,setselectoptions]=useState('Power Source')
    const [selectoptionFacility,setselectedoptionFacility]=useState('Type of Facilities')
    const [selectoptionworkPackage,setselectedworkPackage]=useState('')
    const [selectoptionWorkStatus,setselectedWorkStatus]=useState('')
    
    const handleOptionChange = (event) => {
      setSelectedOption(event.target.value);
    };

    const handleoptionsChanges = (event) => {
        setselectoptions(event.target.value);
      };  

      const handleoptionFacility = (event) => {
        setselectedoptionFacility(event.target.value);
      }; 
      const handleOptionsworkPackage = (event) => {
        setselectedworkPackage(event.target.value);
      }; 
      const handleOptionsWorkStatus = (event) => {
        setselectedWorkStatus(event.target.value);
      };  
    const EMPType = {
        'Full Time Employment':<FullTime compName={selectedOption}/>,
        'Part Time Employment':<PartTime compName={selectedOption}/>
    }
    const powersources={      
        'Solar roop top':<selectoptions/>,
        'Power Generated':<selectoptions/>
        
    }
    const WorkPackage={        
      'CW2':<selectoptionworkPackage/>,
      'CW3':<selectoptionworkPackage/>,
      'CW5':<selectoptionworkPackage/>,
      'CW6':<selectoptionworkPackage/>,
  }
  const WorkStatus = {
    'As per Shedule':<selectoptionWorkStatus/>,
    'Behind Shedule':<selectoptionWorkStatus/>,
    'Ahead of Shedule':<selectoptionWorkStatus/>,
  }
    const Facilities={
        'Cold Storage':<selectoptions/>,
        'Pack House (TPD)':<selectoptions/>,
        'Secondary Processing (TPD)':<selectoptions/>,
        'Tertiary Processing (TPD)':<selectoptions/>
        
    }
  const [selectedCategory, setSelectedCategory] = useState("");

    const Category = {};
    const handleOptionsCategory = (event) => {
        setSelectedCategory(event.target.value);
      }


    return (
        <>
          <main id="main" class="main">
            <section class="section">
              <div class="row">
                <div id="exTab3" class="contain">
                  <div class="card">
                    <Box
                      sx={{
                        width: "100%",
                        typography: "body1",
                        bgcolor: "background.paper",
                      }}
                    >
                      <TabContext value={value}>
                    <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
                      <Tabs
                        variant="scrollable"
                        scrollButtons="auto"
                        aria-label="scrollable auto tabs example"
                        value={value}
                        onChange={handleChange}
                      >
                        <Tab
                          label="Physical Financial"
                          value="PhyFin"
                          className={`${value === "PhyFin" ? "activeClickCSS" : ""}`}
                        />
                        <Tab
                          label="Employment Generated- During construction - MSAMB Facility"
                          value="EmpGen"
                          className={`${value === "EmpGen" ? "activeClickCSS" : ""}`}
                        />
                        <Tab
                          label="Gender Equality and Social Inclusion"
                          value="GenderReg"
                          className={`${value === "GenderReg" ? "activeClickCSS" : ""}`}
                        />
                        <Tab
                          label="Environmental safegaurd"
                          value="Safeguard"
                          className={`${value === "Safeguard" ? "activeClickCSS" : ""}`}
                        />
                        <Tab
                          label="Progress in facilities"
                          value="FacilityChange"
                          className={`${value === "FacilityChange" ? "activeClickCSS" : ""}`}
                        />
                      
                        </Tabs>
                        
                    </Box>
                        
                    {/* Physical status */}
                    <TabPanel value="PhyFin">
                      <div className="tab-pane">
                        <form class="row g-3">
                          <div className="quarterSelect">
                            {/* <SelectField /> */}
                          </div>
                          <div className="word-with-line">
                            <span>Beneficiary Information</span>
                            <i
                              class="fa-solid fa-angle-down"
                              style={{ color: "#4e2683" }}
                            ></i>
                          </div>
                          <div className='col-md-4'>
                          <Box sx={{ minWidth: "100%" }}>
                          <FormControl fullWidth
                            placeholder="FPO/VCO"
                          >
                            <InputLabel id="demo-simple-select-label">
                              Work Package
                            </InputLabel>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              value={selectoptionworkPackage}
                              onChange={handleOptionsworkPackage}
                              label="WorkPackage"
                            >
                              {Object.keys(WorkPackage).map((option) => (
                                <MenuItem value={option}>{option}</MenuItem>
                              ))}
                            </Select>
                          </FormControl>
                          </Box>
                          </div>

                          <div className='col-md-4'>
                          <Box sx={{ minWidth: "100%" }}>
                          <FormControl
                            fullWidth
                            placeholder="FPO/VCO"
                            
                          >
                            <InputLabel id="demo-simple-select-label">
                              Name of the MSAMB Facility
                            </InputLabel>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              value={selectedCategory}
                              onChange={handleOptionsCategory}
                              label="FPOVCO"
                            >
                              {Object.keys(Category).map((option) => (
                                <MenuItem value={option}>{option}</MenuItem>
                              ))}
                            </Select>
                          </FormControl>
                          </Box>
                          </div>
                          <InputField readOnly={true} label="Division" type="text" />
                          <InputField readOnly={true} label="District" type="text" />
                          <InputField readOnly={true} label="Taluka" type="text" />
                          <InputField readOnly={true} label="MAGNET Crops" type="text" />
                          

                          <div
                            className="word-with-line"
                            style={{ margin: "1.5rem 0" }}
                          >
                            <span>
                              Project Wise Information
                            </span>
                            </div>
                        <InputField readOnly={true} label="Contract No" type="number" />
                        <InputField readOnly={true} label="Component as per accepted Tender" type="text" />
                        <InputField readOnly={true} label="Area" type="number" endAr="(in Sqm)" />
                        <InputField readOnly={true} label="Supplier / Contractor Name" type="text" />
                        <InputField readOnly={true} label="Contract Amount" type="number" endAr="(in lakhs)" />
                        <DatePick readOnly={true} label="Contract Award Date" className="custom-date-picker"/>
                        <DatePick readOnly={true} label="Completion Date" className="custom-date-picker"/>
                        <DatePick readOnly={true} label="Extension Date" className="custom-date-picker"/>
                        <InputField readOnly={true} label="Time Elapsed (as on date System date) duration" type="text" />
                        <InputField readOnly={true} label="Contract Extension " type="text" endAr="(Yes/ No)"/>
                        <InputField readOnly={true} label="Price Increase " type="text" endAr="(Yes/ No)"/>
                        <InputField readOnly={true} label="Increased Amount (added in Total Contract Amount)" type="number" endAr="(in lakhs)"/>
                        <InputField readOnly={true} label="Total Disbursed amount on Contract" type="number" endAr="(in lakhs)"/>
                        <InputField readOnly={true} label="Total Undisbursed Amount" type="number" endAr="(in Lakhs)"/>

                        <InputField readOnly={true} label="Total expenditure till date" type="number" endAr="(in lakhs)" />
                        <InputField readOnly={true} label="Civil Progress till date" type="number" endAr="(%)"  />
                        <InputField readOnly={true} label="Electro Mechanical Progress till date" type="number" endAr="(%)"  />
                            <div class="row g-3">
            
                    <div className='word-with-line'>
                        <span>Details of Expenditure for Current Quarter</span>
                        <i class="fa-solid fa-angle-down" style={{color: "#4e2683"}}></i>
                    </div>
                    <div className='col-md-4'>
                          <Box sx={{ minWidth: "100%" }}>
                    <FormControl
                          fullWidth
                            placeholder="FPO/VCO"
                            
                          >
                            <InputLabel id="demo-simple-select-label">
                              Status of Work
                            </InputLabel>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              value={setselectedWorkStatus}
                              onChange={handleOptionsWorkStatus}
                              label="StatusOfWork"
                            >
                              {Object.keys(WorkStatus).map((option) => (
                                <MenuItem value={option}>{option}</MenuItem>
                              ))}
                            </Select>
                          </FormControl>
                          </Box>
                          </div>
                        {/* <InputField readOnly={true} label="Completed Work" type="number" endAr="(%)"/> */}
                        <InputField readOnly={true} label="Expenditure incurred in this quarter" type="number" endAr="(in lakhs)"  />
                        <div className='word-with-line'>
                        <span>Details of Physical Progress for Current Quarter</span>
                        <i class="fa-solid fa-angle-down" style={{color: "#4e2683"}}></i>
                      </div>
                        <InputField readOnly={true} label="Civil Progress" type="number" endAr="(%)"  />
                        <InputField readOnly={true} label="Electro Mechanical Progress" type="number" endAr="(%)"  />
                        <InputField readOnly={true}
                            col="6"
                            label="Action points/ Recommendation made"
                            type="text"
                          />
                        <div class="col-md-4" style={{position: "relative",  bottom: "19px"}}>
                        <label for="inputNumber" class="col-lg col-form-label">Remarks (Upload Progress Report / Photos)</label>
                        <div class="col-sm-10">
                          
                            <input class="form-control" type="file" id="formFile"/>
                        </div>
                    </div>
                     
                        
                </div>
                          <div style={{ textAlign: "center" }}>
                            <button type="submit" class="btn submitbtn">
                              Submit
                            </button>
                            <button type="reset" class="btn btn-secondary">
                              Reset
                            </button>
                          </div>
                        </form>
                      </div>
                    </TabPanel>

                    {/* Emp generation during Construction  */}
                    <TabPanel value="EmpGen">
                    <div className="tab-pane">
                      
                        <form class="row g-3">
                          <div className="quarterSelect">
                            {/* <SelectField /> */}
                          </div>
                          <div className="word-with-line">
                            <span>Beneficiary Information</span>
                            <i
                              class="fa-solid fa-angle-down"
                              style={{ color: "#4e2683" }}
                            ></i>
                          </div>
                          <div className='col-md-4'>
                          <Box sx={{ minWidth: "100%" }}>
                          <FormControl
                            fullWidth
                            placeholder="FPO/VCO"
                            
                          >
                            <InputLabel id="demo-simple-select-label">
                              Work Package
                            </InputLabel>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              value={selectoptionworkPackage}
                              onChange={handleOptionsworkPackage}
                              label="WorkPackage"
                            >
                              {Object.keys(WorkPackage).map((option) => (
                                <MenuItem value={option}>{option}</MenuItem>
                              ))}
                            </Select>
                          </FormControl>
                          </Box>
                          </div>
                          <div className='col-md-4'>
                          <Box sx={{ minWidth: "100%" }}>
                          <FormControl
                            fullWidth
                            placeholder="FPO/VCO"
                            
                          >
                            <InputLabel id="demo-simple-select-label">
                              Name of the MSAMB Facility
                            </InputLabel>
                            <Select
                              labelId="demo-simple-select-label"
                              id="demo-simple-select"
                              value={selectedCategory}
                              onChange={handleOptionsCategory}
                              label="FPOVCO"
                            >
                              {Object.keys(Category).map((option) => (
                                <MenuItem value={option}>{option}</MenuItem>
                              ))}
                            </Select>
                          </FormControl>
                          </Box>
                          </div>
                          <InputField readOnly={true} label="Division" type="text" />
                          <InputField readOnly={true} label="District" type="text" />
                          <InputField readOnly={true} label="Taluka" type="text" />
                          
                          <div className='word-with-line' style={{margin: "1.5rem 0"}}>
                                    <span>Details of Current Quarter for - {selectedOption} </span>
                                    <div>
                                        {/* <label>Select an Option:</label> */}
                                        <Box sx={{ minWidth: 390 }} style={{backgroundColor:"#ededed"}}>
                                            <FormControl fullWidth>
                                                <InputLabel id="demo-simple-select-label">Employment Type</InputLabel>
                                                <Select
                                                labelId="demo-simple-select-label"
                                                id="demo-simple-select"
                                                // value={age}
                                                value={selectedOption} onChange={handleOptionChange}
                                                label="Emptype"
                                                // onChange={handleChange}
                                                >
                                                {Object.keys(EMPType).map((option) => (
                                                    <MenuItem value={option}>{option}</MenuItem>
                                                    
                                                ))}
                                                
                                                </Select>
                                            </FormControl>
                                            </Box>
                                        
                                    </div>
                                </div>
                                <br/>
                                        <div>
                                            {selectedOption && (
                                            <div>
                                                {EMPType[selectedOption]}
                                            </div>
                                            )}
                                        </div>    
                                    
                                    <div style={{textAlign:"center"}}>
                                        <button type="submit" class="btn submitbtn" >Submit</button>
                                        <button type="reset" class="btn btn-secondary">Reset</button>
                                    </div>
                        </form>
                      </div>
                    </TabPanel>
                    
                    
                    {/* Gender inclusion and social inclusion regulatory enforcement processes */}
                    <TabPanel value="GenderReg">
                    <div className="tab-pane">
                    <form class="row g-3" >
                                <div className='quarterSelect'>
                                    {/* <SelectField/> */}
                                </div>
                                    <div className='word-with-line'>
                                        <span>Beneficiary Information</span>
                                        <i class="fa-solid fa-angle-down" style={{color: "#4e2683"}}></i>
                                    </div>
                                    <div className='col-md-4'>
                          <Box sx={{ minWidth: "100%" }}>
                                    <FormControl
                                      fullWidth
                                      placeholder="FPO/VCO"
                                      
                                    >
                                      <InputLabel id="demo-simple-select-label">
                                        Work Package
                                      </InputLabel>
                                      <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={selectoptionworkPackage}
                                        onChange={handleOptionsworkPackage}
                                        label="WorkPackage"
                                      >
                                        {Object.keys(WorkPackage).map((option) => (
                                          <MenuItem value={option}>{option}</MenuItem>
                                        ))}
                                      </Select>
                                    </FormControl>
                                    </Box>
                                    </div>
                                    <div className='col-md-4'>
                          <Box sx={{ minWidth: "100%" }}>
                                    <FormControl
                                      fullWidth
                                      placeholder="FPO/VCO"
                                      
                                    >
                                      <InputLabel id="demo-simple-select-label">
                                        Name of the MSAMB Facility
                                      </InputLabel>
                                      <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={selectedCategory}
                                        onChange={handleOptionsCategory}
                                        label="FPOVCO"
                                      >
                                        {Object.keys(Category).map((option) => (
                                          <MenuItem value={option}>{option}</MenuItem>
                                        ))}
                                      </Select>
                                    </FormControl>
                                    </Box>
                                    </div>
                                    <InputField readOnly={true} label="Division" type="text"  />
                                    <InputField readOnly={true} label="District" type="text"  />
                                    <InputField readOnly={true} label="Taluka" type="text"   />

                                    {/* <InputField readOnly={true} label="Total No. of Members" type="number"/>
                                    <InputField readOnly={true} label="Total No.of women" type="number"/> */}
                                    
                                <br/>
                                    <div className='word-with-line' style={{margin: "1.5rem 0"}}>
                                        <span>Details of Current Quarter </span>
                                    </div>
                                    {/* <InputField readOnly={true} label="Total No.of Women having farm land in their family name" type="number"/>

                                    <InputField readOnly={true} label="Number of SC women"  type="number"/>
                                    <InputField readOnly={true} label="Number of ST women"  type="number"/>
                                    <InputField readOnly={true} label="Number of disabled women" type="number"/>
                                    <InputField readOnly={true} label="Number of women below the poverty line"  type="number"/> */}

                                    {/* <InputField readOnly={true} label="Total number of toilets" type="number"/> */}
                                    <InputField readOnly={true} label="Total number of toilets for men" type="number"/>
                                    <InputField readOnly={true} label="Total number of toilets for women" type="number"/>
                                    <InputField readOnly={true} label="Total number of toilets for disabled" type="number"/>
                                    <div style={{marginTop:"1.5rem"}}>
                                        <Input3TabThree/>
                                    </div>
                                <br/>
                                    <div style={{textAlign:"center"}}>
                                        <button type="submit" class="btn submitbtn" >Submit</button>
                                        <button type="reset" class="btn btn-secondary">Reset</button>
                                    </div>
                                </form>  
                      </div>
                    </TabPanel>
                    
                    
                    {/* Safeguard */}
                    <TabPanel value="Safeguard">
                    <div className="tab-pane">
                    <form class="row g-3">
                                <div className='quarterSelect'>
                                    {/* <SelectField/> */}
                                </div>
                                    <div className='word-with-line'>
                                        <span>Beneficiary Information</span>
                                        <i class="fa-solid fa-angle-down" style={{color: "#4e2683"}}></i>
                                    </div>

                                    <div className='col-md-4'>
                          <Box sx={{ minWidth: "100%" }}>
                                    <FormControl
                                    fullWidth
                                    placeholder="FPO/VCO"
                                    
                                  >
                                    <InputLabel id="demo-simple-select-label">
                                      Work Package
                                    </InputLabel>
                                    <Select
                                      labelId="demo-simple-select-label"
                                      id="demo-simple-select"
                                      value={selectoptionworkPackage}
                                      onChange={handleOptionsworkPackage}
                                      label="WorkPackage"
                                    >
                                      {Object.keys(WorkPackage).map((option) => (
                                        <MenuItem value={option}>{option}</MenuItem>
                                      ))}
                                    </Select>
                                  </FormControl>

                                  </Box>
                                  </div>
                                  <div className='col-md-4'>
                                    <Box sx={{ minWidth: "100%" }}>
                                  <FormControl
                                      fullWidth
                                      placeholder="FPO/VCO"
                                      
                                    >
                                      <InputLabel id="demo-simple-select-label">
                                        Name of the MSAMB Facility
                                      </InputLabel>
                                      <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={selectedCategory}
                                        onChange={handleOptionsCategory}
                                        label="FPOVCO"
                                      >
                                        {Object.keys(Category).map((option) => (
                                          <MenuItem value={option}>{option}</MenuItem>
                                        ))}
                                      </Select>
                                    </FormControl>
                                    </Box>
                                    </div>
                                    <InputField readOnly={true} label="Division" type="text"/>
                                    <InputField readOnly={true} label="District" type="text"/>
                                    <InputField readOnly={true} label="Taluka" type="text"/>
                                    
                                    <div className='word-with-line'>
                                        <span>Details for - Upgradation of Facilities- MSAMB -Safeguard</span>
                                        <i class="fa-solid fa-angle-down" style={{color: "#4e2683"}}></i>
                                    </div>
                                    
                                    <InputField readOnly={true} label="Total Power Augumented in Grid" type="number" />
                                    <InputField readOnly={true} label="Power Bill Saved" type="number" endAr="(in Thousand)"/>
                                    <InputField readOnly={true} label="M Cub of Rain water Harvested" type="number" endAr="(in ltr)"/>
                                    <div>
                                            <FormControl className="col-4" placeholder="Power source">
                                                <InputLabel id="demo-simple-select-label">Power Source</InputLabel>
                                                <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    // value={age}
                                                    value={selectoptions} onChange={handleoptionsChanges}
                                                    label="powersources  "
                                                    // onChange={handleChange}
                                                >
                                                {Object.keys(powersources).map((option) => (
                                                    <MenuItem value={option}>{option}</MenuItem>
                                                    
                                                ))}
                                               
                                                </Select>
                                            </FormControl>
                                            {/* </Box> */}
                                  
                                    </div>
                                        <div>
                                            {selectedOption && (
                                            <div>
                                                {powersources[selectoptions]}
                                            </div>
                                            )}
                                        </div>
                                        <div style={{marginTop:"1.5rem"}}>
                                            {/* <InputTwoTab/> */}
                                        </div>
                                    <div style={{textAlign:"center"}}>
                                    <button type="submit" class="btn submitbtn">Submit</button>
                                    <button type="reset" class="btn btn-secondary">Reset</button>
                                </div>
                            </form> 
                      </div>
                    </TabPanel>


                    {/* GESI */}
                    <TabPanel value="FacilityChange">
                    <div className="tab-pane">
                    <form class="row g-3">
                                <div className='quarterSelect'>
                                    {/* <SelectField/> */}
                                </div>
                                    <div className='word-with-line'>
                                        <span>Beneficiary Information</span>
                                        <i class="fa-solid fa-angle-down" style={{color: "#4e2683"}}></i>
                                    </div>
                                    <div className='col-md-4'>
                          <Box sx={{ minWidth: "100%" }}>
                                    <FormControl
                                    fullWidth
                                    placeholder="FPO/VCO"
                                    
                                  >
                                    <InputLabel id="demo-simple-select-label">
                                      Work Package
                                    </InputLabel>
                                    <Select
                                      labelId="demo-simple-select-label"
                                      id="demo-simple-select"
                                      value={selectoptionworkPackage}
                                      onChange={handleOptionsworkPackage}
                                      label="WorkPackage"
                                    >
                                      {Object.keys(WorkPackage).map((option) => (
                                        <MenuItem value={option}>{option}</MenuItem>
                                      ))}
                                    </Select>
                                  </FormControl>
                                  </Box>
                                  </div>

                                  <div className='col-md-4'>
                          <Box sx={{ minWidth: "100%" }}>
                                  <FormControl
                                      fullWidth
                                      placeholder="FPO/VCO"
                                      
                                    >
                                      <InputLabel id="demo-simple-select-label">
                                        Name of the MSAMB Facility
                                      </InputLabel>
                                      <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={selectedCategory}
                                        onChange={handleOptionsCategory}
                                        label="FPOVCO"
                                      >
                                        {Object.keys(Category).map((option) => (
                                          <MenuItem value={option}>{option}</MenuItem>
                                        ))}
                                      </Select>
                                    </FormControl>
                                    </Box>
                                    </div>
                                    <InputField readOnly={true} label="Division" type="text" disable="true" />
                                    <InputField readOnly={true} label="District" type="text"  disable="true" />
                                    <InputField readOnly={true} label="Taluka" type="text"  disable="true" />

                                    <div className='word-with-line'>
                                        <span>Details for - Changes on Facilities to be GESI compliant - MSAMB</span>
                                        <i class="fa-solid fa-angle-down" style={{color: "#4e2683"}}></i>
                                    </div>
                                    {/* <InputField readOnly={true} label="Type of Facility" type="text"/> */}
                                    <div className='col-md-4'>
                          <Box sx={{ minWidth: "100%" }}>
                                    <FormControl fullWidth>
                                                <InputLabel id="demo-simple-select-label">Type of Facility*</InputLabel>
                                                <Select
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    // value={age}
                                                    value={selectoptionFacility} onChange={handleoptionFacility}
                                                    label="Type of facility*  "
                                                    // onChange={handleChange}
                                                >
                                                {Object.keys(Facilities).map((option) => (
                                                    <MenuItem value={option}>{option}</MenuItem>
                                                    
                                                ))}
                                               
                                                </Select>
                                            </FormControl>
                                            </Box>
                                            </div>
                                            {/* {Facilities[Facility]} */}
                                    
                                            
                                        
                                    <InputField readOnly={true} label="Capicity" type="text" endAr="MTR"/>
                                        
                                    <InputField readOnly={true} label="Progress" type="number" endAr="%"/>
                                    <InputField readOnly={true} label="Action taken" type="text" />
                                    <br/>
                                    <InputField readOnly={true} label="Suggestions/Changes made" type="text" style={{ width: '200px' }}/>
                                    <div style={{textAlign:"center"}}>
                                    <button type="submit" class="btn submitbtn" >Submit</button>
                                    <button type="reset" class="btn btn-secondary">Reset</button>
                                </div>
                            </form> 
                      </div>
                    </TabPanel>

                  
                    </TabContext>
                    </Box>
                  </div>
                </div>
              </div>
            </section>
          </main>
        </>
      );
    };    