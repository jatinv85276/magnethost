import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  expenditureIncurred:'',
  promotersExpenditure:'',
  matchingGrantExpenditure:'',
  termLoanExpenditure:'',
  
  // Current Quarter
  totalProjectCost: '',
  promotersContribution: '',
  matchingGrant:'',
  termLoan:'',
  progressByEnd:'',

  // Till Date
  totalExpenditure:'',
  physicalProgress:'',
  totalPromotorsExpenditure:'',
  totalMatchingGrantExpenditure:'',
  totalTermLoan:''
  // Add other input fields' initial values here
};

const landDevDataSlice = createSlice({
  name: 'landDevDataSlice',
  initialState,
  reducers: {
    setInputFieldData: (state, action) => {
      // Use the action payload to update the corresponding input field
      state[action.payload.fieldName] = action.payload.value;
    },
    // Add other reducers for different input fields if needed
  },
});

export const { setInputFieldData } = landDevDataSlice.actions;

export default landDevDataSlice.reducer;
