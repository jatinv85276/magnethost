import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  ///////////

  from_date:null,
  to_date:null,
  location_taluka: '',
  location_address: '',
  name_of_organization:'',
  nomination_by_division:[],

  participants_male: '',
  participants_female: '',
  participants_total: '',


  validationErrors:{

    from_date:'',
    to_date:'',
    location_taluka: '',
    location_address: '',
    name_of_organization:'',
    nomination_by_division:'',
    participants_male: '',
    participants_female: '',
  }
};

const TotTrainingSlice = createSlice({
  name: 'TotTrainingSlice',
  initialState,
  reducers: {
    setInputFieldData: (state, action) => {
      // Use the action payload to update the corresponding input field
      state[action.payload.fieldName] = action.payload.value;
    },
    setTotalParticipants: (state, action) => {
        state.participants_total= action.payload;
      },
    // Add other reducers for different input fields if needed
    updateTotTrainingForm: (state, action) =>{
      const payload = action.payload;
      // Loop through the keys in initialState and update the corresponding fields
      for (const key in initialState) {
        if (payload[key] !== undefined) {
          state[key] = payload[key];
        }
      }
      // state.expenditureIncurred = action.payload.amount_disbursement;
    },

    resetDataTotTraining: (state) => {
      // Reset all fields to their initial values
      return { ...initialState };
    },
    setValidationErrorsTotTraining: (state, action) => {
      state.validationErrors = { ...state.validationErrors, ...action.payload };
    },
    resetValidation: (state, action) =>{
      state.validationErrors = { ...initialState.validationErrors };
    },
  },
});

export const {setValidationErrorsTotTraining, resetValidation, setInputFieldData, updateTotTrainingForm, resetDataTotTraining, setTotalOBC, setTotalBPL, setTotalPWD, setTotalST, setTotalSC, setTotalParticipants } = TotTrainingSlice.actions;

export default TotTrainingSlice.reducer;
