import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  ///////////

  name_of_exhibition: '',
  location_of_exhibition: '',
  country_of_exhibition: '',

  total_cost: "",
  total_saction_grant: "",
  beneficiary_contribution: "",
  disbursement_amt: "",
  disbursement_date: null,


  validationErrors:{
    disbursement_amt: "",
    disbursement_date: "",
  }
};

const marketAssistenceSlice = createSlice({
  name: 'marketAssistenceSlice',
  initialState,
  reducers: {
    setInputFieldData: (state, action) => {
      // Use the action payload to update the corresponding input field
      state[action.payload.fieldName] = action.payload.value;
    },
    // Add other reducers for different input fields if needed
    updateMarketAssistForm: (state, action) =>{
      const payload = action.payload;
      // Loop through the keys in initialState and update the corresponding fields
      for (const key in initialState) {
        if (payload[key] !== undefined) {
          state[key] = payload[key];
        }
      }
      // state.expenditureIncurred = action.payload.amount_disbursement;
    },

    resetDataMarketAssist: (state) => {
      // Reset all fields to their initial values
      return { ...initialState };
    },
    setValidationErrorsMarketAssist: (state, action) => {
      state.validationErrors = { ...state.validationErrors, ...action.payload };
    },
    resetValidation: (state, action) =>{
      state.validationErrors = { ...initialState.validationErrors };
    },
  },
});

export const {setValidationErrorsMarketAssist, resetValidation, setInputFieldData, updateMarketAssistForm, resetDataMarketAssist} = marketAssistenceSlice.actions;

export default marketAssistenceSlice.reducer;
