import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  ///////////
  payment_tranche: '',
  tranche_id: '',
  
  exp_incurred:'',
  promoters_exp_incurred:'',
  matching_grant_exp_incurred:'',
  term_loan_exp_incurred:'',

  total_term_loan_exp_till_date:'',
  total_promotors_exp_till_date:'',
  total_matching_grant_exp_till_date:'',

  // Current Quarter
  total_technology_cost: '',
  promoters_contribution: '',
  matching_grant:'',
  term_loan:'',

  amount_disbursement: '',
  disbursement_date: null,
  
  validationErrors:{
    promoters_exp_incurred:'',
    matching_grant_exp_incurred:'',
    term_loan_exp_incurred:'',
  }
};

const SupportTechSlice = createSlice({
  name: 'SupportTechSlice',
  initialState,
  reducers: {
    setInputFieldData: (state, action) => {
      // Use the action payload to update the corresponding input field
      state[action.payload.fieldName] = action.payload.value;
    },
    // Add other reducers for different input fields if needed
    updateApiDataToSupportTech: (state, action) =>{
      const payload = action.payload;
      // Loop through the keys in initialState and update the corresponding fields
      for (const key in initialState) {
        if (payload[key] !== undefined) {
          state[key] = payload[key];
        }
      }
    },
    resetDataSupportTech: (state) => {
      // Reset all fields to their initial values
      return { ...initialState };
    },
    setValidationErrorsSupportTech: (state, action) => {
      state.validationErrors = { ...state.validationErrors, ...action.payload };
    },
    resetValidation: (state, action) =>{
      state.validationErrors = { ...initialState.validationErrors };
    },
  },
});

export const {setValidationErrorsSupportTech, resetValidation, setInputFieldData, updateApiDataToSupportTech, resetDataSupportTech } = SupportTechSlice.actions;

export default SupportTechSlice.reducer;
