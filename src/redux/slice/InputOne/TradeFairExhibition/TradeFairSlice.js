import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  ///////////
  state_id: '',
  country_id: '',
  from_date: null,
  to_date: null,
  location_address: '',
  name_of_trade_fair_exhibition: '',
  participants_male: '',
  participants_female: '',
  participants_total: '',

  validationErrors: {
    state_id: '',
    country_id: '',
    from_date: '',
    to_date: '',
    location_address: '',
    name_of_trade_fair_exhibition: '',
    participants_male: '',
    participants_female: '',
    participants_total: '',
  }
};

const TradeFairExhibitionSlice = createSlice({
  name: 'TradeFairExhibitionSlice',
  initialState,
  reducers: {
    setInputFieldData: (state, action) => {
      // Use the action payload to update the corresponding input field
      state[action.payload.fieldName] = action.payload.value;
    },
    setTotalParticipants: (state, action) => {
      state.participants_total= action.payload;
    },
    // Add other reducers for different input fields if needed
    updateApiDataTradeFair: (state, action) =>{
      const payload = action.payload;
      // Loop through the keys in initialState and update the corresponding fields
      for (const key in initialState) {
        if (payload[key] !== undefined) {
          state[key] = payload[key];
        }
      }
    },
    resetDataTradeFair: (state) => {
      // Reset all fields to their initial values
      return { ...initialState };
    },
    setValidationErrorsTradeFair: (state, action) => {
      state.validationErrors = { ...state.validationErrors, ...action.payload };
    },
    resetValidation: (state, action) =>{
      state.validationErrors = { ...initialState.validationErrors };
    },
  },
});

export const {setValidationErrorsTradeFair, setTotalParticipants, resetValidation, setInputFieldData, updateApiDataTradeFair, resetDataTradeFair } = TradeFairExhibitionSlice.actions;

export default TradeFairExhibitionSlice.reducer;
