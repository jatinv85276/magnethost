
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  ///////////

  "6": {
    magnet_crop_id:[],

    location_taluka:'',
    no_of_farmers:'',
    area:'',
    total_project_cost:'',
    matching_grant:'',
  
    mg_1st_tranche:'',
    mg_2nd_tranche:'',
    mg_3rd_tranche:'',
    mg_4th_tranche:'',
    actual_date_1:'',
    actual_date_2:'',
    actual_date_3:'',
    actual_date_4:'',
  },
    // Validation
  validationErrors:{  
    magnet_crop_id:[],
    location_taluka:'',
    no_of_farmers:'',
    area:'',
    total_project_cost:'',
    matching_grant:'',

    mg_1st_tranche:'',
    mg_2nd_tranche:'',
    mg_3rd_tranche:'',
    mg_4th_tranche:'',
    actual_date_1:'',
    actual_date_2:'',
    actual_date_3:'',
    actual_date_4:'',

        }
    };

const HDPMasterSlice = createSlice({
  name: 'HDPMasterSlice',
  initialState,
  reducers: {
    setInputFieldData: (state, action) => {
      // Use the action payload to update the corresponding input field
      state["6"][action.payload.fieldName] = action.payload.value;
    },
    // Add other reducers for different input fields if needed
    updateApiDataHDPMaster: (state, action) =>{
      const payload = action.payload;
      // Loop through the keys in initialState and update the corresponding fields
      for (const key in initialState["6"]) {
        if (payload[key] !== undefined) {
          // state["6"][key] = payload[key];
          if(payload[key] === null){
            state["6"][key] = '';
          }else{
            state["6"][key] = payload[key];
          }
        }
      }
    },
    resetDataHDPMaster: (state) => {
      // Reset all fields to their initial values
      return { ...initialState };
    },
    setValidationErrorsHDPMaster: (state, action) => {
      state.validationErrors = { ...state.validationErrors, ...action.payload };
    },
    resetValidation: (state, action) =>{
      state.validationErrors = { ...initialState.validationErrors };
    },
  },
});

export const {setValidationErrorsEntityMaster, resetValidation, setInputFieldData, updateApiDataHDPMaster, resetDataHDPMaster } = HDPMasterSlice.actions;

export default HDPMasterSlice.reducer;
