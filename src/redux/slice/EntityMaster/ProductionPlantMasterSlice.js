
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  ///////////

  "16": {
    typedata: [],
  },

// Validation
  validationErrors:{  
    magnet_crop_id:[],
    location_taluka:'',
    area:'',
    capacity_total_plants:'',
    total_project_cost:'',
    bank_term_loan:'',
    matching_grant:'',
        }
    };

const ProdPlantMasterSlice = createSlice({
  name: 'ProdPlantMasterSlice',
  initialState,
  reducers: {
    setInputFieldData: (state, action) => {
      // Use the action payload to update the corresponding input field
      state["16"][action.payload.fieldName] = action.payload.value;
    },
    setInputActivity: (state, action) => {
      state["16"].typedata = action.payload;
    },
    // Add other reducers for different input fields if needed
    updateApiDataProdPlantMaster: (state, action) =>{
      const payload = action.payload;
      // const payload = { ["typedata"]: pay["typedata"] };
      // console.log(newObj);
      const existingIndex = state["16"].typedata.findIndex(item => item.id === payload.id);
    
      if (existingIndex !== -1) {
        // If the item with the same id exists, update it
        state["16"].typedata[existingIndex] = payload;
      } else {
        // If not, push the new item
        state["16"].typedata.push(payload);
      }
    },
    resetDataProdPlantMaster: (state) => {
      // Reset all fields to their initial values
      return { ...initialState };
    },
    setValidationErrorsProdPlantMaster: (state, action) => {
      state.validationErrors = { ...state.validationErrors, ...action.payload };
    },
    resetValidation: (state, action) =>{
      state.validationErrors = { ...initialState.validationErrors };
    },
  },
});

export const {setValidationErrorsEntityMaster, setInputActivity, setComponents, resetValidation, setInputFieldData, updateApiDataProdPlantMaster, resetDataProdPlantMaster } = ProdPlantMasterSlice.actions;

export default ProdPlantMasterSlice.reducer;
