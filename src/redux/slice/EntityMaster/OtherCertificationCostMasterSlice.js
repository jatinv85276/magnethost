
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  ///////////

  "15": {
    typedata: [],
  },

// Validation
  validationErrors:{  
    magnet_crop_id:[],
    location_taluka:'',
    area:'',
    capacity_total_plants:'',
    total_project_cost:'',
    bank_term_loan:'',
    matching_grant:'',
        }
    };

const OtherCertiMasterSlice = createSlice({
  name: 'OtherCertiMasterSlice',
  initialState,
  reducers: {
    setInputFieldData: (state, action) => {
      // Use the action payload to update the corresponding input field
      state["15"][action.payload.fieldName] = action.payload.value;
    },
    setInputActivity: (state, action) => {
      state["15"].typedata = action.payload;
    },
    // Add other reducers for different input fields if needed
    updateApiDataOtherCertiMaster: (state, action) =>{
      const payload = action.payload;
      // const payload = { ["typedata"]: pay["typedata"] };
      // console.log(newObj);
      const existingIndex = state["15"].typedata.findIndex(item => item.id === payload.id);
    
      if (existingIndex !== -1) {
        // If the item with the same id exists, update it
        state["15"].typedata[existingIndex] = payload;
      } else {
        // If not, push the new item
        state["15"].typedata.push(payload);
      }
    },
    resetDataOtherCertiMaster: (state) => {
      // Reset all fields to their initial values
      return { ...initialState };
    },
    setValidationErrorsOtherCertiMaster: (state, action) => {
      state.validationErrors = { ...state.validationErrors, ...action.payload };
    },
    resetValidation: (state, action) =>{
      state.validationErrors = { ...initialState.validationErrors };
    },
  },
});

export const {setValidationErrorsEntityMaster, setInputActivity, setComponents, resetValidation, setInputFieldData, updateApiDataOtherCertiMaster, resetDataOtherCertiMaster } = OtherCertiMasterSlice.actions;

export default OtherCertiMasterSlice.reducer;
