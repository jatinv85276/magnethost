// export const GAPPH1 = [
//     {
//         'id':5,
//         'type': "Training on GAP"
//     },
//     {
//         'id':7,
//         'type': "Certification cost on GAP"
//     },
//     {
//         'id':6,
//         'type': "MRL testing"
//     },
//     {
//         'id':8,
//         'type': "Support for Fruit Care Activities"
//     },
// ]

// export const PHM = [
//     {
//         'id': 3,
//         'type':'PH training of 5 days'
//     },
//     {
//         'id': 4,
//         'type':'PH training of 3 days'
//     }
// ]

// export const ValueChain = [
//     {
//         'id': 1,
//         'type':'Training of BoD of FPOs'
//     },
//     {
//         'id': 2,
//         'type':'Training of women led FPOs'
//     }
// ] 

// export const MarketDevAss = [
//     {
//         'id': 9,
//         'type':'Gulfood Dubai'
//     },
//     {
//         'id': 10,
//         'type':'Fruit Logistica'
//     },
//     {
//         'id': 11,
//         'type':'Asia Fruit Logistica'
//     }
// ] 

// export const OtherCert = [
//     {
//         'id': 12,
//         'type': 'SMETA'
//     }
// ]

// export const ProdPlant = [
//     {
//         'id': 13,
//         'type': 'HACCP'
//     }
// ]

// export const StudyExpVisit = [
//     {
//         'id': 17,
//         'type':'International'
//     },
//     {
//         'id': 18,
//         'type':'Outside state'
//     }
// ] 

// export const StudyTourFarm = [
//     {
//         'id': 14,
//         'type':'Within state (3 days trip, 2 farmers per FPO)'
//     },
//     {
//         'id': 15,
//         'type':'Outside state (7 days trip, 2 farmers per FPO)'
//     },
//     {
//         'id': 16,
//         'type':'International (30 farmers per crop)'
//     }
// ] 

// export const TradeFairExhibition = [
//     {
//         'id': 20,
//         'type':'Domestic'
//     },
//     {
//         'id': 21,
//         'type':'International'
//     },
// ] 

// export const MarketDevProm = [
//     {
//         'id': 22,
//         "type": "Agri tech workshop",
//     },
//     {
//         'id': 23,
//         "type": "Buyer Seller",
//     },
// ] 