export const farmerCompanies = [
    "Aakash Agro Farmer PCL",
    "Aagrinnex Farm Solutions Private Limited",
    "Agasti Farmers Producer Company Limited",
    "Agrinexus Farm Solutions Private Limited",
    "Agronirmiti Export Innovation Farmer Producer Company Limited",
    "Anandwadi Agro and Animal Husbandry Farmers Producer Co. Ltd",
    "Anis Mustak Shaikh",
    "Anukaran Farmers PCL",
    "Aarya Cold Storage and Exports",
    "Bhairavnath Enterprises",
    "Bhagyatara Farmers Producer Company Limited",
    "Devnadi Agricultural Producer Company Limited",
    "Dudheshwar Farmers Producer Company Limited",
    "Gitai Farmers PCL",
    "Goveendaya Farmers Producer Company Limited",
    "Indica Fresh",
    "Kiran Doke Fruit Supplier",
    "Konewadi FPCL",
    "Lokhande Agro",
    "Madalmohi FPCL",
    "Mahableshwar Strawberry and Organic Farmer Producer Company Ltd",
    "Monsoon Foods",
    "Nisarga Business Holding LLP",
    "Nitin Foods",
    "Nature One Farm Produce Pvt. Ltd.",
    "Natural Farms and Agro Products PCL",
    "Om Gayatri FPCL",
    "Priyadarshini Flouriculture",
    "Raien Fresh Produce Pvt. Ltd.",
    "Sangharsh Lok Sanchalit Sadhan Kendra",
    "Savitribai CMRC, Ashti",
    "Savitribai Fule Tejaswini CMRC, Waluj",
    "Savitribai Loksanchalit Sadhan Kendra, Akluj",
    "Shidod FPCL",
    "Shriram Fruit Processing Co-operative Society Ltd.",
    "Shivkrupa Fruit Suppliers and Cold Storage",
    "SRP Overseas",
    "Sonpaula Agro PCL",
    "Vanashree Farmer PCL",
    "Varchaswa Farmer PCL",
    "Venkatesh Agro Processing Company",
    "Vinayak Farmers Producer Company Ltd.",
    "Zashichi Rani CMRC, Jalna"
  ];
  