export const ServerPage = ({ error }) =>{
    return(<>
        <h1>500 Internal Server Error</h1>
        <p>{error.message}</p>
    </>)
}