import { EventsForm } from "../components/EventsForm"
import { Header } from "../components/Header"
import { LeftNavigation } from "../components/LeftNavigation"

export const EventsPage = () => {
    return (
        <>
            <Header/>
            <LeftNavigation/>
            <EventsForm/>
        </>
    )
}