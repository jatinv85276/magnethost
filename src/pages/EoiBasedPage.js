import { EoiBasedForm } from "../components/EoiBasedForm"
import { Header } from "../components/Header"
import { LeftNavigation } from "../components/LeftNavigation"

export const EoiBasedPage = () => {
    return (
        <>
            <Header/>
            <LeftNavigation/>
            <EoiBasedForm/>
        </>
    )
}