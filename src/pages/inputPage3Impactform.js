import { Header } from "../components/Header"
import { InputForm2ImapctForms } from "../components/Input2Impactforms"
import { InputForm3ImapctForms } from "../components/Input3Impactforms"
import { LeftNavigation } from "../components/LeftNavigation"

export const InputPage2ImpactForms = () => {
    return (<>
        <Header/>
        <LeftNavigation/>
        <InputForm3ImapctForms/>
    </>)
}