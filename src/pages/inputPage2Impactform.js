import { Header } from "../components/Header"
import { InputForm2ImapctForms } from "../components/Input2Impactforms"
import { LeftNavigation } from "../components/LeftNavigation"

export const InputPage2ImpactForms = () => {
    return (<>
        <Header/>
        <LeftNavigation/>
        <InputForm2ImapctForms/>
    </>)
}