import { Header } from "../components/Header"
import { InputForm13 } from "../components/InputForm13"
import { LeftNavigation } from "../components/LeftNavigation"

export const InputPage3 = () => {
    return (<>
        <Header/>
        <LeftNavigation/>
        <InputForm13/>
    </>)
}