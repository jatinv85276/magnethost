import { Header } from "../components/Header"
import { InvitationBasedForm } from "../components/InvitationBasedForm"
import { LeftNavigation } from "../components/LeftNavigation"

export const InvitationBasedPage = () => {
    return (
        <>
            <Header/>
            <LeftNavigation/>
            <InvitationBasedForm/>
        </>
    )
}