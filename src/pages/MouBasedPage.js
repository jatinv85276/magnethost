import { Header } from "../components/Header"
import { LeftNavigation } from "../components/LeftNavigation"
import { MouBasedForm } from "../components/MouBasedForm"

export const MouBasedPage = () => {
    return (
        <>
            <Header/>
            <LeftNavigation/>
            <MouBasedForm/>
        </>
    )
}