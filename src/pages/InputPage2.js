import { ErrorBoundary } from "react-error-boundary";
import { Header } from "../components/Header"
import { InputForm2 } from "../components/InputForm2"
import { LeftNavigation } from "../components/LeftNavigation"
import { HasAccess } from "@permify/react-role";
import { ServerPage } from "./ErrorPage/ServerPage";

export const InputPage2 = () =>{

    return(<>
        <Header/>
           
        <LeftNavigation/>
        <ErrorBoundary FallbackComponent={<ServerPage/>}>
            <InputForm2/>
        </ErrorBoundary>
    </>)
}